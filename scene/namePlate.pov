#version 3.7

#include "E_Nom.inc"
#include "colors.inc"
#include "textures.inc"
#include "lego.inc"



/* FLAGS */
#declare HighQualTexture=0;
#declare LegoQuality=1;


/* Textures */
#if (HighQualTexture!=1)

#declare TLego1= texture {
  pigment {color Red transmit .8}
  finish {phong 1.0 phong_size 20 diffuse .7 refraction on ior 1.33}
  }

#declare TLego2= texture {
  pigment {color Yellow transmit .8}
  finish {phong 1.0 phong_size 20 diffuse .7 refraction on ior 1.33}
  }
 #else

 #declare TLego1= texture {
  pigment {color Red transmit 0}
//  finish {phong 1.0 phong_size 20 diffuse .7 refraction on ior 1.33}
  }
 #declare TLego2= texture {
  pigment {color Red transmit 0}
//  finish {phong 1.0 phong_size 20 diffuse .7 refraction on ior 1.33}
  }

 #end


// #declare TextureVec=[texture {TLego2},texture{TLego1},texture {TLego2}]

/*
*********************************************************
*                                                       *
*                 DEBUT DE LA SCENE                     *
*                                                       *
*********************************************************
*/
#declare Brique = Lego (2,2,1)

#declare Nom=Ecris_Nom ("DarkBruce",LegoQuality)
#declare RealName=Ecris_Nom ("Florent Revelut",LegoQuality)

 

object {Nom
  translate <20,0,-20>
  texture {
    TLego1
  //pigment {color Yellow}
  //finish {phong 1.0 phong_size 20}
  }
  translate (x+z)*4
  //  rotate -15
  scale 1/5
}

object {RealName
  translate <90,0,140>*.8
  texture {
    TLego2
  //pigment {color Yellow}
  //finish {phong 1.0 phong_size 20}
  }
  translate (x+z)*4
  //  rotate -15
  scale 1/5
}

light_source {1000*(y/2-z+x/2) color Red + .2*White}
light_source {1000*(y/2-z- x/2) color Blue +.2*White}
light_source {1000*(y/2-z) color Green +.2*White}

plane {
  y,0 
  texture {
    pigment {color rgb <.9,.9,1>}
    finish {
      reflection 0.8
    }   
    normal {
      bozo
      scale <.4,1,.4>
        }
  }
}


//plane {y,0 texture {pigment {checker color Black color White}} hollow no_shadow}
//plane {z,10 texture {pigment {checker color Black color White}} hollow no_shadow}
//plane {x,10 texture {pigment {checker color Black color White}} hollow no_shadow}

sky_sphere {
  pigment {
  gradient y 
  color_map {
    [0 color MidnightBlue]
    [1 color rgb <.4,0,0>]
  }
  }
}



camera {
  location <0,3,-8>*4.5
  right <16/10,1,1>
  direction 1.5*z
  look_at 1.5*y
  rotate y*20
}
