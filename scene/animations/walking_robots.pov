#include "colors.inc"
#include "textures.inc"
#include "robot.inc"

global_settings {
  ambient_light .7
  max_trace_level 20
}


//de la macro du droide

/*
La scene en elle-meme
qui correspond donc à la fin de la précédente unit...
*/

/*
#declare Objet=Droide1 (20,-20,15,-15,
  20,45,30,20,
  90,120,90,120,
  0,00,T_1,T_2)

object {Objet
  translate -3*y
  rotate y*45
  }
*/            

#declare T_1=texture {
  pigment {color Orange} 
  finish {
    phong .8 
    phong_size 20 
    metallic 
    //reflection .3
  }
}
#declare T_2=texture {
  pigment {color rgb <0,0,.6>} 
  finish {
    phong .8 
    phong_size 200 
    metallic 
    //reflection .4
  }
}
#declare T_Slip=texture {
  pigment {color rgb <0,0,.6>} 
  finish {
    phong .8 
    phong_size 200 
    metallic 
    //reflection .4
  }
}

//#declare T_1=texture {Gold_Metal}
//#declare T_1=texture {Silver_Metal}

#declare T_3= texture {
  pigment {color rgb <.7,.7,.9>}
  finish {phong 1.0 phong_size 20}
  normal {bumps 1 scale .005}
}


/* 
Pour l'animation 
*/
#macro F1 (g)
360*g*g*g-180*g*g-100*g+30
#end
#macro F2 (g)
-1440*g*g*g+1080*g*g-260*g+10
#end
#macro F3 (g)
-1980*g*g*g+1260*g*g-35*g-20
#end
#macro F4 (g)
-3960*g*g*g+3240*g*g-550*g-30
#end
#macro F5 (g)
15-5*cos(2*Pi*g)
#end
#macro F6 (g)
-80*cos(2*Pi*g)
#end
#macro F7 (g)
47.5-27.5*cos(2*Pi*g)
#end
#macro F8 (g)
62.5*(1-cos(2*Pi*g))
#end



#if (clock<=.5)
  #declare Objet=Droide2 (F1(clock),F2(clock),F3(clock),F4(clock),
    F5 (clock),F6(clock),F5(clock+.5),F6(clock+.5),
    F7(clock),F8(clock),F7(clock+.5),F8(clock+.5),
    10*cos (clock*Pi*2),0,20,20,0,0,0)
#else
  #declare Objet=Droide2 (F3(clock-.5),F4(clock-.5),F1(clock-.5),F2(clock-.5),
    F5 (clock),F6(clock),F5(clock+.5),F6(clock+.5),
    F7(clock),F8(clock),F7(clock+.5),F8(clock+.5),
    10*cos (clock*Pi*2),0,20,20,0,0,0)
#end


#local Ang_Rob=50;

union {
  #declare Temoin=-5;
  #while (Temoin<5) 
    object {Objet
      translate -3*y 
      translate z*(L_Cuisse * sin ( (F1(mod(clock , 0.5))) * Pi/180) * cos (Ang1*Pi/180)
        + L_Mollet * sin ( (F2(mod(clock , 0.5))) * Pi/180)
        - L_Pas * (div (clock,.5) + 2*Temoin))
      rotate y*(Ang_Rob)
    }
    #declare Temoin=Temoin+1;
  #end
}

plane {y,-3 
  texture {pigment {checker color Yellow color Magenta turbulence .3} scale 4}
  // texture {pigment {color Black}}
}        

/*
sky_sphere {
  pigment {
  gradient y
  color_map {
  [0 color Pink]
  [1 color Cyan]
  }
  }
  pigment {color White}  
  }*/

// background {White}

camera {location <0,6,-27>
  direction 2*z
  right <16/9,0,0>
  look_at 3.4*y
}

light_source {100*(y-z-x) color 1.2*White
  area_light 5*(-x+z)/.707,5*y,2,2 //10,10,8,8 
  jitter adaptive 2                       //10,10,5,5 peut sembler suffisant
  spotlight 
  point_at 2*y
  radius 2
  falloff 3
}                

light_source {100*(y-z+x) color 1.2*White
  area_light 5*(x+z)/.707,5*y,2,2 //8,8
  jitter adaptive 2
  spotlight 
  point_at 2*y
  radius 2
  falloff 3
}        

