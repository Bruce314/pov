#declare C=array [6] {pigment {color rgb <1,0,0>},
        pigment {color rgb <1,1,0>},
        pigment {color rgb <0,1,0>},
        pigment {color rgb <0,1,1>},
        pigment {color rgb <0,0,1>},
        pigment {color rgb <1,0,1>}}

#macro Arbre (Position,Direction,Rayon,Rapport,Rapport_R,Proba,Gravite,Ecartement,Indice,Lim)

union {
        #if (Indice=Lim)
        cylinder {Position,Position+Direction,Rayon pigment {C[Indice]}}
        #end
        sphere {Position+Direction,Rayon}
        #if (Indice<Lim)
        Arbre (Position + Direction,
                vnormalize (Direction+vrotate (Ecartement,<0,0,0>))*vlength (Direction)*Rapport,
                Rayon*Rapport_R,Rapport,Rapport_R
                Proba,Gravite,Ecartement,Indice + 1,Lim)
        Arbre (Position + Direction,
                vnormalize (Direction+Gravite)*vlength (Direction)*Rapport,
                Rayon*Rapport_R,Rapport,Rapport_R
                Proba,Gravite,Ecartement,Indice + 1,Lim)
        Arbre (Position + Direction,
                Direction+Gravite+x,
                Rayon*Rapport_R,Rapport,Rapport_R
                Proba,Gravite,Ecartement,Indice + 1,Lim)
        #end
        }
#end

#declare A1=Arbre (0,4*y,.1,1,1,0,-.3*y,5*x,0,5)

object {A1 texture {pigment {color rgb <0,0,1>}} rotate y*30}

plane {y,0 texture {pigment {checker color rgb 0 color rgb 1}}}

light_source {1000*(y-z-x/2) color rgb 1}

camera {location <0,4,-30>
        look_at 2*y
        }
                