#include "colors.inc"
#include "textures.inc"                  


/*

D�clarations de longueur
                        
*/

#declare Larg1=110.5;
#declare Haut1=76;
#declare Prof1=56;
#declare Deb1=2.5; //d�bord 1
#declare Larg2=3; //cot� du pied de carr�
#declare Plat1=2.2; //�paisseur du plateau sup
#declare Arrondi=3;
#declare Larg3=1; //largeur des bords
#declare Haut2=12; //hauteur de la lame � l'avant du meuble

/*
  
D�clarations de textures
  
*/

#declare T_Sol=texture {
        pigment {checker color White color Black}
        finish {phong .8 phong_size 25 reflection .15}
        }
        
#declare T_Bois1=texture {
        pigment {MidnightBlue}
        finish {phong .9 phong_size 20 reflection .0}                                                                
        }

#declare Bois1_Z=texture {T_Bois1}

#declare Bois1_Y=texture {T_Bois1 rotate -x*90}

#declare Bois1_X=texture {T_Bois1 rotate  y*90}

/*
  
D�clarations d'objets
  
*/

#declare Pied_G=union {
        box {<-Larg2,0,0>,<0,Haut1-Plat1,Larg2>}
        box {<0,0,-Larg2>,<Larg2,Haut1-Plat1,0>}
        intersection {
                cylinder {0,<0,Haut1-Plat1,0>,Larg2}
                box {<-Larg2,0,-Larg2>,<0,Haut1-Plat1,0>}
                }
        texture {Bois1_Y}
        }
        
#declare Pied_D=union {
        box {<-Larg2,0,-Larg2>,<0,Haut1-Plat1,0>}
        box {<0,0,0>,<Larg2,Haut1-Plat1,Larg2>}
        intersection {
                cylinder {0,<0,Haut1-Plat1,0>,Larg2}
                box {<0,0,0>,<Larg2,Haut1-Plat1,Larg2>}
                }
        texture {Bois1_Y}
        }
        
#declare Pied_A=box {<-Larg2,0,-Larg2>,<Larg2,Haut1-Plat1,Larg2>
        texture {Bois1_Y}
        }        

#declare Plateau=merge {
        box {<-Larg1/2+Arrondi,-Plat1,-Prof1/2+Arrondi>,<Larg1/2-Arrondi,0,Prof1/2>}
        box {<-Larg1/2,-Plat1,-Prof1/2+Arrondi>,<-Larg1/2+Arrondi,0,Prof1/2>}
        box {<Larg1/2-Arrondi,-Plat1,-Prof1/2+Arrondi>,<Larg1/2,0,Prof1/2>}
        box {<-Larg1/2+Arrondi,-Plat1,-Prof1/2>,<Larg1/2-Arrondi,0,-Prof1/2+Arrondi>}
        cylinder {<-Larg1/2+Arrondi,-Plat1,-Prof1/2+Arrondi>,<-Larg1/2+Arrondi,0,-Prof1/2+Arrondi>,Arrondi}
        cylinder {<Larg1/2-Arrondi,-Plat1,-Prof1/2+Arrondi>,<Larg1/2-Arrondi,0,-Prof1/2+Arrondi>,Arrondi}
        texture {Bois1_X}
        }
        
#declare Cote_Com=box {
        <-Larg3/2,0,-Prof1/2+Deb1+Larg2>,<Larg3/2,Haut1-Plat1,Prof1/2-Larg2>
        texture {Bois1_Z}
        }
        
#declare Fond=box {
        <-Larg1/2+Larg2+Deb1,0,-Larg3/2>,<Larg1/2-Larg2-Deb1,Haut1-Plat1,Larg3/2>
        texture {Bois1_X}
        }        

#declare Lame=box {        
        <-Larg1/2+Larg2+Deb1,0,-Larg3/2>,<Larg1/2-Larg2-Deb1,Haut2,Larg3/2>
        texture {Bois1_X}
        }        
        
#declare Commode=union {
        object {Plateau
                translate y*Haut1
                }        
        object {Pied_G
                translate <-Larg1/2+Deb1+Larg2,0,-Prof1/2+Deb1+Larg2>
                }
        object {Pied_D
                translate <Larg1/2-Deb1-Larg2,0,-Prof1/2+Deb1+Larg2>
                }
        object {Pied_A
                translate <Larg1/2-Deb1-Larg2,0,Prof1/2-Larg2>
                }
        object {Pied_A
                translate <-Larg1/2+Deb1+Larg2,0,Prof1/2-Larg2>
                }
        object {Cote_Com
                translate <Larg1/2-Deb1-Larg2,0,0>
                }
        object {Cote_Com
                translate <-(Larg1/2-Deb1-Larg2),0,0>
                }
        object {Fond
                translate <0,0,Prof1/2-Larg2>
                }                
        object {Lame
                translate <0,0,-Prof1/2+Larg2+Deb1>
                }                
        }
        
/*

D�claration de la sc�ne

*/


plane {y,0
        texture {T_Sol}
        }
        
object {Commode
        scale .1
        rotate -y*45
        }        
                   
camera {
        location <0,Haut1*5/40,-21>
        direction 2*z
        look_at Haut1*y/20
        }
                   
light_source {10000*(y-x/2-z) color White}
//light_source {10000*(y+x/3-z) color White}
