#include "colors.inc"
#include "textures.inc"

#declare Nounours=blob {
        threshold .6
        sphere {0,1,1 
                scale <1,2,.5>
                }
        sphere {0,1,1
                scale <.2,.2,.6>
                translate -z*.6
                rotate y*45
                rotate -x*30
                translate -y*.4
                }
        sphere {0,1,1
                scale <.2,.2,.7>
                translate -z*.7
                rotate -y*45
                rotate -x*30
                translate -y*.4
                }
        texture {pigment {color Yellow}}
        }
        
#object {Nounours}

light_source {10000*(y-z) color White}

camera {
        location <0,1,-8>
        look_at y
        }
        
                        
        