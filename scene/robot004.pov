#include "colors.inc"
#include "textures.inc"     
#include "robot03.inc"      

#macro F1 (g)
360*g*g*g-180*g*g-100*g+30
#end
#macro F2 (g)
-1440*g*g*g+1080*g*g-260*g+10
#end
#macro F3 (g)
-1980*g*g*g+1260*g*g-35*g-20
#end
#macro F4 (g)
-3960*g*g*g+3240*g*g-550*g-30
#end
#macro F5 (g)
15-5*cos(2*Pi*g)
#end
#macro F6 (g)
-80*cos(2*Pi*g)
#end
#macro F7 (g)
47.5-27.5*cos(2*Pi*g)
#end
#macro F8 (g)
62.5*(1-cos(2*Pi*g))
#end
                                                   
#declare T_Slip = texture {
        pigment {
                leopard color_map {
                [0 color Black]
                [.2 color Gray10]
                [.21 color Black]
                [.5 color Black]
                [.7 color Orange]
                [.75 color Yellow]
                [.85 color Orange]
                [1 color Black]
                }       
                turbulence .5
                scale .02
                }
        }
                                                   
#declare Objet=Droide2 (F1(clock),F2(clock),F3(clock),F4(clock),
                F5 (clock),F6(clock),F5(clock+.5),F6(clock+.5),
                F7(clock),F8(clock),F7(clock+.5),F8(clock+.5),
                10*cos (clock*Pi*2),0,20,20,0,0,-10)

#local Ang_Rob=25;

object {Objet
        translate -3*y 
        translate z*(L_Cuisse * sin ( (F1(mod(clock , 0.5))) * Pi/180) * cos (Ang1*Pi/180)
                    + L_Mollet * sin ( (F2(mod(clock , 0.5))) * Pi/180)
                      - L_Pas * (div (clock,.5)))
        rotate y*(Ang_Rob)
        }

camera {location <0,6,-27>
        direction 2*z
        look_at 3.4*y
        }

sky_sphere {pigment {color White}}
        
light_source {100*(y-z-x) color 1.2*White
        area_light 5*(-x+z)/.707,5*y,2,2 //10,10,8,8 
        jitter adaptive 2                       //10,10,5,5 peut sembler suffisant
        spotlight 
        point_at 2*y
        radius 2
        falloff 3
        }                
              
light_source {100*(y-z+x) color 1.2*White
        area_light 5*(x+z)/.707,5*y,2,2 //8,8
        jitter adaptive 2
        spotlight 
        point_at 2*y
        radius 2
        falloff 3
        }        
