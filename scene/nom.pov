#include "colors.inc"
#include "textures.inc"
#include "lego.inc"

global_settings {
	#max_trace_level  10
}

#declare Q=1;

#declare Brique1=Lego (2,2,Q)
#declare Brique2=Lego (2,5,Q)
#declare Brique3=Lego (2,4,Q)
#declare Socle=Lego (4,14,Q)


#declare LB = 4;
#declare HB = 5;

#declare T1 = texture {
	pigment {color Red transmit .8}
	finish {
		phong 1.0
		phong_size 20
		refraction on
		ior 1.33
		reflection .2
		}
	}

#declare T2 = texture {
	pigment {color Blue transmit .8}
	finish {
		phong 1.0
		phong_size 20
		refraction on
		ior 1.33
		reflection .2
		}
	}
#declare T3=texture {Gold_Metal}	



 #declare T1 = texture {pigment {color Red}}
 #declare T2 = texture {pigment {color Blue}}
 #declare T3 = texture {pigment {color Yellow}}

#declare Lettre_F = union {
	object {Brique1}
	object {Brique1 translate y*HB}
	object {Brique3 translate 2*y*HB+1*x*LB}
	object {Brique1 translate 3*y*HB}
	object {Brique2 translate 4*y*HB+1.5*x*LB}
	texture {T2}
	}
#declare Lettre_R = union {
	object {Brique1}
	object {Brique1 translate 4*x*LB}
	object {Brique1 translate y*HB}
	object {Brique1 translate y*HB+3*x*LB}
	object {Brique3 translate 2*y*HB+1*x*LB}
	object {Brique1 translate 3*y*HB}
	object {Brique1 translate 3*y*HB+3*x*LB}
	object {Brique2 translate 4*y*HB+1.5*x*LB}
	texture {T1}
	}

object {Lettre_R
	translate x*LB*2
	scale 1/5
	}
object {Lettre_F
	translate -x*LB*4
	scale 1/5
	}
object {Socle
	texture {T3}
	translate -y*HB+x*1*LB
	scale 1/5
	}

light_source {<1000,1000,-1000> color White}
light_source {<-500,1000,-1000> color White}

sky_sphere {
	pigment {
		gradient y 
		color_map {
			[0 color .4*Red]
			[1 color MidnightBlue]
			}
		}
	}

plane {y,-HB/3/5
	texture {
		pigment {Jade}
		finish {
			phong .5
			phong_size 20
			reflection .5
			}
		}
	texture {pigment {color Green}}
	hollow
	}

plane {z,4
	texture {Silver_Metal}
	texture {pigment {color White}}
	hollow
	}

camera {
	location <-1.5,2.2,-16>
	direction 2*z
	look_at 2*y+0.8*x
	}
	