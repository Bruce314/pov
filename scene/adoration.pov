#include "colors.inc"
#include "textures.inc"
#include "robot.inc"

#declare T_Stat=0; //statues textur�s?
#declare P_Stat=0; //statues pr�sentes  
#declare T_Col=1;  //Colonnes texture�s
#declare Beau_Sol=1; //Sol haute qualit�?
#declare P_Prieurs = 0; //les prieurs sont pr�sents?

#declare Haut1=6;
#declare Haut2=2;
#declare Haut3=.5;
#declare Ray1=.6;
#declare Ray2=.3;        
#declare n1=5;
#declare n2=7;                          
#declare Larg1=5;
#declare Long1=5;
#declare Ray3 = .05;
#declare Lim=24;
                                         
#declare T_Sol_1=texture {
        pigment {Jade}
        finish {Shiny}
        normal {quilted 1 control0 0 control1 0 scale <1,.1,1>}
        }
#declare T_Sol_2=texture {
        pigment {Sapphire_Agate scale .3}
        finish {Shiny}
        normal {quilted 1 control0 0 control1 0 scale <1,.1,1>}
        }
                                         
                                         
#if (T_Stat = 1)
#declare T_1=texture  {
        pigment {White_Marble scale .5}
        finish {phong 1.0 phong_size 20 reflection .3}
        }
#else
#declare T_1=texture {pigment {Yellow}}
#end

#declare T_2=texture  {T_1}
#declare T_3=texture  {T_1}
#declare Pas_de_Logo=1;        

#declare Robot_Statue = Droide1 (10,-10,10,-10, //jambes         
                                110,45,15,85, //humerus
                                135, 70,100,150, //radius
                                0,5,10,10)  //bassin et mains                      
#undef T_1
#undef Pas_de_Logo
#undef T_3
#undef T_2



#declare Colonne=union {
        #local Rayon_H = Ray1 + (Ray2-Ray1)*Haut2/Haut1;
        #if (P_Stat = 1)
        object {Robot_Statue
                scale .2
                rotate y*90
                translate <-Rayon_H*1.5,Haut2,0>
                }
        object {Robot_Statue
                scale .2
                rotate -y*90
                translate <Rayon_H*1.5,Haut2,0>
                }
        #end                
        intersection {
                sphere {0,1        
                        scale <2*Rayon_H,Haut3,Rayon_H>
                        }
                plane {y,0}
                translate Haut2*y
                }                        
        cone {0,Ray1,Haut1*y,Ray2 }
        #declare Tem = 0;
        #while (Tem < Lim)             
                cylinder {<Ray1,0,0>,<Ray2,Haut1,0>,Ray3
                        rotate y*Tem*360/Lim
                        }
                #declare Tem = Tem + 1;
                #end
        torus {Ray1,Ray3*1.5}          
        }
        
#declare Temple = union {        
        #declare Tem_x=-n1/2;
        #while (Tem_x<=n1/2)
                #declare Tem_z=-n2/2;
                #while (Tem_z<=n2/2)
                        object {Colonne
                                translate <Larg1*Tem_x,0,Long1*Tem_z>
                                #if (T_Col = 1) 
                                texture {
                                        pigment {Red_Marble scale .1}
                                        finish {phong .5 phong_size 35}
                                        }
                                #else                                        
                                texture {pigment {color Red}}
                                #end
                                }    
                                
                        #declare Tem_z = Tem_z + 1;                        
                        #end                       
                #declare Tem_x=Tem_x+1;                
                #end
        }

#if (P_Prieurs = 1)                    

#declare Robot_Priant = Droide1 (60,-102,60,-102,90,45,90,45,100,120,100,120,0,-60,10,10)                        

#declare Croyants = union {        
        #declare Tem_x=-n1/2;
        #while (Tem_x<n1/2)
                #declare Tem_z=-n2/2;
                #while (Tem_z<=n2/2)
                        object {Robot_Priant
                                rotate y*180
                                scale .3
                                translate <Larg1*(Tem_x+1/2),0,Long1*(Tem_z+1/2)>
                                }
                        #declare Tem_z = Tem_z + 1;                        
                        #end                       
                #declare Tem_x=Tem_x+1;                
                #end
        }
#end

#if (P_Prieurs =1)
object {Croyants }
#end



object {Temple }

        
plane {y,0                               
        #ifdef (Beau_Sol)
        texture {checker texture {T_Sol_1} texture {T_Sol_2}
                turbulence .001
                }
        #else
        texture {pigment {checker color Black color White}}
        #end
        }
        
// sphere {0,1 texture {pigment {color NeonPink}}}                                                          
        
camera {
        location <Larg1/2-1.5*Ray1,2,-12>
        look_at <0,2,0>
        }
        
light_source {10000*(x/2+y-z) color White}
light_source {10000*(-x/2+y-z) color White}
light_source {<0,1,-10000> color White}                                                                