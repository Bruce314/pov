#include "colors.inc"
#include "textures.inc"

#declare T_Mur=
        texture {
                pigment { color Gray70}
                }

plane {y,0
        texture {
                pigment {checker color White color Black} //Le sol infini
                }
        }                
        
#declare Colonne=union {
        torus {1.5,.3 translate <0,.3,0>}
        cone {0,1.6,<0,3,0>,1.3}
        cylinder {<0,3,0>,<0,6,0>,1.3}
        torus {1.2,.3 translate <0,5.6,0>}
        texture {T_Mur}
        }
        
#declare Colonnade=union {
        object {Colonne translate <-8,0,12>}
        object {Colonne translate <-8,0,4>}
        object {Colonne translate <-8,0,-4>}
        object {Colonne translate <-8,0,-12>}
        object {Colonne translate <8,0,12>}
        object {Colonne translate <8,0,4>}
        object {Colonne translate <8,0,-4>}
        object {Colonne translate <8,0,-12>}
        object {Colonne translate <0,0,12>}
        object {Colonne translate <0,0,-12>}
        }
        
object {Colonnade}

light_source {100000*(x/2-z+y) color White}

camera {
        location <-8,1.7,-30>
        look_at <0,.5,0>
        }