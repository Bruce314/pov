#include "colors.inc"
#include "textures.inc"

#declare F_Truc = finish {
    Shiny          
    reflection 0.5
}

#include "genere.inc"


plane {z,2
    texture {
        pigment {
            color White
        }                
        finish {
            reflection 0.9
        
        }
    }
} 
  
  /*
sphere {0,7
    texture {Silver_Texture }
    no_shadow
    
}   
*/                

object {monTruc       
rotate z*12
}
    

sphere {0,.1 
    texture {
        Gold_Texture
    }               
}

light_source {<0,0,-25> color Red
    spotlight 
    radius 4
    falloff 7
    point_at 0
    }
                                                 
light_source {<0,0,-25> color Green
    spotlight 
    radius 4
    falloff 7
    point_at 0 
    rotate -y*30
    }

light_source {<0,0,-25> color Blue
    spotlight 
    radius 4
    falloff 7
    point_at 0
    rotate y*30
    }
                                                 
camera {
    location <0,0.5,-3>
    look_at 0
}