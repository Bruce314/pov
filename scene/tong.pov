#include "colors.inc"
#include "textures.inc"
#include "robot.inc"
#include "matrices.inc"

//Vue : 1, dessus
//  2, face 
//  3, c�t�
//  4 : normale
#declare Vue = 4;

#declare E_Sem = .15;

#declare Semelle= union {
  cylinder {<-0.9,0,.5>,<-.9,E_Sem,.5>,.2}
  cylinder {<-1.4,0,-1.2>,<-1.4,E_Sem,-1.2>,.2}
  cylinder {<-0.9,0,-1.2>,<-.9,E_Sem,-1.2>,.2}
  box {<-1.1,0,-1.2>,<-0.7,E_Sem,.5>}
  box {<-1.4,0,-1.4>,<-.9,E_Sem,-1>}
  box {<-1.1,0,.5-sqrt(1.7*1.7+0.5*0.5)>,<-0.7,E_Sem,.5>
    translate -<-0.9,0,.5>
    rotate y*atan2(5,17)*180/pi
    translate <-0.9,0,.5>
    }
  
  }

#declare Laniere1 = union {
  intersection {
    torus {1,.1
      rotate x*90
      }
    box {<0,0,-1>,<2,2,1>}
    translate -x
    }
  sphere {0,.1}
  rotate -y*(90-atan2(2.2,7.5)*180/pi)
  scale <sqrt(0.22*0.22+0.75*0.75),.25,sqrt(0.22*0.22+0.75*0.75)>
  translate <-.75,E_Sem,-.5>
  }

#declare Laniere2 = union {
  intersection {
    torus {1,.1
      rotate x*90
      }
    box {<-2,0,-1>,<0,2,1>}
    translate x
    }
  sphere {0,.1}
  rotate y*(90-atan2(4.2,7.5)*180/pi)
  scale <sqrt(0.42*0.42+0.75*0.75),.25,sqrt(0.42*0.42+0.75*0.75)>
  translate <-1.35,E_Sem,-.5>
  }

#declare Pic = union {
  cylinder {E_Sem*y,(E_Sem+.25)*y,.02}
  sphere {(E_Sem+.25)*y,.08}
  translate <-1.35+.42,0,-.5-.75>
  }

#declare Tong=union {
  object {Semelle}
  object {Laniere1 texture {pigment {color White}}}
  object {Laniere2 texture {pigment {color NeonPink}}}
  object {Pic}
  }

#declare Chaussure = object {Tong texture {pigment {color Blue}}}


#declare Droide = Droide2 (0,0,0,0,00,0,0,00,0,00,0,0,00,0,0,0,0,0,0)

union {
  object {Droide
    translate (E_Sem-.05)*y
    }
  object {Chaussure}
  object {Chaussure
    matrix Transf (-x,y,z)
    }
  #switch (Vue)
  #case (1)
    rotate y*180
    rotate -x*90
    translate (-E_Sem-.2)*z
    #break
  #case (3)
    rotate -y*90
    #break
  #case (4)
    rotate -y*45
    #break
  #end
  }

#declare Sol1= texture {pigment {checker color Yellow color Red} scale 1/10}
#declare Sol2= texture {pigment {checker color White color Black} scale 1/10}

#switch (Vue)
#case (2)
#case (3)
#case (4)
  plane {y,0
    texture {checker texture {Sol1} texture {Sol2}}
    }
  #break
#end

#switch (Vue)
#case (1)
#case (2)
  plane {z,0
    texture {checker texture {Sol2} texture {Sol1}}
    hollow
    }
  #break
#end
                       
light_source {1000*(y-z-x/2) color White}
light_source {1000*(y-z) color .2*Red}
light_source {1000*(y-z+x/2) color .3*Yellow}

camera {location <0,1,-5>
        look_at 0
        }                
                
        
                