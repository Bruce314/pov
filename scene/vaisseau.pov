#include "textures.inc"
#include "colors.inc"


#declare T_Vaisseau = texture {
        bozo 
        texture_map {
                [0 Brass_Metal]
                [1 Silver_Metal]
                }               
        scale .1
        }
#declare T_Cockpit1 = texture {
        pigment {color rgbf <.8,.8,1,.9>}
        finish {phong 1.0
                phong_size 20
                ior 4.33
                refraction on
                reflection .2
                irid {
                        .5
                        thickness .001                        
                        }
                }
        }
#declare T_Cockpit2 = texture {Gold_Metal}        

#declare L=100;

#declare Reacteur = union {
        difference {
                cylinder {0,x,.07}
                sphere {2*x,sqrt(1+.07*.07)}
                }
        sphere {0,1                 
                hollow
                texture {pigment {color Clear}}
                interior {
                        media {
                                emission
                                density {
                                        spherical
                                        color_map {
                                                 [0 color Red]
                                                 [1 color Yellow]
                                                 }
                                        }
                                }
                        }
                scale <.8,.07,.07>
                translate x
                }                
        }                

#declare Aile = difference {
        intersection {
                // cylinder {-3.5*z-0.1*y,-3.5*z+0.1*y,5}
                sphere {-L*y,sqrt (L*L+5*5)}
                sphere {L*y,sqrt (L*L+5*5)}
                translate -3.5*z                
                }
        union {             
                cylinder {<0,-10,-65/4>,<0,10,-65/4>,65/4}
                plane {-x,0}
                plane {z,-1}
                }
        }
                 

#declare Corps= difference {
        merge {
                cylinder {0,x,1/2}
                sphere {0,1/2}
                cone {<-2,-1/2,0>,0,0,1/2}
                }
        sphere {5*x,sqrt ((5-1)*(5-1)+1/4)}
        
        }
        
#declare Cockpit =  union {
        sphere {0,1
                scale <.7,.2,.1>
                rotate z*30
                translate <-1.08,-.05,0>
                texture {T_Cockpit1}
                }        
        torus {1,.05
                scale <.7,.2,.1>
                rotate z*30
                translate <-1.08,-.05,0>
                texture {T_Cockpit2}
                }        
        torus {1,.05
                rotate z*90
                scale <.7,.2,.1>
                rotate z*30
                translate <-1.08,-.05,0>
                texture {T_Cockpit2}
                }        
        torus {sqrt(3)/2,.05
                rotate z*90    
                translate .5*x
                scale <.7,.2,.1>
                rotate z*30
                translate <-1.08,-.05,0>
                texture {T_Cockpit2}
                }        
        torus {sqrt(3)/2,.05
                rotate z*90
                translate -.5*x
                scale <.7,.2,.1>
                rotate z*30
                translate <-1.08,-.05,0>
                texture {T_Cockpit2}
                }        
        torus {1,.1
                rotate -x*90
                scale <.7,.2,.1>
                rotate z*30
                translate <-1.08,-.05,0>
                texture {T_Cockpit2}
                }        
        }                                        
        
#declare Vaisseau = union { 
        object {Cockpit}
        object {Corps}
        object {Aile 
                translate 1/4*x
                rotate -y*90
                scale <1/1.5,1/2,1/2>
                translate .7*x
                rotate x*90
                }
        object {Aile 
                translate 1/4*x
                rotate -y*90
                scale <1/2,1/2,2/3>
                translate .7*x
                rotate x*(90-100)
                }
        object {Aile 
                translate 1/4*x
                rotate -y*90
                scale <1/2,1/2,2/3>
                translate .7*x
                rotate x*(90+100)
                }
        object {Reacteur translate .4*x}                
        #declare Temoin=0;
        #while (Temoin<7)
                object {Reacteur translate .5*x+.25*y rotate x*Temoin*360/7}
                #declare Temoin=Temoin+1;                
                #end
        texture {T_Vaisseau}               
        }        


cylinder {0,x,.05 texture {pigment {color Yellow}}}
cylinder {0,y,.05 texture {pigment {color Blue}}}
cylinder {0,z,.05 texture {pigment {color Green}}}
        
object {Vaisseau
        rotate y*75
    //    texture {pigment {color Red}}
        }

light_source {1000*(y-z) color White}

 // plane {y,0 texture {pigment {checker color Black color White scale 1/2}}}

camera {location <0,0,-5>
        look_at 0
        }                