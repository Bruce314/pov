/* Future include Ordi */
/* en cours de cr�ation du meuble */

 #include "colors.inc"
 #include "textures.inc"
#declare T_Meub_Y=texture {DMFWood4
                scale .1
                rotate x*90
                }
#declare T_Meub_X=texture {T_Meub_Y rotate z*90}
#declare T_Meub_Z=texture {T_Meub_Y rotate x*90}     
#declare T_Roulette=texture {pigment {color Yellow}}

#declare T_Chaise_Y=texture {T_Meub_Y}
#declare T_Chaise_X=texture {T_Meub_X}


#declare Ep_Planches=.02;
#declare Ep_ContrP=.01;
#declare L1=.9;
#declare L2=.07;
#declare H1=1.1;
#declare H2=.7;
#declare H3=.65;
#declare H4=.07;
#declare P1=.4;
#declare P2=.6;

#declare Roulette=union {
        cylinder {H4/2*y,H4*y,Ep_Planches/3 texture {T_Roulette}}
        torus {5/18*H4,5/36*H4
                rotate z*90
                translate <Ep_Planches/3+5*H4/36,3/2*5/18*H4,0>
                texture {T_Roulette}
                }
        torus {5/18*H4,5/36*H4
                rotate z*90
                translate <-Ep_Planches/3-5*H4/36,3/2*5/18*H4,0>
                texture {T_Roulette}
                }
        cylinder {<Ep_Planches/3+5*H4/36,3/2*5/18*H4,0>,<-Ep_Planches/3-5*H4/36,3/2*5/18*H4,0>,2/9*H4 texture {T_Roulette}}                
        }

#declare Meuble=union {
        box {<L1/2,H4+L2,-P1/2-L2/2>,<L1/2+Ep_Planches,H1,-P1/2+L2/2> texture {T_Meub_Y}} //les 4 montants verticaux
        box {<L1/2,H4+L2,P1/2-L2/2>,<L1/2+Ep_Planches,H1,P1/2+L2/2> texture {T_Meub_Y}}
        box {<-L1/2-Ep_Planches,H4+L2,-P1/2-L2/2>,<-L1/2,H1,-P1/2+L2/2> texture {T_Meub_Y}}
        box {<-L1/2-Ep_Planches,H4+L2,P1/2-L2/2>,<-L1/2,H1,P1/2+L2/2> texture {T_Meub_Y}}
        
        box {<-L1/2-Ep_Planches,H4,P1/2+L2/2-P2>,<-L1/2,H4+L2,P1/2+L2/2> texture {T_Meub_Z}} //les deux planches qui forment le spieds
        box {<L1/2,H4,P1/2+L2/2-P2>,<L1/2+Ep_Planches,H4+L2,P1/2+L2/2> texture {T_Meub_Z}}
        
        box {<-L1/2,-Ep_Planches/2+H4+L2/2,-L2/2+P2/6>,<L1/2,Ep_Planches/2+H4+L2/2,L2/2+P2/6> texture {T_Meub_X}} // la planche pour poser ses pieds
        
        object {Roulette translate <L1/2+Ep_Planches/2,0,P1/2>}  //les 4 mignones petites roulettes
        object {Roulette translate <-L1/2-Ep_Planches/2,0,P1/2>}
        object {Roulette translate <L1/2+Ep_Planches/2,0,P1/2-P2+L2>}
        object {Roulette translate <-L1/2-Ep_Planches/2,0,P1/2-P2+L2>}
        }

#declare Haut_As=.5;
#declare Larg=.35;
#declare Haut_Mt=1;
#declare Ep_Pl=.01;
#declare Ep_Pi=.05;   
#declare Haut_Ds=.2;

#declare Chaise=union {
        box {<-Larg/2-Ep_Pi/2,-Ep_Pl,-Larg/2-Ep_Pi/2>,<Larg/2+Ep_Pi/2,Ep_Pl,Larg/2>     //l'assise 
                texture {T_Chaise_X}
                translate <0,Haut_As,0>
                }
        box {<-Ep_Pi/2,0,-Ep_Pi/2>,<Ep_Pi/2,Haut_As,Ep_Pi/2>    //les pieds de derri�re
                texture {T_Chaise_Y}
                translate <-Larg/2,0,-Larg/2>
                }
        box {<-Ep_Pi/2,0,-Ep_Pi/2>,<Ep_Pi/2,Haut_As,Ep_Pi/2>
                texture {T_Chaise_Y}
                translate <Larg/2,0,-Larg/2>
                }
        box {<-Ep_Pi/2,0,-Ep_Pi/2>,<Ep_Pi/2,Haut_Mt,Ep_Pi/2>                 //les pieds de devant
                texture {T_Chaise_Y}                                           
                translate <Larg/2,0,Larg/2>
                }
        box {<-Ep_Pi/2,0,-Ep_Pi/2>,<Ep_Pi/2,Haut_Mt,Ep_Pi/2>
                texture {T_Chaise_Y}
                translate <-Larg/2,0,Larg/2>
                }
        box {<-Larg/2,-Haut_Ds,-Ep_Pl>,<Larg/2,0,Ep_Pl> //la planche de dossier
                texture {T_Chaise_X}
                translate <0,Haut_Mt,Larg/2>
                }
        box {<-Larg/2+Ep_Pi/2,Haut_As-Ep_Pi-Ep_Pl/2,-Larg/2>,<Larg/2-Ep_Pi/2,Haut_As-Ep_Pl/2,-Larg/2+Ep_Pi/2>
                texture {T_Chaise_X}
                rotate y*0
                }
        box {<-Larg/2+Ep_Pi/2,Haut_As-Ep_Pi-Ep_Pl/2,-Larg/2>,<Larg/2-Ep_Pi/2,Haut_As-Ep_Pl/2,-Larg/2+Ep_Pi/2>
                texture {T_Chaise_X}
                rotate y*90
                }
        box {<-Larg/2+Ep_Pi/2,Haut_As-Ep_Pi-Ep_Pl/2,-Larg/2>,<Larg/2-Ep_Pi/2,Haut_As-Ep_Pl/2,-Larg/2+Ep_Pi/2>
                texture {T_Chaise_X}
                rotate y*180
                }
        box {<-Larg/2+Ep_Pi/2,Haut_As-Ep_Pi-Ep_Pl/2,-Larg/2>,<Larg/2-Ep_Pi/2,Haut_As-Ep_Pl/2,-Larg/2+Ep_Pi/2>
                texture {T_Chaise_X}
                rotate y*270
                }
        }
        
object {Chaise
        rotate -30
        scale 10
        }        

/*
object {Meuble 
        rotate y*45
        scale 10
        }
        */
        
camera {
        location <1.5,3,-20>
        direction 2*z
        look_at 2*y
        }
        
#declare T_Sol=texture {
        pigment {checker color Red color Black}
        finish {diffuse .7
                reflection .2
                phong 1.0
                phong_size 20
                }
        }
background {color White}        
        
plane {y,0 texture {T_Sol}}
        
light_source {100000*(y-z/2) color White}        
        