#include "colors.inc"

#version 3.5;

#declare Photons=on;

// place all settings of globally influenced features here
global_settings {
  assumed_gamma 1.0
  // [GLOBAL ITEM(S)]
  // used in global_settings, sets the maximum ray tracing bounce depth (1 or more) [5]
  max_trace_level 15
  #if (Photons)          // global photon block
    photons {
      spacing 0.02                 // specify the density of photons
    }
  #end
}



camera {
  location  <1, -1,-6>
  direction <0, 0,  1>
  up        <0, 1,  0>
  right   <4/3, 0,  0>
  look_at   <0.5, 0.3, 0>
}

light_source {<10, 10, -15> color Red}
light_source {<0, 20,0> color Blue} 
light_source {<-5,-5,-8> color Green}
background{color Pink}

#declare Param=1;

object {
  poly { 6,<8, 0, 0, 0, 12, 0, 0, 12, 0, -12, 0, 0, 0, 
    0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 12, 0, -12, 0, 0, 0, 
    0, 6, -0.100000, -12, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 3, 0, 
    -3, 0, 0, 0, 0, 3, -1, -6, 0, 3, 0, 0, 0, 0, 0, 0, 
    1, 0, -3, 0, 3, 0, -Param> 
    
    material {
      texture {
        pigment {Pink filter .8}
        finish {
          ambient 0.0
          diffuse 0.05
          specular 0.6
          roughness 0.005
          reflection {
            0.1, 1.0
            fresnel on
          }
          conserve_energy
        }
      }
      interior {
        ior 1.5
        fade_power 1001
        fade_distance 0.9
        fade_color <0.5,0.8,0.6>
      }
    }
   
    rotate <0 90 0>
    rotate < 0, 0, 100>
    translate <0.01, 0.1, 0.01>
    scale 1.5
  }
  //bounded_by{sphere{<0,0,0>,1 }}
}

plane {z,5 
  texture {
    pigment {
      checker color White color Black
    }
  }
}

