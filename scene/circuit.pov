#include "colors.inc"
#include "textures.inc"
#include "matrices.inc"
#include "chaine.inc"

/* 
d�claration des param�tres
*/
#declare Temps = clock;
#declare Af_Ciel = 1;
#declare Precision = 1500;
#declare Choix_Piste = 0; //les indices commencent � 0           
        //0 : ovale standard
        //1 : ovale avec tourbillons
        //2 : ovale avec tourbillon et looping
        //3 : Mo�bius
        //4 : piste en 8                

/* 
D�claration des textures
*/
#declare T_Piste1= texture {pigment {color Yellow}}
#declare T_Bord1 = texture {
        pigment {color Blue}
        finish {phong 1.0
                phong_size 20
                reflection .6
                }
        }                
#declare T_Bord2 = texture {
        pigment {color Red}
        finish {phong 1.0
                phong_size 20
                reflection .6
                }
        }                

/*
Fonction de perturbation
*/

#macro Perturb (s)
#local Num=32*s*s*s*(s-1)*(s-1)*(s-1)*(2*s+sqrt(17)-5)*(2*s-sqrt(17)-5);
#local Denom=256+s*(-2560+s*(10592+s*(-23200+s*(29033+s*(-21636+s*(10262+s*(-2916+425*s)))))));
Num/Denom
#end

/*
D�claration du vaisseau qui coursera
*/
#declare T_Vaisseau = texture {
        bozo 
        texture_map {
                [0 Brass_Metal]
                [1 Silver_Metal]
                }               
        scale .1
        }
#declare T_Cockpit1 = texture {
        pigment {color rgbf <.8,.8,1,.9>}
        finish {phong 1.0
                phong_size 20
                ior 4.33
                refraction on
                reflection .2
                irid {
                        .5
                        thickness .001                        
                        }
                }
        }
#declare T_Cockpit2 = texture {Gold_Metal}        

#declare L=100;

#declare Reacteur = union {
        difference {
                cylinder {0,x,.07}
                sphere {2*x,sqrt(1+.07*.07)}
                }
        sphere {0,1                 
                hollow
                texture {pigment {color Clear}}
                interior {
                        media {
                                emission
                                color Red
                                }
                        }
                scale <.8,.07,.07>
                translate x
                }                
        }                

#declare Aile = difference {
        intersection {
                // cylinder {-3.5*z-0.1*y,-3.5*z+0.1*y,5}
                sphere {-L*y,sqrt (L*L+5*5)}
                sphere {L*y,sqrt (L*L+5*5)}
                translate -3.5*z                
                }
        union {             
                cylinder {<0,-10,-65/4>,<0,10,-65/4>,65/4}
                plane {-x,0}
                plane {z,-1}
                }
        }
                 

#declare Corps= difference {
        merge {
                cylinder {0,x,1/2}
                sphere {0,1/2}
                cone {<-2,-1/2,0>,0,0,1/2}
                }
        sphere {5*x,sqrt ((5-1)*(5-1)+1/4)}
        
        }
        
#declare Cockpit =  union {
        sphere {0,1
                scale <.7,.2,.1>
                rotate z*30
                translate <-1.08,-.05,0>
                texture {T_Cockpit1}
                }        
        torus {1,.05
                scale <.7,.2,.1>
                rotate z*30
                translate <-1.08,-.05,0>
                texture {T_Cockpit2}
                }        
        torus {1,.05
                rotate z*90
                scale <.7,.2,.1>
                rotate z*30
                translate <-1.08,-.05,0>
                texture {T_Cockpit2}
                }        
        torus {sqrt(3)/2,.05
                rotate z*90    
                translate .5*x
                scale <.7,.2,.1>
                rotate z*30
                translate <-1.08,-.05,0>
                texture {T_Cockpit2}
                }        
        torus {sqrt(3)/2,.05
                rotate z*90
                translate -.5*x
                scale <.7,.2,.1>
                rotate z*30
                translate <-1.08,-.05,0>
                texture {T_Cockpit2}
                }        
        torus {1,.1
                rotate -x*90
                scale <.7,.2,.1>
                rotate z*30
                translate <-1.08,-.05,0>
                texture {T_Cockpit2}
                }        
        }                                        
        
#declare Vaisseau = union { 
        object {Cockpit}
        object {Corps}
        object {Aile 
                translate 1/4*x
                rotate -y*90
                scale <1/1.5,1/2,1/2>
                translate .7*x
                rotate x*90
                }
        object {Aile 
                translate 1/4*x
                rotate -y*90
                scale <1/2,1/2,2/3>
                translate .7*x
                rotate x*(90-100)
                }
        object {Aile 
                translate 1/4*x
                rotate -y*90
                scale <1/2,1/2,2/3>
                translate .7*x
                rotate x*(90+100)
                }
        object {Reacteur translate .4*x}                
        #declare Temoin=0;
        #while (Temoin<7)
                object {Reacteur translate .5*x+.25*y rotate x*Temoin*360/7}
                #declare Temoin=Temoin+1;                
                #end
        texture {T_Vaisseau}               
        scale 1/4
        }        


/*
D�claration des macros relatives � des circuits
*/
#declare Rayon = .8;
#declare Droit = 3;      

#macro Piste_Huit (s)  
#local Rayon_8 = vlength (<0,0,2>-<3/4,0,3/2>);
#local Longueur_8 = 3*sqrt(5)/2;
#local A_Max=180/pi*atan2(3,2);           
#local Hauteur = .2;
#local Arc = Rayon*(360-2*A_Max)/180*pi;
#local Longueur = 2*Arc + 2*Longueur_8;
#local tps = (s-floor(s))*Longueur;

#switch (tps)
#range (0,Longueur_8)
        #local Angle=tps/Longueur_8;
        #declare Pos = <-3/4+3/2*Angle,Hauteur/2*(1-cos (2*pi*Angle)),-3/2+3*Angle>;
       // #debug concat ("Droit 1 :",str(Angle,6,6),chr(10))
        #break
#range (Longueur_8, Longueur_8+Arc)
        #local Angle = (-90+A_Max) + (tps-Longueur_8)/Arc*(360-2*A_Max);
        #declare Pos = <0+Rayon_8*cos(2*pi*Angle/360),0,2+Rayon_8*sin(2*pi*Angle/360)>;
        #break
#range (Longueur_8+Arc,Longueur_8*2+Arc)
        #local Angle=(tps-Longueur_8-Arc)/Longueur_8;   
//        #debug concat ("Droit 2 :",str(Angle,6,6),chr(10))
        #declare Pos = <-3/4+3/2*Angle,-Hauteur/2*(1-cos (2*pi*Angle)),3/2-3*Angle>;
        //#declare Pos = <Angle,Angle,-Angle>;
        #break
#range (Longueur_8*2+Arc, Longueur)
        #local Angle = (-90+A_Max) + (tps-(Longueur_8*2+Arc))/Arc*(360-2*A_Max);
        #declare Pos = <0+Rayon_8*cos(2*pi*Angle/360),0,-2-Rayon_8*sin(2*pi*Angle/360)>;
        #break
        #end
Pos
#end        
                 


#macro Piste_Moebius (s)
<-Rayon*cos(4*pi*s),Rayon/2*sin(8*pi*s),Rayon*sin(4*pi*s)>
#end

#macro Piste_Ovale_Looping (s)
#declare Arc = pi*Rayon;                          
#declare Arc_Loop = pi/2*3*Droit/8;
#declare Arc_Haut = pi*Droit/8;
#declare Looping = Droit/2+2*Arc_Loop+Arc_Haut;
#local Longueur = 2*Arc +  Droit +Looping;
#local tps=(s-floor (s))*Longueur;
#switch (tps)  
#range (0,Droit)
        #declare Pos = <-Rayon,0,-Droit/2+tps>;
        #break
#range (Droit,Droit+Arc)
        #declare Pos = <-Rayon * cos ((tps-Droit)*pi/Arc),0,Droit/2+ Rayon * sin ((tps-Droit)*pi/Arc)>;
        #break
#range (Droit+Arc,Droit+Arc+Looping)  
        #declare tps2 = tps-Droit-Arc;
        #switch (tps2)
        #range (0,Droit/4) //le bout de droit
                #declare Pos = <Rayon-Droit/16+Droit/16*cos (tps2*4*pi/Droit), 0, Droit/2 - tps2>;
                #break
        #range (Droit/4,Droit/4+Arc_Loop) //on monte       
                #local Angle = (tps2-Droit/4) / (Arc_Loop);
                #declare Pos = <Rayon-Droit/8,3*Droit/8*(1-cos (pi/2*Angle)),Droit/4-3*Droit/8*sin(pi/2*Angle)>;
                //#debug concat ("mont�e looping ",str(Angle,5,5)," ",str(Pos.y,6,6),chr(10))
                #break
        #range (Droit/4+Arc_Loop,Droit/4+Arc_Loop+Arc_Haut) //on tourne                 
                #declare Angle = (tps2-(Droit/4+Arc_Loop))/(Arc_Haut);
                #declare Pos = <Rayon+Droit/8-Droit/4*Angle,3*Droit/8+Droit/8*sin (pi*Angle),Droit/8*cos(pi*Angle)>;
                #declare Pos = <Rayon+Droit/8*cos(pi*(1-Angle)),3*Droit/8+Droit/8*sin (pi*(1-Angle)),Droit/8*cos(pi*(1-Angle))>;
                //#debug concat ("looping haut ",str(Angle,6,6)," ",str(Pos.y,6,6),chr(10))
                #break
        #range (Droit/4 + Arc_Loop + Arc_Haut , Droit/4 + Arc_Loop + Arc_Haut + Arc_Loop) //on descend    
                #local Angle = (tps2-(Droit/4 + Arc_Loop+Arc_Haut)) / (Arc_Loop);
                #declare Pos = <Rayon+Droit/8,3*Droit/8*(1-sin(pi/2*Angle)),-Droit/4+3*Droit/8*cos (pi/2*Angle)>;
              //  #debug concat ("descente looping ",str(Angle,5,5)," ",str(Pos.y,6,6),"Angle ",str (Angle,6,6),chr(10))
                #break
        #range (Droit/4+2*Arc_Loop+Arc_Haut,Looping) //le bout de droit        
                #local Angle = tps2-Looping+Droit/4;
                #declare Pos = <Rayon+Droit/16+Droit/16*cos(Angle*4*pi/Droit) ,0,-Droit/4-Angle>;
             //   #debug concat ("droit looping ",str(Pos.x,5,5)," ",str(Pos.y,6,6)," ",str(Pos.z,6,6)," Angle ",str(Angle,6,6),chr(10))
        #end   
        #break
#range (Looping+Droit+Arc,Longueur)   
        // #debug concat ("passage dans l'arc ",str ((tps-Longueur+Arc)/Arc,6,6),chr(10))
        #declare Pos = <Rayon*cos ((tps-Droit-Looping-Arc)*pi/Arc),0,-Droit/2-Rayon*sin ((tps-Looping-Droit-Arc)/Arc*pi)>;
        #break
#end  
// #debug concat ("Sortie ",str(tps-Looping-Droit-Arc,6,6)," POS : ",str (Pos.x,5,5)," ",str(Pos.y,6,6)," ",str (Pos.z,6,6),,chr(10))
Pos
#end


#macro Piste_Ovale (s)
#declare Arc = pi*Rayon;
#local Longueur = 2*Arc + 2* Droit;
#local tps=(s-floor (s))*Longueur;
#switch (tps)  
#range (0,Droit)
        #declare Pos = <-Rayon,0,-Droit/2+tps>;
        #break
#range (Droit,Droit+Arc)
        #declare Pos = <-Rayon * cos ((tps-Droit)*pi/Arc),0,Droit/2+ Rayon * sin ((tps-Droit)*pi/Arc)>;
        #break
#range (Droit+Arc,2*Droit+Arc)
        #declare Pos = <Rayon,0,Droit/2-(tps-Droit-Arc)>;
        #break
#range (2*Droit+Arc,Longueur)
        #declare Pos = <Rayon*cos ((tps-2*Droit-Arc)*pi/Arc),0,-Droit/2-Rayon*sin ((tps-2*Droit-Arc)/Arc*pi)>;
        #break
#local Pos = 0; 
#end                
Pos
#end

/*
d�claration des normales des pistes
*/  
   
#macro Normale_Huit (s)
#local Rayon_8 = vlength (<0,0,2>-<3/4,0,3/2>);
#local Longueur_8 = 3*sqrt(5)/2;
#local A_Max=180/pi*atan2(3,2);           
#local Hauteur = .2;
#local Arc = Rayon*(360-2*A_Max)/180*pi;
#local Longueur = 2*Arc + 2*Longueur_8;
#local tps = (s-floor(s))*Longueur;  
#declare Coef = 1;

#switch (tps)
#range (0,Longueur_8)
        #local Angle=tps/Longueur_8;
        #declare Pos = y*cos (2*pi*Angle) + x*sin (2*pi*Angle);
        #break
#range (Longueur_8, Longueur_8+Arc)
        #local Angle = (tps-Longueur_8)/Arc;
        #local Vect = vnormalize (-Piste (s)+<0,0,2>);
        #declare Pos = vnormalize (y+Coef/2*Vect*(1-cos (2*pi*Angle)));
//        #declare Pos = y;
        #break
#range (Longueur_8+Arc,Longueur_8*2+Arc)
        #local Angle=(tps-Longueur_8-Arc)/Longueur_8;   
        #declare Pos = y;
        #break
#range (Longueur_8*2+Arc, Longueur)
        #local Angle = (tps-(Longueur_8*2+Arc))/Arc;
        #local Vect = vnormalize (-Piste (s)+<0,0,-2>);
        #declare Pos = vnormalize (y+Coef/2*Vect*(1-cos (2*pi*Angle)));
        #break
        #end
Pos
#end   
   
#macro Normale_Moebius (s)
#local V=vnormalize (Piste_Moebius (s));
vnormalize (y*cos(2*pi*s)+V*sin(2*pi*s))
#end

#macro Normale_Ovale_Tourbillon (s) 
#declare Coef = 1;
#declare Arc = pi*Rayon;
#local Longueur = 2*Arc + 2* Droit;
#local tps=(s-floor (s))*Longueur;
#switch (tps)  
#range (0,Droit)
        #declare Pos = y*cos (2*pi*tps/Droit) + x*sin (2*pi*tps/Droit);
        #break
#range (Droit,Droit+Arc)
        #local Vect = vnormalize (-Piste (s)+Droit/2*z);
        #declare Pos = vnormalize (y+Coef/2*Vect*(1-cos (2*pi*(tps-Droit)/Arc)));
        #break
#range (Droit+Arc,2*Droit+Arc)
        #declare Pos = y;
        #declare Pos = y*cos (-2*pi*(tps-Droit-Arc)/Droit) + x*sin (-2*pi*(tps-Droit-Arc)/Droit);
        #break
#range (2*Droit+Arc,Longueur)
        #local Vect = -Piste (s)-Droit/2*z ;
        #declare Pos = vnormalize (y+Coef/2*Vect*(1-cos (2*pi*(tps-2*Droit-Arc)/Arc)));
        #break
#local Pos = 0; 
#end                
Pos
#end                             

#macro Normale_Ovale (s) 
#declare Coef = 1;
#declare Arc = pi*Rayon;
#local Longueur = 2*Arc + 2* Droit;
#local tps=(s-floor (s))*Longueur;
#switch (tps)  
#range (0,Droit)
        #declare Pos = y;
        #break
#range (Droit,Droit+Arc)
        #local Vect = vnormalize (-Piste (s)+Droit/2*z);
        #declare Pos = vnormalize (y+Coef/2*Vect*(1-cos (2*pi*(tps-Droit)/Arc)));
        #break
#range (Droit+Arc,2*Droit+Arc)
        #declare Pos = y;
        #declare Pos = y;
        #break
#range (2*Droit+Arc,Longueur)
        #local Vect = -Piste (s)-Droit/2*z ;
        #declare Pos = vnormalize (y+Coef/2*Vect*(1-cos (2*pi*(tps-2*Droit-Arc)/Arc)));
        #break
#local Pos = 0; 
#end                
Pos
#end

#macro Normale_Ovale_Looping (s) 
#local Arc = pi*Rayon;                          
#local Arc_Loop = pi/2*3*Droit/8;
#local Arc_Haut = pi*Droit/8;
#local Looping = Droit/2+2*Arc_Loop+Arc_Haut;
#local Longueur = 2*Arc +  Droit +Looping;
#declare Coef=2;       
#local N1=vnormalize (<Rayon,3*Droit/8,0>-Piste ((Droit+Arc+Droit/4)/Longueur));
#local N2=vnormalize (<Rayon,3*Droit/8,0>-Piste ((Droit+Arc+Looping-Droit/4)/Longueur));
#local tps=(s-floor (s))*Longueur;
#switch (tps)  
#range (0,Droit)
        #declare Pos = y*cos (2*pi*tps/Droit) + x*sin (2*pi*tps/Droit);
        #break
#range (Droit,Droit+Arc)
        #local Vect = vnormalize (-Piste (s)+Droit/2*z);
        #declare Pos = vnormalize (y+Coef/2*Vect*(1-cos (2*pi*(tps-Droit)/Arc)));
        #break
#range (Droit+Arc,Droit+Arc+Droit/4)
        #local Angle = (tps-Droit-Arc)/Droit*4;
        #declare Pos = y*(1-Angle)+N1*Angle;           
        #break;
#range (Droit+Arc+Droit/4,Droit+Arc+Looping-Droit/4)        
        #declare Pos = vnormalize (<Rayon,3*Droit/8,0>-Piste (s));
        #break                     
#range (Droit+Arc+Looping-Droit/4,Droit+Arc+Looping)
        #declare Angle = (tps - (Droit+Arc+Looping-Droit/4))/Droit*4;
        #declare Pos=y*Angle+N2*(1-Angle);
        #break
#range (Droit+Arc+Looping,Longueur)
        #local Vect = -Piste (s)-Droit/2*z ;
        #declare Pos = vnormalize (y+Coef/2*Vect*(1-cos (2*pi*(tps-Looping-Droit-Arc)/Arc)));
        #break
#local Pos = 0; 
#end                
Pos
#end

/*
Choix du circuit courant
*/
#switch (Choix_Piste)
#case (0)
        #macro Piste (s)                             
        Piste_Ovale(s)
        #end
        #macro Normale (s)
        Normale_Ovale (s)
        #end
        #break
#case (1)
        #macro Piste (s)                             
        Piste_Ovale(s)
        #end
        #macro Normale (s)
        Normale_Ovale_Tourbillon (s)
        #end
        #break
#case (2)
        #macro Piste (s)                             
        Piste_Ovale_Looping(s)
        #end
        #macro Normale (s)
        Normale_Ovale_Looping (s)
        #end
        #break
#case (3)
        #macro Piste (s)                             
        Piste_Moebius(s)
        #end
        #macro Normale (s)
        Normale_Moebius (s)
        #end
        #break
#case (4)
        #macro Piste (s)                             
        Piste_Huit(s)
        #end
        #macro Normale (s)
        Normale_Huit (s)
        #end
        #break
#end  
 /*
 d�claration des macro g�n�rales
 */
#macro Vitesse (s)        
#local Epsilon_Derive = 0.000001 ;
#local Vit = (Piste (s+Epsilon_Derive)-Piste(s-Epsilon_Derive))/(2*Epsilon_Derive);
vnormalize (Vit )
#end
                                 
#macro Trace_Piste (Echelle, Largeur, Precision)                                 

#local Epsilon = 1/Precision;
#local Dist = vlength (Piste (Epsilon) - Piste (0));
union {
#local Temoin = 0;
#while (Temoin<=1)                   
        #local P = Piste (Temoin);
        #local V = Vitesse (Temoin);
        #local N = Normale (Temoin);
        #local L = vcross (V,N);
        
/*        cone {-L*Largeur/2,0,L*Largeur/2,Largeur/10
                translate Echelle*P
                texture {pigment {color Red*Temoin+Blue*(1-Temoin)}}
                } 
        sphere {0,Largeur/100
                translate Echelle*P
                texture {pigment {color Red*Temoin+Blue*(1-Temoin)}}
                }        */
        cone {0,Largeur/30,V/30,0
                translate Echelle*P
                texture {pigment {color Red*Temoin+Blue*(1-Temoin)}}
                } 
        #local Temoin= Temoin+Epsilon;
        #end
        }  
#end        


#macro Trace_Piste_Mesh (Echelle, Largeur, Precision)                                 
#local Epsilon = 1/Precision;
#local Dist = Largeur/5;
#local P0 = Piste (0);
#local V0 = Vitesse (0);
#local N0 = Normale (0);
#local L0 = vcross (V0,N0)*Largeur;
mesh {
        #local Temoin = 0;
        #while (Temoin<=1)                   
                #local P1 = Piste (Temoin+Epsilon);
                #local V1 = Vitesse (Temoin+Epsilon);
                #local N1 = Normale (Temoin+Epsilon);
                #local L1 = vcross (V1,N1)*Largeur;                 
//                #debug concat (str(P0.x,6,6)," ",str(P0.y,6,6)," ",str(P0.z,6,6)," ",chr(10))
  //              #debug concat (str(L0.x,6,6)," ",str(L0.y,6,6)," ",str(L0.z,6,6)," ",chr(10))
    //            #debug concat (str(V0.x,6,6)," ",str(V0.y,6,6)," ",str(V0.z,6,6)," ",chr(10))
      //          #debug concat (str(P1.x,6,6)," ",str(P1.y,6,6)," ",str(P1.z,6,6)," ",chr(10))
        //        #debug concat (str(L1.x,6,6)," ",str(L1.y,6,6)," ",str(L1.z,6,6)," ",chr(10))
                //la piste itself
                triangle {P0-L0/2,P0+L0/2,P1-L1/2 texture {T_Piste1}}                     
                triangle {P1-L1/2,P0+L0/2,P1+L1/2 texture {T_Piste1}}
                //la bordure int�rieure
                triangle {P0-L0/2+N0*Dist,P0-L0/2,P1-L1/2+N1*Dist texture {T_Bord1}}
                triangle {P1-L1/2,P0-L0/2,P1-L1/2+N1*Dist texture {T_Bord1}}        
                //la bordure ext�rieure
                triangle {P0+L0/2+N0*Dist,P0+L0/2,P1+L1/2+N1*Dist texture {T_Bord2}}
                triangle {P1+L1/2,P0+L0/2,P1+L1/2+N1*Dist texture {T_Bord2}}
                //la bordure ext�rieure hors piste
                triangle {P0+L0/2+N0*Dist,P0+L0-N0*Dist,P1+L1/2+N1*Dist texture {T_Bord2}}
                triangle {P1+L1-N1*Dist,P0+L0-N0*Dist,P1+L1/2+N1*Dist texture {T_Bord2}}
                //la bordure int�rieure hors piste                
                triangle {P0-L0/2+N0*Dist,P0-L0-N0*Dist,P1-L1/2+N1*Dist texture {T_Bord1}}
                triangle {P1-L1-N1*Dist,P0-L0-N0*Dist,P1-L1/2+N1*Dist texture {T_Bord1}}
                #declare P0 = P1;
                #declare N0 = N1;
                #declare V0 = V1;
                #declare L0 = L1;
                #local Temoin= Temoin+Epsilon;
                #end
                }
#end        
   

// Les deux vecteurs du sol                                
//  cylinder {0,x,.02 texture {pigment {color Blue}}}        
// cylinder {0,z,.02 texture {pigment {color Green}}}        

// D�claration du d�cor


// D�claration de la piste en personne      
#declare Piste_T = Trace_Piste_Mesh (1,.1,Precision)


//impl�mentation de la sc�ne
object {Piste_T 
        // texture {pigment {color Blue}}
        //rotate y*90
        }                


#declare P = Piste (Temps+.005);
#declare V = Vitesse (Temps+.005);
#declare N = Normale (Temps+.005); 
#undef L
#declare L = vcross (V,N);
object {Vaisseau  
        rotate y*90
        scale .05*1/3
        matrix Transf (L,N,V)
        translate P+.01*N+L*.02
        }

#declare P = Piste (Temps+.005+0.2*Perturb(Temps));
#declare V = Vitesse (Temps+.005+0.2*Perturb(Temps));
#declare N = Normale (Temps+.005+0.2*Perturb(Temps)); 
#undef L
#declare L = vcross (V,N);
object {Vaisseau  
        rotate y*90
        scale .05*1/3
        matrix Transf (L,N,V)
        translate P+.01*N-L*.02
        }

light_source {1000*(y-z-x) color White}
light_source {1000*(y-z+x) color White}
light_source {1000*(-y+z+x) color White}

#declare P = Piste (Temps);
#declare V = Vitesse (Temps);
#declare N = Normale (Temps);
#declare L = vcross (V,N);
light_source {P + .010*N -L*.01 color 3*White
        spotlight
        point_at P + .01*N + V*.07 -L*.01
        radius 8
        falloff 10
        }
light_source {P + .010*N +L*.01 color 3*White
        spotlight
        point_at P+ .01*N + V *.07 +L*.01
        radius 8
        falloff 10
        }
 
#if (Af_Ciel = 1)
sky_sphere {       
        pigment {onion
                turbulence 7
                color_map {
                        [0 color Clear]
                        [.4 color Clear]
                        [.45 color Green]
                        [.5 color Clear]
                        [.6 color Red]
                        [.65 color Clear]
                        [.7 color NeonPink]
                        [.75 color Clear]
                        [.8 color Cyan]
                        [.9 color Clear]
                        [1 color Clear]
                        }
                }                        
        pigment {agate
                turbulence 5
                color_map {
                        [0 color Clear]
                        [.5 color Clear]
                        [.6 color Red]
                        [.65 color Clear]
                        [.7 color Yellow]
                        [.75 color Clear]
                        [.8 color Cyan]
                        [.9 color Clear]
                        [1 color Clear]
                        }
                }                        
        pigment {bozo 
        color_map {
                [0 color Clear]
                [.5 color Clear]
                [.6 color Red transmit .3]
                [.7 color .5*Red transmit .4]
                [.75 color Yellow transmit .8]
                [.8 color Clear]
                [1 color Clear]
                }
                scale .1
                }                         
                
        }                
#end                

//  plane {y,0 texture {pigment {checker color White color Black scale 1/4}}}

    /*    
camera {location Piste (Temps) + .02*Normale (Temps)
        look_at Piste (Temps) + .02*Normale (Temps) + Vitesse (Temps)/2
        }                 

    
camera {location <-4,2,3>
        direction -y
        up z
        right 4/3*x
        look_at 0
        }
      */

/*
camera {location Piste (Temps) + .02*Normale (Temps)
        direction vnormalize (vcross(Normale (Temps),vcross(Vitesse (Temps),Normale (Temps))))
        // direction Vitesse (Temps)
        up Normale (Temps)
        right -4/3 * vcross (Vitesse (Temps),Normale (Temps))
        }         
*/


camera {location Piste (Temps) + .02*Normale (Temps)
        direction Vitesse (Temps)
        up Normale (Temps)
        right -16/9 * vcross (Vitesse (Temps),Normale (Temps))
        }         

        