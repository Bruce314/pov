#version 3.7;
global_settings { 
  assumed_gamma 1.0
}

// reference file for camshaft
#include "spring.inc"

#include "gears.inc"

// used for interpolate
#include "math.inc"     


// some default includes
#include "golds.inc"
#include "metals.inc"

// debug
#declare DebugTexture=1;
#declare d_axis=1;

// used to help debugging
#include "b_debug.inc"      

// 


object {
  Spring (2,3,0.1,4+2*sin(360*clock))
  texture {
    pigment {
      color rgb <1,1,1>
    }
  }
  rotate -z*90
  translate -x*4
}

/*
object {SpringSpire (3,0.3,3)
  translate -y*1.5
  texture {
    pigment {
      color rgb <1,1,1>
    }
  }
  rotate x*180*clock
}
*/
// camera and basic light
camera {
  location <0,0,-10>
  look_at 0
  right x*image_width/image_height
  up y
}                   

light_source {
    100000*(-z) color rgb <1,1,1>
}

light_source {
    100000*(y) color rgb <0,0,1>
}

light_source {
    100000*(x) color rgb <1,0,0
    >
}
