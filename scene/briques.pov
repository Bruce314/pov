#include "colors.inc"
#include "textures.inc"
#include "lego.inc"


/*
*********************************************************
*							*
*		DEBUT DE LA SCENE			*
*							*
*********************************************************
*/
#declare Brique = Lego (2,2,1)

object {Brique
        texture {
		pigment {color Yellow}
		finish {phong 1.0 phong_size 20}
		}
	translate (x+z)*4
//	rotate -15
	scale 1/5
	}
object {Brique
        texture {
		pigment {color Yellow transmit .8}
		finish {phong 1.0 phong_size 20 diffuse .7 refraction on ior 1.33}
		}
	translate (x-z)*4
//	rotate -15
	scale 1/5
	}
object {Brique
        texture {
		pigment {color Yellow}
		finish {phong 1.0 phong_size 20}
		}
	translate (-x+z)*4
//	rotate -15
	scale 1/5
	}
object {Brique
        texture {
		PinkAlabaster
		finish {phong 1.0 phong_size 20}
		}
	translate -(x+z)*4
//	rotate -15
	scale 1/5
	}
#declare Brique = Lego (2,2,1)
object {Brique
        texture {
		pigment {color Blue transmit .8}
		finish {phong 1.0 phong_size 20 diffuse .7 refraction on ior 1.33}
		}
	translate y*5
//	rotate -15
	scale 1/5
	}
#declare Brique = Lego (2,2,1)
object {Brique
        texture {
		pigment {color Red}
		finish {phong 1.0 phong_size 20}
		}
	translate 2*y*5
//	rotate -15
	scale 1/5
	}

/*
sphere {0,1
        texture {
		pigment {color Red}
		finish {phong 1.0 phong_size 200}
		}
        }
*/

light_source {1000*(y/2-z+x/2) color Red + .2*White}
light_source {1000*(y/2-z- x/2) color Blue +.2*White}
light_source {1000*(y/2-z) color Green +.2*White}


//plane {y,0 texture {pigment {checker color Black color White}} hollow no_shadow}
//plane {z,10 texture {pigment {checker color Black color White}} hollow no_shadow}
//plane {x,10 texture {pigment {checker color Black color White}} hollow no_shadow}

sky_sphere {
	pigment {
		gradient y 
		color_map {
			[0 color MidnightBlue]
			[1 color rgb <.4,0,0>]
			}
		}
	}



camera {
        location <0,3,-8>
	direction 1.5*z
        look_at 1.5*y
	rotate y*20
        }
