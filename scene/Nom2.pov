#include "colors.inc"
#include "textures.inc"
#include "lego.inc"
#include  "E_Nom.inc"

global_settings {
	#max_trace_level  10
}

#declare Qualite = 1 ; 

#declare Nom = Ecris_Nom ("Florent",Qualite)
object {Nom
	texture {pigment {color Blue}}
	scale 1/5
//	translate -6*x
	}
#declare Nom = Ecris_Nom ("Revelut",Qualite)
object {Nom
	texture {pigment {color Green}}
	scale 1/5
	translate  6*y
	}



plane {y,-HB/3/5
	texture {
		pigment {checker color Black color White}
		scale 4/5
		}
	hollow
	}

plane {z,4
//	texture {pigment {gradient 10*y color_map {[0 color Blue][1 color MidnightBlue]}}}
	texture {pigment {checker color Yellow color Red} scale <4/5,1,1>}
	hollow
	}

//light_source {1000*(y-z+x/2) color White}
light_source {1000*(y-z-x/2) color 1.2*White}


camera {
	location <-1.5,4,-20>
	direction .8*z
	look_at 4*y
	}
	