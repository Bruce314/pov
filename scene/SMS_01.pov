#include "colors.inc"
#include "textures.inc"
#include "robot03.inc"

#declare T_Slip = texture {pigment {color MidnightBlue}}

#declare T_Sofa = texture {
        pigment {leopard color_map {
                        [0 color White]
                        [.2 color Black]
                        [.6 color White]
                        [1 color White]
                        }
                turbulence 1                        
                scale .05                        
                }           
        normal {agate .1 scale .01}                        
        }                

#declare T_Mur= texture {pigment {color MidnightBlue}}

/* Le robot Test Plafond */                      
#declare Robot01 = Droide2 (0,0,0,0,
        180,45,5,0,
        180,90,60,80,
        -5,15,0,0,90,00,
        -30)

/* Le robot assis dans le canap� du salon */                      
#declare Robot01 = Droide2 (90,20,90,20,
        30,45,5,0,
        90,90,60,80,
        -5,15,0,0,90,00,
        -30)



/* Le canap' du salon */                      
#declare Sofa=blob {
        threshold .7
        #declare Temoin= -1.3;
        #while (Temoin<=1.3)
                cylinder {0,2*y,.7,2
                        translate <1.5,0,Temoin>
                        }
                cylinder {0,2*y,.7,2
                        translate <-1.5,0,Temoin>
                        }                        
                cylinder {-1.5*x,1.5*x,.7,1
                        translate <0,1.7,Temoin>
                        }
                #declare Temoin=Temoin+.4;
                #end
        #declare Temoin= 0;
        #while (Temoin<=5.5)
                cylinder {-1.5*x,1.5*x,.7,1
                        translate <0,Temoin,1.3+(sqrt(Temoin)-sqrt (2))/3>
                        }
                #declare Temoin=Temoin+.4;
                #end
        #declare Temoin= 0;
        #while (Temoin<1.7)
                cylinder {-1.5*x,1.5*x,.7,1
                        translate <0,Temoin,-1.2+(Temoin-1.7)/3>
                        }
                #declare Temoin=Temoin+.4;
                #end
                
        cylinder {<1.5,2,-1.3>,<1.5,2,1.3>,1.0,1.5}
        cylinder {<-1.5,2,-1.3>,<-1.5,2,1.3>,1.0,1.5}
        }


/* La piece Salon */
#declare L_Piece = 10;
#declare Salon=union {
        box {<-L_Piece,-1,-L_Piece>,<L_Piece,15,L_Piece> hollow no_shadow texture {T_Mur finish {ambient .5}}}
        union {
                object {Robot01                   //robot assis sur le sofa
                        scale .75
                        translate <0,.15,0.3>
                        }
                object {Sofa                    //Sofa su salon
                        texture {T_Sofa}
                        }
                translate <.5,0,L_Piece-4>
                }                        
        object {Sofa                    //Sofa su salon
                rotate y*30
                translate <5.5,0,L_Piece-4.25>
                texture {T_Sofa}
                }
        object {Sofa                    //Sofa su salon
                rotate -y*22
                translate <-5,0,L_Piece - 4.5>
                texture {T_Sofa}
                }
        }


/* La scene itself */
//Le sol
plane {y,0 texture {pigment {checker color Red color Yellow}}}
//le plafond
plane {y,13 texture {
        pigment {checker color Red color Yellow} 
        finish {ambient 1.0}}                    
        no_shadow
        hollow
        }
//la piece courante
// object {Salon}                             

light_source {<000,1000,-1000> color White}

camera {
        location <0,5,-(-L_Piece + 1)>
        direction .7*z
        look_at 5*y     
        }