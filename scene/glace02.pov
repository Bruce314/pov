#include "colors.inc"
#include "textures.inc"

#declare L1=.2;
#declare Z1=.05;
#declare H1=3.5;

#declare Glace02=superellipsoid {<.5,1>}

#declare Baton=union {
        box {<-L1,0,-Z1>,<L1,-H1,Z1>}
        cylinder {-Z1*z,Z1*z,L1
                translate -H1*y
                }
        texture {pigment {color Red}}                
        }                

union {
        object {Glace02     
                scale <1,2,.5>
                texture {pigment {color Yellow}}
                }       
        object {Baton}                
       // plane {y,0 texture {pigment {checker color White color Black}}}
        // plane {z,0 texture {pigment {checker color Blue color Yellow}}}
        rotate y*30
        }
                
        

camera {
        location <0,1,-7>
        look_at 0
        }
        
light_source {10000*(y-z) color White}        
light_source {10000*(-y-z+x/2) color .5*White}        
        