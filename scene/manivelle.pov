#include "colors.inc"
#include "textures.inc"
#include "metals.inc"
// gears : circular_pitch
#include "gears.inc"

#declare radius1=1;
#declare axis=0.1;
#declare l1=3*radius1;

#declare wheelAngle1=360*clock;

#declare angleLever=-asin(radius1/l1*sin(wheelAngle1*pi/180))*180/pi;
#declare positionLever=radius1*cos(wheelAngle1*pi/180)+sqrt(l1*l1-radius1*radius1*sin(wheelAngle1*pi/180)*sin(wheelAngle1*pi/180));

union {
  difference {
    union {
      cylinder {0.1*z,0.2*z,radius1}
      cylinder {-0.1*z,0.1*z,axis
        translate <radius1,0,0>
      }
    }
    cylinder {-0.2*z,0.2*z,axis}
    texture {pigment {color rgb <1,0,0>}}
    rotate z*wheelAngle1
  }

  difference {
    union {
      cylinder {-0.1*z,0.1*z,axis*2}
      cylinder {-0.1*z,0.1*z,axis*2
        translate <l1,0,0>
      }
      cylinder {0,<l1,0,0>,0.1}
      
    }
    union {
      // axis holes
      cylinder {-0.11*z,0.11*z,axis}
      cylinder {-0.11*z,0.11*z,axis
        translate <l1,0,0>
      }
    }
    texture {
      pigment {
        color rgb <0,1,1>
      }
    }
    rotate <0,0,angleLever>
    translate <radius1*cos(wheelAngle1*pi/180),radius1*sin(wheelAngle1*pi/180),0>
  }

  // cylinder
  union {
    cylinder {-0.5*z,0.1*z,axis}
    box {<-0.1,-0.1,0.1>,<0.1,0.5,0.3>}
    
    texture {
      pigment{
        color rgb <1,0,1>
      }
    }
    translate <positionLever,0,0>
  }
  translate <-2*radius1,0,0>
  //rotate x*45
}


// debug
cylinder {0,20*x,0.05
  texture {
    pigment {
      color rgb <1,1,1>
    }
  }
}
    



// d=r.cos(theta)+sqrt(d²-r²sin(theta))



camera {
  location <0,0,-6>
  look_at 0
  right x*image_width/image_height
  up y
}                   

light_source {10000*(y-z) color rgb <1,1,1>}
light_source {10000*(y-z-x) color rgb <1,0,0>}
light_source {10000*(y-z+x) color rgb <0,1,0>}