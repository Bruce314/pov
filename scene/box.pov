#version 3.7;
global_settings { 
  assumed_gamma 1.0
  charset utf8 
}

#include "colors.inc"
#include "textures.inc"
#include "woods.inc"
#include "golds.inc"
#include "metals.inc"

//#declare d_axis=1;

// this specific files is used as base for an explanation on my website
// details of the article on http://www.revelut.ch/2016/02/16/box-with-non-vertical-sides/

// used to help debugging
#include "b_debug.inc"      

#switch (clock)
  #range (0,0.125)
    #declare PhaseBlue=-4*8*clock;
    #declare PhaseRed=0;
  #break
  #range (0.125,0.25)
    #declare PhaseBlue=0;
    #declare PhaseRed=-4*8*clock;
  #break
  #else
    #declare PhaseBlue=0;
    #declare PhaseRed=0;
#end

#switch (clock)
  #range(0,0.25)
    #declare AngleSide=45*clock*4;
    #declare AnimRotation=0;
    #declare CloseRatio=0; //(-cos(2*pi*clock)*0.5+0.5)
  #break
  #range(0.25,0.5)
    #declare AngleSide=45;
    #declare AnimRotation=0;
    #declare CloseRatio=(-cos(pi*4*(clock-0.25))*0.5+0.5);
  #break
  #range (0.5,0.75)
    #declare lClock=4*(clock-0.5);
    #declare AngleSide=45-sin(pi*lClock)*30;
    #declare CloseRatio=1;
    #declare AnimRotation=0;
  #break
  
  #range (0.75,1)
    #declare lClock=4*(clock-0.75);
    #declare AngleSide=45*(1-lClock);
    #declare AnimRotation=00;
    #declare CloseRatio=1-(-cos(pi*lClock)*0.5+0.5);
  #break
  
/*  #range(0.5,1)
    #declare lClock=2*(clock-0.5);
    #declare AngleSide=45*(1-lClock);
    #declare AnimRotation=360*lClock;
    #declare CloseRatio=1-(-cos(pi*lClock)*0.5+0.5);
  #break
  */
  #else
#end

  
//#declare AngleSide=45; // as beta
#declare AngleCut=atan(sin(AngleSide*pi/180))*180/pi;
#declare SideLength=2;

#declare BaseWidth=3;
#declare BaseLength=5;     
#declare Thickness=0.05;

#declare epsilon=0.0001;

#declare Rot=CloseRatio*(90-AngleSide);

#declare RealHeight=SideLength*cos(AngleSide*pi/180);

#macro display_beta()
  difference
  {
    intersection {
      difference {
        merge {
          box {
            <-BaseLength/2-SideLength,0,-BaseWidth/2>,
            <BaseLength/2+SideLength,RealHeight,+BaseWidth/2>
          }
          box {
            <-BaseLength/2,0,-BaseWidth/2-SideLength>,
            <BaseLength/2,RealHeight,+BaseWidth/2+SideLength>
          }
        }
        union {
          box {
            <-BaseLength/2-SideLength-epsilon,-epsilon,-BaseWidth/2+Thickness>,
            <BaseLength/2+SideLength+epsilon,SideLength+epsilon,+BaseWidth/2-Thickness>
          }
          box {
            <-BaseLength/2+Thickness,-epsilon,-BaseWidth/2-SideLength-epsilon>,
            <BaseLength/2-Thickness,SideLength+epsilon,+BaseWidth/2+SideLength+epsilon>
          }
        }
      }

      plane {x,0
        rotate -z*AngleSide
        translate x*BaseLength/2
      }
      plane {-x,0
        rotate z*AngleSide
        translate -x*BaseLength/2
      }
      plane {z,0
        rotate x*AngleSide
        translate z*BaseWidth/2
      }
      plane {-z,0
        rotate -x*AngleSide
        translate -z*BaseWidth/2
      }
    }
    cylinder {0,SideLength*y,1}
    texture {
      pigment {
        gradient y
        phase PhaseRed
        color_map {
          [0 color White filter 0.9]
          [1 color Red filter 0.95]
        }
        scale <1,RealHeight,1>
      }
      finish {
        phong 1.0
        phong_size 100
      }
    }
    interior {
      ior 1.3
    }

  }
#end  
  

#macro side_zy()
  union {
    intersection {
      box {
        <0,-Thickness,-BaseLength>,
        <SideLength,0,BaseLength>
      }
      plane {z,0
        rotate -y*AngleCut
        translate z*(BaseWidth/2)
      }

      
      plane {-z,0
        rotate  y*AngleCut
        translate -z*(BaseWidth/2)
      }
      texture {
        pigment {
          gradient x
          phase PhaseBlue
          color_map {
            [0 color White]
            [1 color Blue]
          }
          scale <SideLength,1,1>
        }
      }
      
    }
    box {
      <0,-Thickness-epsilon,-BaseWidth/2>,
      <SideLength,epsilon,BaseWidth/2>
      
    }

    texture {
      T_Wood11    
      normal { wood 0.5 scale 0.05 turbulence 0.0 rotate<0,90,0> }
      finish { phong 1 } 
      rotate<0,0,0> 
      scale 4
      translate<0,0,0>
    }
    rotate z*Rot
  }
#end

#macro side_xy()
  union {
    intersection {
      box {
        <-10,-Thickness,0>,
        <10,0,SideLength>
      }
      plane {x,0
        rotate y*AngleCut
        translate x*(BaseLength/2)
      }
      plane {-x,0
        rotate  -y*AngleCut
        translate -x*(BaseLength/2)
      }
      texture {
        pigment {
          gradient z
          phase PhaseBlue
          color_map {
            [0 color White]
            [1 color Blue]
            
          }
          scale <1,1,SideLength>
        }
      }
      
    }
    box {
      <-BaseLength/2,-Thickness-epsilon,0>,
      <BaseLength/2,epsilon,SideLength>
      
    }

    texture {
      T_Wood11    
      normal { wood 0.5 scale 0.05 turbulence 0.0 rotate<0,90,0> }
      finish { phong 1 } 
      rotate<0,90,0> 
      scale 4
      translate<0,0,0>
    }
    rotate -x*Rot
  }
#end


union { 
  box {
    <-BaseLength/2,-Thickness,-BaseWidth/2>,
    <BaseLength/2,00,BaseWidth/2>
    texture {T_Brass_2A}
    /*
    texture {            
      
      T_Wood6     
      finish { phong 1}
      scale 2    
      rotate x*90                      
      rotate <10,90,-15>
    }
  */
    
  }

  object {side_zy()
    translate x*(BaseLength/2)
  }

  object {side_zy()
    rotate y*180
    translate -x*(BaseLength/2)
  }

  
  object {side_xy()
    translate z*(BaseWidth/2)
  }

  object {side_xy()
    rotate y*180
    translate -z*(BaseWidth/2)
  }

  object {
    display_beta()
  }

  text {
    ttf "SourceSansPro-Regular.ttf" concat("β=",str(AngleSide,0,1)) 
    0.2,0
    
    texture {
      pigment {
        gradient y
        phase PhaseRed
        color_map {
          [0 color White]
          [1 color Red]
        }
        scale 0.5
      }
    }
    rotate x*90
    translate <-1,Thickness,0.5>
  }
  text {
    ttf "SourceSansPro-Regular.ttf" concat( "α=",str(AngleCut,0,1) ) 
    0.2,0
    
    texture {
      pigment {
        gradient y
        phase PhaseBlue
        color_map {
          [0 color White]
          [1 color Blue]
        }
        scale 0.5
      }
    }
    rotate x*90
    translate <-1,Thickness,-0.5>
  }

  rotate y*(12+AnimRotation)
  rotate -x*45
}

camera {
  location <0,0,-8>
  look_at 0
  right x*image_width/image_height
  up y
}                   

light_source {
  100000*(y-z) color rgb <1,1,1>
}

light_source {
  100000*(y) color rgb <0,0,1>
}

light_source {
  100000*(y+x) color rgb <1,0,0>
}
