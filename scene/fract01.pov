#include "colors.inc"
#include "textures.inc"

#macro Cube (p1,p2,niv,lim)
        #if (niv = lim)     
                box {p1,p2}
        #else              
                #local X=vdot(p2-p1,x);
                #local Y=vdot(p2-p1,y);
                #local Z=vdot(p2-p1,z);
               // #debug concat("X : ",str (X,4,4)," Y : ",str(Y,4,4)," Z : ",str(Z,4,4),"\n")
                
                Cube (p1,p1+(p2-p1)/4,niv+1,lim)
                Cube (p1+(p2-p1)/4,p1+3*(p2-p1)/4,niv+1,lim)
                Cube (p1+3*(p2-p1)/4,p2,niv+1,lim)

                Cube (p1+<0,Y*3/4,0>,p1+<X/4,Y,Z/4>,niv+1,lim)
                Cube (p1+<X*3/4,0,0>,p1+<X,Y/4,Z/4>,niv+1,lim)
                Cube (p1+<0,0,Z*3/4>,p1+<X/4,Y/4,Z>,niv+1,lim)   
                
                Cube (p1+<0,Y*3/4,Z*3/4>,p1+<X/4,Y,Z>,niv+1,lim)
                Cube (p1+<3/4*X,0,Z*3/4>,p1+<X,Y/4,Z>,niv+1,lim)
                Cube (p1+<3/4*X,Y*3/4,0>,p1+<X,Y,Z/4>,niv+1,lim)
                
        #end                                                
#end

#declare Fract = union {Cube (-1,1,0,5)}


object {Fract 
        //texture {pigment {color Yellow}} 
        texture {Gold_Metal}
        rotate 30
        }

sky_sphere {        /*
        pigment {
                gradient y
                color_map {
                        [0 color Cyan]
                       // [0 color MidnightBlue]
                     //   [.5 color Cyan]
                        [1 color MidnightBlue]
                }                             
        }             */
        pigment {color White}
}        

camera  {
        location <0,0,-4>
        look_at 0
}

light_source {1000*(x+y-z) color White}        
light_source {1000*(-x+y-z) color White}        

      
        
       