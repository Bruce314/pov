/******************************/
/*                            */
/*   IRTC APRIL 2002 : WINTER */
/*                            */
/******************************/

/******************************/
/*                            */
/*   Directives de trac�      */
/*                            */
/******************************/


/* D�finition de la qualit� du trac� */
 //#declare BQ_ARBRE = 1; //arbre basse qualit�
 #declare BELLE_TABLE = 1;
 #declare BEAU_MUR = 1;
 #declare BELLES_TEXTURES_OBJET = 1;
 #declare ARBRES=1;     
 #declare GLOBE=1; //1 en transparent, 2 en opaque
 #declare NEIGE_TOIT=1; //Neige sur le toit de la maison
 #declare SOL=1; //neige dans le sol
 #declare MAISON=1; // existence de la "cholie pitite maizon"
// #declare NEIGE_TOMBE = 1; //
#declare NEIGE_TOMBE2 = 1; //
#declare CAMERA=2; //2 finale
                        // 1 de face
                        // 3 de haut

 #declare RADIOSITY = 1; // Utilisation ou non de la radiosit�                        
                       

global_settings {
#ifdef (RADIOSITY)
    radiosity {
      pretrace_start 0.08
      pretrace_end   0.02
      count 50

      nearest_count 5
      error_bound 1.8
      recursion_limit 3

      low_error_factor 0.5
      gray_threshold 0.0
      minimum_reuse 0.015
      brightness 1

      adc_bailout 0.01/2
    }
#end 
// used in global_settings, sets the maximum ray tracing bounce depth (1 or more) [5]
  max_trace_level 15

}

/******************************/
/*                            */
/*   Inclusions               */
/*                            */
/******************************/


/* include standards */
#include "colors.inc"
#include "textures.inc"
#include "woods.inc"


/******************************/
/*                            */
/*   Textures                 */
/*                            */
/******************************/

#declare T_Support = texture {
        #ifdef (BELLES_TEXTURES_OBJET) 
                pigment {Red_Marble scale 1/3}
                finish {phong .5 phong_size 20}
                normal {bumps .1 scale .05}
        #else
                pigment {color Red}
        #end
}                         

#ifdef (MAISON)
#declare T_Maison = texture {
        pigment {color DarkBrown}
        finish {diffuse .6 reflection 0}
//        normal {
}        
#end

#declare T_Sapin = texture {
        pigment {color rgb <.2,.7,.1>}
        finish {phong 1.0 phong_size 30}
        normal {bumps 1 scale .01}
}                       

#declare T_Arbre1 = texture {
        pigment {color Brown}
        finish {phong 1.0 phong_size 30}
        normal {bumps 1 scale .01}
}                       

#ifdef (GLOBE)
#declare T_Globe = texture {
        pigment {color White filter .9}
        finish {phong 1.0 phong_size 75 reflection .15}
}             
#end

#declare T_Neige= texture {
        pigment {color White}
        finish {phong 1.0 phong_size 10}
       // normal {bumps 1 scale <.03,.01,.03>}
}        

#ifdef (BELLE_TABLE)
#declare T_Table=texture {
//        DMFWood4   
        //DMFWood6
        T_Wood10
        finish {
                phong 1.0 
                phong_size 20
                reflection .03
        }
        normal {ripples 1.0 scale <3,.2,3>}             
        rotate y*90
}                      
#end

/******************************/
/*                            */
/*   Constantes               */
/*                            */
/******************************/

#declare Epsilon = 1E-6;
#declare Ep_Globe = .05;   
#declare Rayon_TrucZ = 2/1.5;
#declare Rayon_TrucX = 3/1.5;
#declare Haut_Sup_Truc = .6/1.5;
#declare Haut_Dome = 4/1.5;            
#declare Haut_Neige=.1;
#declare R_Rondin=.1;
#declare Long_Maison=4;
#declare Larg_Maison=3;
#declare Haut_Maison=2;
#declare Haut_Toit=2;
#declare Larg_Fen = 1.3;

#declare R1=seed (0);
#declare R2=seed (1);
               
/******************************/
/*                            */
/*   Objets                   */
/*                            */
/******************************/

#ifdef (NEIGE_TOMBE)
#debug "On utilise la neige\n"

#declare Flocon=sphere {0,1 
        hollow
        texture {
                pigment {color Clear}
        }        
        interior {
                ior 1
                media {    // atmospheric media sample
             /*           intervals 10
                        scattering {.75, rgb .03}
                        samples 1, 10
                        confidence 0.9999
                        variance 1/1000
                        ratio 0.9
                        density {
                                spherical
                                color_map {                     
                                        [0 color Black filter 1]
                                        [.5 color White filter -1]
                                        [1 color White filter 1]
                                }
                        }                */

    emission 0.75
    scattering {1, 0.5}
    density { spherical
      color_map {
        [0.0 rgb <0,0,0>]
        [0.5 rgb <0.4, 0.4, 0.8>]
        [1.0 rgb <1,1,1>]
      }
    }
                          
                }

        }
}                

#declare Neige_Tombe= merge {
        #declare X_al=seed (3);
        #declare Y_al=seed (3);
        #declare Z_al=seed (3);
        #declare Temoin=0;
        #declare Limite=500;
        
        #while (Temoin <= Limite)
                #declare loc_X=2*rand(X_al)-1;
                #declare loc_Y=rand(X_al);
                #declare loc_Z=2*rand(X_al)-1;
                
                #if (loc_X*loc_X+loc_Y*loc_Y+loc_Z*loc_Z < 1)
                        #declare loc_Pos = <loc_X*Rayon_TrucX-Ep_Globe*4,loc_Y*Haut_Dome*(1-4*Ep_Globe/Rayon_TrucX),loc_Z*Rayon_TrucZ*(1-4*Ep_Globe/Rayon_TrucX)>;
                        object {Flocon 
                                scale 1/8
                                translate loc_Pos
                        } 
                        #end                
                #declare Temoin= Temoin+1;
                #end
        translate Haut_Sup_Truc*y               
}        
#end

#ifdef (MAISON)
#macro Rondin (longueur,rayon,pic)
union {
        cylinder {-longueur*x/2,longueur*x/2,rayon}
        cone {-longueur*x/2,rayon,-(longueur/2+pic)*x,0}
        cone {longueur*x/2,rayon,(longueur/2+pic)*x,0}
}
#end // de la macro Rondin

#declare Maison = difference {
        union {
                #declare loc_hauteur=-5*R_Rondin;
                #declare aaa=Rondin (Long_Maison,R_Rondin,Larg_Maison/8)
                #declare bbb=Rondin (Larg_Maison,R_Rondin,Larg_Maison/8)
                #while (loc_hauteur <= Haut_Maison)
                        object {aaa 
                                texture {T_Maison}
                                translate <-0,loc_hauteur,-Larg_Maison/2>
                        }
                        object {aaa               
                                texture {T_Maison}
                                translate <-0,loc_hauteur,Larg_Maison/2>
                        }                       
                        object {bbb               
                                texture {T_Maison}
                                rotate y*90
                                translate <-Long_Maison/2,loc_hauteur-R_Rondin,0>
                        }
                        object {bbb               
                                texture {T_Maison}
                                rotate y*90
                                translate <Long_Maison/2,loc_hauteur-R_Rondin,0>
                        }           
                        #declare loc_hauteur = loc_hauteur +R_Rondin*2;
                        #end
                #while (loc_hauteur <= (Haut_Maison+Haut_Toit))                                                               
                        #declare bbb=Rondin (Larg_Maison*(1-(loc_hauteur-Haut_Maison)/Haut_Toit+Epsilon),R_Rondin,Larg_Maison/8)
                        object {aaa                                     
                                texture {T_Maison}
                                translate <0,loc_hauteur,-Larg_Maison*(1-(loc_hauteur-Haut_Maison)/Haut_Toit+Epsilon)/2>
                        }
                        object {aaa               
                                texture {T_Maison}
                                translate <0,loc_hauteur,+Larg_Maison*(1-(loc_hauteur-Haut_Maison)/Haut_Toit+Epsilon)/2>
                        }                       
                        object {bbb               
                                texture {T_Maison}
                                rotate y*90
                                translate <-Long_Maison/2,loc_hauteur-R_Rondin,0>
                        } 
                        object {bbb               
                                texture {T_Maison}
                                rotate y*90
                                translate <Long_Maison/2,loc_hauteur-R_Rondin,0>
                        }           
                        #declare loc_hauteur = loc_hauteur +R_Rondin;
                        #end
        }
        union {                   
                box {<-Larg_Fen/2,Haut_Maison/3,0>,<+Larg_Fen/2,Haut_Maison*2/3,-Larg_Maison>}
                box {<-Long_Maison,-1,-Larg_Fen*1/2>,<0,Haut_Maison*3/4,Larg_Fen*1/2>}
                texture {T_Maison}                   
        }
}        

#end            //du ifdf MAISON

#ifdef (NEIGE_TOIT)
#declare Neige_Toit = blob {                     
        #declare pas=.15;     
        #declare loc_hauteur = Haut_Maison;
        #while (loc_hauteur <= (Haut_Maison+Haut_Toit))                                                               
                #declare x_pos=-Long_Maison/2;
                #while (x_pos <= Long_Maison/2)  
                        sphere {<x_pos,loc_hauteur+Haut_Maison/10*rand(R2),-Larg_Maison*(1-(loc_hauteur-Haut_Maison)/Haut_Toit+Epsilon)/2>,.4,1 }
                        sphere {<x_pos,loc_hauteur+Haut_Maison/10*rand(R2),Larg_Maison*(1-(loc_hauteur-Haut_Maison)/Haut_Toit+Epsilon)/2>,.4,1 }
                        #declare x_pos=x_pos+pas;
                        #end
                #declare loc_hauteur = loc_hauteur +R_Rondin;
                #end            //du while loc_hauteur                                                                     
                texture {T_Neige}
        }
#end          // du ifdef NEIGE_TOIT


/*include arbres */
#ifdef (BQ_ARBRE)
  #include "low_qual.inc"
  #declare Arbre1 = object {Arbre texture {T_Arbre1}}
  #declare Sapin = object {Arbre texture {T_Sapin}}
#else
  #include "arbre.inc"
  #declare Arbre1 = object {Arbre texture {T_Arbre1}}
  #include "sapin.inc"
  #declare Sapin = object {Arbre texture {T_Sapin}}
#end

#declare Support = cylinder {0,Haut_Sup_Truc*y,1
        scale <Rayon_TrucX,1,Rayon_TrucZ>
        texture {T_Support}
}

#ifdef (GLOBE)

#declare Globe_1 = intersection {
        union {
                sphere {0,1
                        interior {
                                ior 1.5
                                caustics .4
                        }
                }
                sphere {0,1-Ep_Globe/Rayon_TrucX
                        interior {
                                ior 1.33
                                caustics .4
                                #ifdef (NEIGE_TOMBE2)
                                media {
                                        emission 0.75
                                        scattering {1, 0.5}
                                        #declare X_al=seed (3);
                                        #declare Temoin=0;
                                        #declare Limite=100;
        
                                        #while (Temoin <= Limite)
                                                #declare loc_X=2*rand(X_al)-1;
                                                #declare loc_Y=rand(X_al);
                                                #declare loc_Z=2*rand(X_al)-1;
                
                                                #if (loc_X*loc_X+loc_Y*loc_Y+loc_Z*loc_Z < 1)
                                                        #declare loc_Pos = <loc_X*Rayon_TrucX-Ep_Globe*4,loc_Y*Haut_Dome*(1-4*Ep_Globe/Rayon_TrucX),loc_Z*Rayon_TrucZ*(1-4*Ep_Globe/Rayon_TrucX)>;
                                                        density { 
                                                                spherical
                                                                color_map {
                                                                        [0.0 rgb <0,0,0>]
                                                                        [0.5 rgb <0.4, 0.4, 0.8>]
                                                                        [1.0 rgb <1,1,1>]
                                                                }
                                                                scale 1/10
                                                                translate loc_Pos
                                                        }
                                                        #end                
                                                #declare Temoin= Temoin+1;
                                                #end
                                }                    
                                #end
                        }
                }
                scale <Rayon_TrucX,Haut_Dome,Rayon_TrucZ>
        }
        plane {-y,0
                translate -y*Epsilon
        }
        #if (GLOBE = 1)
                texture {T_Globe}
        #else
                texture {pigment {color Yellow}}
        #end      
        hollow
        translate Haut_Sup_Truc*y
}        
#end              
              
               
#ifdef (ARBRES)               
#declare Arbres= union {
        object {Arbre1
        	scale 1
        	rotate y*20
        	translate <1/2,Haut_Sup_Truc,0>
        	// rotate y*90
        }
        object {Sapin
                rotate y*45
        	scale 1
        	translate <-1/2,Haut_Sup_Truc,0>
        	// rotate y*90
        }
        object {Sapin
        	scale 1.1
        	translate <Rayon_TrucX*.56,Haut_Sup_Truc,Rayon_TrucZ/2>
        	// rotate y*90
        }
        object {Sapin      
                rotate y-y*38
        	scale 1.2
        	translate <0,Haut_Sup_Truc,Rayon_TrucZ*.56>
        	// rotate y*90
        }
        object {Arbre1
        	scale 1.2
        	translate <-Rayon_TrucX*.57,Haut_Sup_Truc,Rayon_TrucZ*.03>
        	// rotate y*90
        }
        object {Sapin
        	scale 1
        	translate <-Rayon_TrucX/2,Haut_Sup_Truc,Rayon_TrucZ/3>
        	// rotate y*90
        }
        translate <0,Haut_Neige,0>
}        
#end

#ifdef (SOL) 
#declare Sol = intersection {
        blob {
                #local T_X = -1;
                #while (T_X<=1)
                        #local T_Z=-1;
                        #while (T_Z <=1)                          
                                sphere {0,.2,.5           
                                        translate <T_X,.2*(rand(R1)-1.2),T_Z>
                                }
                                #local T_Z = T_Z + .05 ;
                        #end    
                        #local T_X = T_X + .05;
                #end                
        }
        sphere {0,1-Ep_Globe/Rayon_TrucX-3*Epsilon}
        scale <Rayon_TrucX-Epsilon,Haut_Dome-Epsilon,Rayon_TrucZ-Epsilon>
        translate y*Haut_Sup_Truc
        texture {T_Neige}
}       
#end             
                       

#declare Truc_a_retourner = union {
        object {Support}
        #ifdef (GLOBE) object {Globe_1} #end
        #ifdef (ARBRES) object {Arbres} #end
        #ifdef (SOL) object {Sol} #end
        #ifdef (MAISON) object {Maison 
                scale 1/5
                rotate -y*10 
                translate <.1*Rayon_TrucX,Haut_Sup_Truc+.2,-.6*Rayon_TrucZ> 
        //        rotate -y*45
        } #end
        #ifdef (NEIGE_TOIT) object {Neige_Toit 
                scale 1/5
                rotate -y*10 
                translate <.1*Rayon_TrucX,Haut_Sup_Truc+.2,-.6*Rayon_TrucZ> 
        //        rotate -y*45
        } 
        #end
        #ifdef (NEIGE_TOMBE)
        object {Neige_Tombe}
        #end
}
        


/******************************/
/*                            */
/*   Scene en elle m�me       */
/*                            */
/******************************/

object {Truc_a_retourner}

// La table
#ifndef (BELLE_TABLE) /*
plane  {y,0 
        texture {
                pigment {color Brown}
                finish {phong 1.0 phong_size 20}
        } 
}                       */
#else                 
#declare Rayon_Table = 1;
union {
        box  {<-6,-3,-4>,< 6,0,4>}           
        sphere {<-6,-Rayon_Table,4>,Rayon_Table}
        sphere {<6,-Rayon_Table,4>,Rayon_Table}
        cylinder {<-6,0-Rayon_Table,4>,<6,0-Rayon_Table,4>,Rayon_Table}
        cylinder {<-6,0-Rayon_Table,4>,<-6,0-Rayon_Table,-4>,Rayon_Table}
        cylinder {<6,0-Rayon_Table,-4>,<6,0-Rayon_Table,4>,Rayon_Table}
        
        texture {
                T_Table
        } 
}
#end

// Le mur du fond
#ifndef (BEAU_MUR)
        plane  {z,5 
                hollow
                texture {
                        pigment {color Blue}
                        finish {
        //                        ambient .3
                                phong 1.0 
                                phong_size 20
                        }
                } 
        }                 
/*#else
plane  {z,5 
        hollow
        texture {
                pigment {color Blue}
                finish {
                        ambient 1
                        phong 1.0 
                        phong_size 20
                }
        } 
}                 
  */
#end
/*
light_source {10000*(y-z) color 1*White}
        
light_source {10000*(y/1000-z) color 1*White}
  */      

light_source {<-50,50,-30> color White}

sky_sphere {
        pigment {
                gradient y
                color_map {
                        [0 color MidnightBlue]
                       // [0 color MidnightBlue]
                     //   [.5 color Cyan]
                        [1 color Black]
                }  
         }                   
}                 
 /* pigment {color White}} */
 
#switch (CAMERA)
#case (1)  
        camera {location <0,2,-6>
                direction 2*z
        	look_at <0,1,0>
        }
        #break
#case (2)                                  
        camera {location <-1.,3,-4>
                look_at 1.2*y
        } 
        #break
#end      

	
	