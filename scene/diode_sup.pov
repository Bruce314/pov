#include "colors.inc"
#include "textures.inc"
                                     
#declare C1 = color Red;                                     
#declare C2 = color Green;
                                     
#declare Diode_Supelec = array [7][10] {
        {C1,C1,C1,C1,C1,C2,C1,C1,C1,C1},
        {C1,C1,C1,C1,C1,C2,C1,C1,C2,C2},
        {C1,C1,C1,C1,C1,C1,C2,C2,C1,C1},
        {C1,C1,C1,C1,C1,C1,C2,C2,C1,C1},
        {C1,C1,C1,C2,C2,C2,C1,C1,C2,C1},
        {C2,C2,C2,C1,C1,C1,C1,C1,C2,C1},
        {C1,C1,C1,C1,C1,C1,C1,C1,C2,C1},
}

#declare Rayon_Diode = 0.1;

#declare Tableau = union {
        #local X = 0;
        #while (X<10)
                #local Y = 0;
                #while (Y<7)
                        sphere {0,Rayon_Diode translate <X,Y,-0.33>*Rayon_Diode*2 
                                texture {pigment {color Diode_Supelec [6-Y][X] filter .9 transmit .5}}
                        }                                                   
                        #if (mod (X,3) = 0)
                                light_source {<X,Y,-0.33>*Rayon_Diode*2 color White}
                        #end
                        #local Y=Y+1;
                #end
                #local X=X+1;
        #end
        box {<-Rayon_Diode*2,-Rayon_Diode*2,0>,<10*Rayon_Diode*2,7*Rayon_Diode*2,Rayon_Diode>
               texture {Gold_Metal}
        }
}                        

object  {Tableau translate <-10*Rayon_Diode,-7*Rayon_Diode,0>}

camera {location <0,0,-3>}

// light_source {-1000*z color White}
