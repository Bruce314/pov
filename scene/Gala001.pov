/* GALA #001 
Ah l'Egypte
*/

#include "colors.inc"
#include "textures.inc"
#include "vegetaux.inc"

sky_sphere {pigment {color White}}

#declare Feuill1=Feuille (0,0,.2*x)

object {Feuill1
        rotate y*45
        scale 2
        }

light_source{1000*(y-z) color White}

camera {
        location <0,5,-20>
        direction z
        look_at x
        }