#include "colors.inc"

#declare Ray2=.3;        
#declare Haut4 = .5;
#declare Larg2= .9;
#declare Deb1=Ray2/4;


#declare Ray3 = (Larg2/2-Ray2 + Deb1)/2;
#declare Chapiteau = union {
        box {<-Ray2 - Ray3 +Deb1,0,-Ray2>,<Ray2+Ray3 -Deb1,Haut4,Ray2>}
        cylinder {<0,0,-Ray2>,<0,0,Ray2>,1
                scale <Ray3,Haut4,1>
                translate <-Ray2 - Ray3 +Deb1,0,0>
                }
        cylinder {<0,0,-Ray2>,<0,0,Ray2>,1
                scale <Ray3,Haut4,1>
                translate <+Ray2 + Ray3 -Deb1,0,0>
                }
        texture {pigment {color Blue}}
        }

object {Chapiteau}
        
camera {
        location <0,1,-4>
        look_at 0
        }
        
light_source {1000*(x/2+y-z) color White}        
                