#include "colors.inc"
#include "textures.inc"
#include "stones.inc"
#include "matrices.inc"
#include "chaine.inc"
#include "robot02.inc"


//jouent le r�le de bool�ens
#declare T_Stat=1; //statues textur�s?
#declare P_Stat=1; //statues pr�sentes  
#declare T_Col=1;  //Colonnes texture�s
#declare Beau_Sol=1; //Sol haute qualit�?
#declare P_Prieurs = 1; //les prieurs sont pr�sents?
#declare P_Torche =1;     //Les torches sont dessin�es?
#declare T_Torche =1;   //Les torches sont textur�es?
#declare P_Light = 1;   //Les torches �clairent??
#declare P_Stat2=1;     //La statue du fond
#declare P_Couronne = 1; // et sa couronne
#declare P_Chapiteau = 1;          //permet de faire des belles volonnes
#declare P_Rosace = 1; //pr�sence de la rosace
#declare T_Mur = 1; //permet de texturer les murs
#declare P_Chaine = 1; //chaines de protection de la grosse statue
#declare Chevelus = 0; //les robots sont-ils chevelus?
#declare P_Lustre = 1;
#declare P_Atmo = 1;

#declare Camera = 3;

#declare Haut1=6;
#declare Haut2=2;
#declare Haut3=.5;
#declare Ray1=.6;
#declare Ray2=.3;        
#declare n1=3;
#declare n2=5; //N2 valait 5
#declare Larg1=5;
#declare Long1=5;
#declare Ray3 = .05;
#declare Lim=24;

#declare Eps=.005;
   
#declare Ep1=.035;                     //.025
#declare Angle=25;       
#declare Lim1=5;                   

#declare Larg2= 2;
#declare Haut4 = .3; 
#declare Haut5 = 1.5;

#declare Prof1 = 2*Larg1;

//d�finition de la texture de la rosace
#declare Rosace1 = texture {
        pigment {spiral1 3 color_map {
                [0 color Red filter 1]
                [.5 color Yellow filter 1]
                [1 color Orange filter 1]
                }
                rotate 0
                }
        }                

#declare Rosace2 = texture {
        pigment {spiral1 3 color_map {
                [0 color Green filter 1]
                [.5 color DarkGreen filter 1]
                [1 color LimeGreen filter 1]
                } 
                rotate -x*180
                }
        }       
                 
#declare Plomb = texture {pigment {color Black}}
#declare Rosace3=texture {onion scale .4 texture_map {
                        [0 Rosace1]
                        [.499 Rosace1]
                        [.5 Plomb]
                        [.501 Rosace2]
                        [1 Rosace2]
                        }}
#declare Rosace4=texture {onion scale .4 texture_map {
                        [0 Rosace2]
                        [.499 Rosace2]
                        [.5 Plomb]
                        [.501 Rosace1]
                        [1 Rosace1]
                        }}        
#declare T_Rosace = texture {radial 
                texture_map {
                        [0 Rosace3 rotate x*90]
                        [.499 Rosace3 rotate x*90]
                        [.5 Plomb]
                        [.501 Rosace4 rotate x*90]
                        [1 Rosace4 rotate x*90]
                        }                     
                rotate x*90                        
                frequency 6                        
                }        

//texture des chaines de protection

#declare T_Chaine = texture {Gold_Metal}
    
// D�finition des textures du sol    
#if (Beau_Sol = 1)                                         
#declare T_Sol_1=texture {
        pigment {Jade}      
        pigment {Blue_Agate scale .1}
        finish {Shiny}
        }
#declare T_Sol_2=texture {
        pigment {Sapphire_Agate scale .1}
        finish {Shiny}
        }         
#declare T_Sol_3=texture {
        //pigment {Jade}      
        //finish {Shiny}
        T_Stone9                    
        scale .1
        }
#end        
                                         
// D�finition des textures des statues                                         
#if (T_Stat = 1)
#declare T_1=texture  {
        
        pigment {White_Marble scale .5} //White_Marble
        finish {phong 1.0 
                phong_size 20 
                //reflection .3
                }
        }
#declare T_1 = texture {
        pigment {agate color_map {
                [0 color rgb <.6,0,0>]
                [.3 color Gray30]    
                [.6 color rgb <.7,.3,.2>]
                [1 color Gray70]
                }}
        finish {phong 1.0 
                phong_size 20 
                //reflection .3
                }
        }
                
#else             

#declare T_1=texture {pigment {Yellow}}
#end

//D�finition des textures des colonnes
#if (T_Col = 1)
#declare T_Col1=texture {
        pigment {Red_Marble scale .1}  //Red_Marble
        finish {phong .5 phong_size 35}
        rotate -z*30
        }           
#declare T_Col1 = texture {
        pigment {granite color_map{
                        [0 color BlueViolet]
                        [.2 color MidnightBlue]
                        [.5 color Gray70]
                        [.6 color White]
                        [.8 color DarkTurquoise]
                        [.9 color Gray30]
                        [1 color MidnightBlue]
                        }
                turbulence .3                        
                scale .4                        
                }        
        finish {
                }
                        
        }                                
#declare T_Col2=texture {T_Col1 rotate z*65}
#declare T_Black = texture {
        pigment {color Black}
        }
#end        


//D�claration des textures du mur
#declare T_Mur1 = texture {T_Grnt4}
#declare T_Mur2 = texture {T_Grnt7}        

#declare T_2=texture  {T_1}
#declare T_3=texture  {T_1}
#declare Pas_de_Logo=1;             
#if (Chevelus = 0)
#declare Pas_de_Cheveux = 1;
#end


// Les statues, fa�on statue de la libert�, avec des torches
#declare Robot_Statue = Droide1 (10,-10,10,-10, //jambes         
                                110,45,15,85, //humerus
                                135, 70,100,150, //radius //135,,100,
                                0,5,10,12,0,80)  //bassin et mains                      
// On flingue les identificateurs, ce qui permet de redessiner des robots standards                                
#undef T_1
#undef Pas_de_Logo
#undef T_3
#undef T_2
#if (Chevelus = 0)
#undef Pas_de_Cheveux
#end

#render concat ("Cr�ation d'une torche",chr(13),chr(10))
#declare Torche1=union {
        torus {.3,Ep1
                rotate x*25
                translate y
                }
        torus {.3,Ep1
                rotate -x*25
                translate y
                }
        }                

#declare Flame=interior {
    media {
      emission 8.05
      intervals 10
      samples 1, 10
      confidence 0.9999
      variance 1/1000
      density {
        spherical
        ramp_wave
        turbulence <.3,.5,.3>
        color_map {
          [0.0 color rgb <0, 0, 0>]
          [0.1 color rgb <0,0,.5>]
          [0.15 color rgb <0,0,0>]
          [0.2 color rgb <1, 0, -.5>]
          [0.8 color rgb <1,.5,-.5>]
          [1.0 color rgb <1, 1, -.5>]
        } 
      }
    translate -.2*y
    scale <.4,1,.3>
    }
  }    


#declare Torche = union {
        cone {0,.07,y,.2}
        sphere {0,.1}
        #declare Temoin=0;
        #while (Temoin <Lim1)             
                object {Torche1
                        rotate y*180*Temoin/Lim1
                        }
                #declare Temoin=Temoin+1;
                #end     
        #if (P_Light = 1)
        light_source {1.4*y color rgb .06*<1,1,.5>
                media_interaction off
                 media_attenuation off
                 } //.06 avant
        #end
        sphere {0,1
                interior {Flame} 
                hollow
                scale <.4,.8,.4> //.2,.5,.2
                translate 1.4*y
                no_shadow
                texture {
                        pigment {color Clear }
/*                        finish {
                                ambient .8
                                }         */
                        }
                }                
        #if (T_Torche = 1)                
        texture {
                // pigment {Jade}
                // finish {Shiny}
                T_Grnt7
                }
        #else                
        texture {pigment {color Red}}                
        #end
        }                



#render concat ("Cr�ation d'un chapiteau",chr(13),chr(10))
#declare Deb1 = Ray2/2;
#declare Ray4 = Larg2/4 - Ray2/2 + Deb1/2;

#declare Cote_Chapiteau_I = 
        union {
                //cylinder {<0,0,-Ray2>,<0,0,Ray2>,1}
                torus {6/7,1/7
                        rotate -x*90
                        scale <1,1,.5>
                        translate <0,0,Ray2/3>
                        }
                torus {6/7,1/7
                        rotate -x*90
                        scale <1,1,.5>
                        translate <0,0,2*Ray2/3>
                        }
                torus {6/7,1/7
                        rotate -x*90
                        scale <1,1,.5>
                        translate <0,0,-Ray2/3>
                        }
                torus {6/7,1/7
                        rotate -x*90
                        scale <1,1,.5>
                        translate <0,0,-2*Ray2/3>
                        }
                torus {6/7,1/7
                        rotate -x*90
                        scale <1,1,.5>
                        translate <0,0,-0>
                        }
                sphere {0,1/7                    
                        scale <1,1,.5>
                        translate <0,0,-Ray2>
                        }
                torus {2/7,1/7
                        rotate -x*90
                        scale <1,1,.5>
                        translate <0,0,-Ray2>
                        }
                torus {4/7,1/7
                        rotate -x*90
                        scale <1,1,.5>
                        translate <0,0,-Ray2>
                        }
                torus {6/7,1/7
                        rotate -x*90
                        scale <1,1,.5>
                        translate <0,0,-Ray2>
                        }
                sphere {0,1/7
                        scale <1,1,.5>
                        translate <0,0,Ray2>
                        }
                torus {2/7,1/7
                        rotate -x*90
                        scale <1,1,.5>
                        translate <0,0,Ray2>
                        }
                torus {4/7,1/7
                        rotate -x*90
                        scale <1,1,.5>
                        translate <0,0,Ray2>
                        }
                torus {6/7,1/7
                        rotate -x*90
                        scale <1,1,.5>
                        translate <0,0,Ray2>
                        }
                }                        


#declare Cote_Chapiteau_D =difference {
        object {Cote_Chapiteau_I}
        box {<-2,0,-2>,<0,2,2>}
        }                
        
#declare Cote_Chapiteau_G =difference {
        object {Cote_Chapiteau_I}
        box {<0,0,-2>,<2,2,2>}
        }                
        

#declare Chapiteau = union {
        union {
                cylinder {<-Ray2-Ray4+Deb1,0,0>,<Ray2+Ray4-Deb1,0,0>,1/7}
                cylinder {<-Ray2-Ray4+Deb1,2/7,0>,<Ray2+Ray4-Deb1,2/7,0>,1/7}
                cylinder {<-Ray2-Ray4+Deb1,4/7,0>,<Ray2+Ray4-Deb1,4/7,0>,1/7}
                cylinder {<-Ray2-Ray4+Deb1,6/7,0>,<Ray2+Ray4-Deb1,6/7,0>,1/7}
                scale <1,Haut4,.5>
                translate <0,0,-Ray2>
                }                
        union {
                cylinder {<-Ray2-Ray4+Deb1,0,0>,<Ray2+Ray4-Deb1,0,0>,1/7}
                cylinder {<-Ray2-Ray4+Deb1,2/7,0>,<Ray2+Ray4-Deb1,2/7,0>,1/7}
                cylinder {<-Ray2-Ray4+Deb1,4/7,0>,<Ray2+Ray4-Deb1,4/7,0>,1/7}
                cylinder {<-Ray2-Ray4+Deb1,6/7,0>,<Ray2+Ray4-Deb1,6/7,0>,1/7}
                scale <1,Haut4,.5>
                translate <0,0,Ray2>
                }                
                
        box {<-Ray2-Ray4+Deb1,0,-Ray2>,<Ray2+Ray4-Deb1,Haut4,Ray2>}
        object {Cote_Chapiteau_D
                scale <Ray4,Haut4,1>
                translate <Ray2+Ray4-Deb1,0,0>
                }
        object {Cote_Chapiteau_G
                scale <Ray4,Haut4,1>
                translate <-Ray2-Ray4+Deb1,0,0>
                } 
        #if (T_Col = 1)                
        texture {T_Col1}                
        #else                       
        texture {pigment {color (Red+White)/2}}
        #end
        translate y*Haut1
        }        
        

#render concat ("Cr�ation d'une colonne",chr(13),chr(10))
//D�claration d'une colonne, avec �ventuellement ses deux statues
#declare Colonne1=union {
        #local Rayon_H = Ray1 + (Ray2-Ray1)*Haut2/Haut1;
        #if (P_Chapiteau = 1)                   
        object {Chapiteau}   
        #else
        cylinder {<0,Haut1,0>,<0,Haut1+Haut4,0>,Ray2+Ray4}
        #end
        cone {0,Ray1,Haut1*y,Ray2 }   
        #if (P_Chapiteau = 1)
        #declare Tem = 0;
        #while (Tem < Lim)
                cylinder {<Ray1,0,0>,<Ray2,Haut1,0>,Ray3
                        rotate y*Tem*360/Lim 
                        }
                #declare Tem = Tem + 1;
                #end         
        #end                
        torus {Ray1,Ray3*1.5}          
        #if (T_Col = 1) 
        texture {
                gradient y texture_map {
                        [0  T_Col1]
                        [.5-2*Eps T_Col1]
                        [.5-Eps T_Black]
                        [.5 T_Col2]
                        [1-2*Eps T_Col2]
                        [1-Eps T_Black]
                        }
                }
        #else                                        
        texture {pigment {color Red}}
        #end
        }

#declare Colonne = union {
        object {Colonne1}        
        intersection {
                sphere {0,1        
                        scale <2*Rayon_H,Haut3,Rayon_H>
                        }
                plane {y,0}
                translate Haut2*y
                }                        
        torus {.97,.03                
                scale <2*Rayon_H,4*Haut3,Rayon_H>
                translate Haut2*y
                }
        #if (P_Stat = 1)
        object {Robot_Statue
                scale .2
                rotate y*90
                translate <-Rayon_H*1.45,Haut2,0>    //1.5
                }
        object {Robot_Statue
                scale .2
                rotate -y*90
                translate <Rayon_H*1.45,Haut2,0> //1.5
                }
        #end    
        #if (P_Torche = 1)            
        object {Torche 
                scale 1.8 
                translate <-4.3,11.5,-3>        
                scale .2
                rotate y*90
                translate <-Rayon_H*1.5,Haut2,0>
                }          
        object {Torche
                scale 1.8 
                translate <-4.3,11.5,-3>        
                scale .2
                rotate -y*90
                translate <Rayon_H*1.5,Haut2,0>
                }          
        #end             
        #if (T_Col = 1) 
        texture {T_Col1}
        #else                                        
        texture {pigment {color Red}}
        #end
        }

#if (P_Chaine = 1)
#declare Chaine01 = Chaine (<-Larg1/2,0,0>,<Larg1/2,0,0>,.2,2)
#end

//le temple, regrouepement de toutes les colonnes  
#render concat ("Creation du temple",chr(13),chr(10))

#declare Contour = merge {   
//le contour de d�part
        box {<-Larg1*n1/2,-1,-Long1*n2/2>,<Larg1*n1/2,Haut1+Haut4,Long1*n2/2>
                }     
//la rosace        
        #if (P_Rosace = 1)                               
        cylinder {-.01*z,.01*z,1       
                scale <1.5,2,1>
                texture {T_Rosace}
                translate <0,5,Long1*n2/2+Prof1/2>
                }         
        #end                
        #declare Tem_x = -n1/2;                
                #while (Tem_x < n1/2)
                intersection {
                        cylinder {<-1/2,0,-Long1*(n2+1)/2>,<-1/2,0,Long1*(n2+1)/2>,1}
                        cylinder {<1/2,0,-Long1*n2/2>,<1/2,0,Long1*n2/2>,1}
                        scale <2*(Larg1/2 - Ray2),Haut5*2/sqrt(3),1>
                        translate <Larg1*(Tem_x+1/2),Haut4+Haut1,0>
                        }
                #declare Tem_x=Tem_x+1;
                #end
        #declare Tem_x = -n2/2;                
                #while (Tem_x < (n2)/2)                  
                intersection {
                        cylinder {<-Larg1*(n1+1)/2,0,1/2>,<Larg1*(n1+1)/2,0,1/2>,1}
                        cylinder {<-Larg1*n1/2,0,-1/2>,<Larg1*n1/2,0,-1/2>,1}
                        scale <1,Haut5*2/sqrt(3),2*(Long1/2 - Ray2)>
                        translate <0,Haut4+Haut1,Long1*(Tem_x+1/2)>
                        }
                #declare Tem_x=Tem_x+1;         
                #end
        intersection {
                merge {                
                intersection {
                        sphere {<-1/2,0,0>,1}
                        sphere {<1/2,0,0>,1}
                        scale <Larg1-Ray2,Haut5*2/sqrt(3),Prof1>
                        translate <0,Haut4+Haut1,Long1*n2/2>
                        }
                intersection {
                        cylinder {<-1/2,0,0,>,<-1/2,Haut4+Haut1,0>,1}
                        cylinder {<1/2,0,0,>,<1/2,Haut4+Haut1,0>,1}
                        scale <Larg1-Ray2,1,Prof1>
                        translate <0,0,Long1*(n2)/2>
                        }
                }
                plane {z,Long1*n2/2+Prof1*1/2} //2/3
                }      
        #if (P_Light = 0)                
        no_shadow        
        #end
        hollow                
        #if (T_Mur = 1)
        texture {brick texture {T_Mur2}, texture {T_Mur1} 
                brick_size .3*<1,1,1>
                mortar .3*.1
                }
        #else
        texture {pigment {color White}}           
        #end
        //la poussi�re ambiante
        #if (P_Atmo = 1)
        interior {
        media {
                intervals 10
                scattering { 1, rgb 0.03}
                samples 1, 10         
                confidence 0.9999
                variance 1/1000
                ratio 0.9
                } 
                }
        #end

        }
                        

#declare Temple = union {                     
        object {Contour}        
        #declare Tem_x=-n1/2;  
        #while (Tem_x<=n1/2)
                #declare Tem_z=-1/2; //-n2/2;
                #while (Tem_z<n2/2)
                        object {Colonne
                                translate <Larg1*Tem_x,0,Long1*Tem_z>
                                }    
                        #declare Tem_z = Tem_z + 1;                        
                        #end                       
                #declare Tem_x=Tem_x+1;                
                #end            
        #declare Tem_x = -n1/2;                
        #while (Tem_x<=n1/2)                 
                object {Colonne1
                        translate <Larg1*Tem_x,0,Long1*n2/2>
                        }    
                #declare Tem_x=Tem_x+1;                
                #end                   
        #if (P_Chaine = 1)                
        object {Chaine01 texture {T_Chaine} translate <0,.33,Long1*n2/2>}
        object {Chaine01 texture {T_Chaine} rotate x*60 translate <0,.66,Long1*n2/2>}                
        object {Chaine01 texture {T_Chaine} rotate x*30 translate <0,1,Long1*n2/2>}                
        #end
        }


// D�claration des robots priants, on ne le fait que si ils doivent �tre dessin�s
// pour limiter les frais en temps de parsing
#if (P_Prieurs = 1)            
#render concat ("Creation des robots prosternes",chr(13),chr(10))

#declare Aleat1 = seed (1);
#declare Aleat2 = seed (45);

#if (Chevelus = 0)
#declare Pas_de_Cheveux = 1;
#end                           
#declare Pas_de_Logo = 1;
#declare Robot_Priant = Droide1 (60,-102,60,-102,90,45,90,45,100,120,100,120,0,-60,10,10,0,0)                        
#if (Chevelus = 0)
#undef Pas_de_Cheveux    
#undef Pas_de_Logo
#end
#if (Chevelus = 0)
#declare Pas_de_Cheveux = 1;
#end                           
#declare Robot_Priant2 = Droide1 (60,-102,60,-102,90,45,90,45,100,120,100,120,0,-60,10,10,0,0)                        
#if (Chevelus = 0)
#undef Pas_de_Cheveux    
#end

#declare Croyants = union {        
        #declare Tem_x=-n1/2;
        #while (Tem_x<n1/2)
                #declare Tem_z=-1/2; //-n2/2;                                                    
                #while (Tem_z<=n2/2)
                        // #if (Tem_x + 10 - ceil (Tem_x) = 0)
                        #declare X_Ofs = Larg1/20 * 2*(rand(Aleat1)-.5);
                        #declare Z_Ofs = Long1/30 * 2*(rand(Aleat1)-.5);
                        #if (rand (Aleat2)>.8)
                        object {Robot_Priant2
                                scale .1
                                rotate y*(180 - 180/pi*atan2(Larg1*(Tem_x+1/2)+X_Ofs,Long1*(-Tem_z+9/16-Z_Ofs+n2/2)+Prof1/4))
                                translate <Larg1*(Tem_x+1/2) + X_Ofs,0,Long1*(Tem_z-9/16) + Z_Ofs>
                                }            
                        #else
                        object {Robot_Priant
                                scale .1
                                rotate y*(180 - 180/pi*atan2(Larg1*(Tem_x+1/2)+X_Ofs,Long1*(-Tem_z+9/16-Z_Ofs+n2/2)+Prof1/4))
                                translate <Larg1*(Tem_x+1/2) + X_Ofs,0,Long1*(Tem_z-9/16) + Z_Ofs>
                                }
                         #end     
                        #declare Tem_z = Tem_z + 1/4;                        
                        #end                  
                #declare Tem_x=Tem_x+1/4;                
                #end
        }
#end                                         

#if (Beau_Sol = 1)
#declare N_X=3;
#declare N_Z=2;
#declare Dalle1 = superellipsoid {<.2,.6> //.2,.6                                        
        scale <.55/N_X,.1,.6/N_Z> //.62
        translate -.09*y 
        }
        
#declare Sol = union {
        #declare Tem_x = -ceil(n1/2*Larg1);
        #while (Tem_x <= n1/2*Larg1)       
                #declare Tem_y = -ceil(n2/2*Long1);
                #while (Tem_y <= n2/2*Long1 + Prof1/2)       
                        object {Dalle1
                                translate <Tem_x ,0,Tem_y>
                                #switch (mod (N_X*Tem_x + 2*N_Z*Tem_y+300,10))
                                #case (0)                                   
                                #case (7)
                                #case (4)
                                texture {T_Sol_3}
                                #break
                                #case (8)
                                #case (5)
                                texture {T_Sol_1}
                                #break
                                #case (1) 
                                texture {T_Sol_2 translate 3*y rotate x*45}
                                #break
                                #case (6) 
                                #case (3)                    
                                texture {T_Sol_2 translate -3*x-4*y}
                                #break                      
                                #else
                                texture {T_Sol_2}
                                #end
                                }                
                        #declare Tem_y = Tem_y +1/N_Z;          
                        #end
                
                #declare Tem_x = Tem_x +1/N_X;          
                #end                                     
        plane {y,-0.5 texture {pigment {color Black}}}                
        }         
#end               

#if (P_Couronne = 1)
#declare Kron1 = torus {1,.08}
#declare Kron2 = union {
        object {Kron1
                rotate x*20
                }
        object {Kron1
                rotate -x*20
                }
        }            
#declare Pierre1 = sphere {0,1
        scale <.2,.29,.1>
        translate z*1.1*cos (pi*20/180)
        }        

#declare Pierre1 = sphere {0,1
        scale <.2,.29,.1>
        translate z*1.1*cos (pi*20/180)
        }        
#declare Pierre2 = sphere {0,1
        scale <.17,.17,.2>
        }
#declare Pierre3 = sphere {0,1
        scale <.15,.13,.1>
        }
#declare Kron3=union {
        union {
                cylinder {0,1.5*y,1}
                sphere {0,1 scale <1,.1,1>}
                sphere {0,1 scale <1,.2,1> translate 1.5*y}
                scale <.2,1,.1>
                translate <0,-sin(20*pi/180),-cos(20*pi/180)>
                }  
        union {                             
                cylinder {-x,x,1
                        scale <.5,.2,.1>
                        }
                sphere {0,1
                        scale <.2,.2,.1>
                        translate -.5*x
                        }
                sphere {0,1
                        scale <.2,.2,.1>
                        translate .5*x
                        }
                translate <0,1-sin(20*pi/180),-cos(20*pi/180)>
                }
        object {Pierre2
                texture {
                        pigment {color rgbf <.9,.9,1,.95>}
                        finish {phong 1.0
                                phong_size 100
                                reflection .1
                                }
                        }
                interior {
                        ior 2.4
                        }
                translate <0,-sin(20*pi/180)+1,-cos(20*pi/180)-.05>
                }               
        object {Pierre3
                texture {
                        pigment {color rgbf <0,.6,.2,.9>}
                        finish {phong 1.0
                                phong_size 100
                                reflection .1
                                }
                        }
                interior {
                        ior 1.9
                        }
                translate <.4,-sin(20*pi/180)+1,-cos(20*pi/180)-.05>
                }               
        object {Pierre3
                texture {
                        pigment {color rgbf <0,.6,.2,.9>}
                        finish {phong 1.0
                                phong_size 100
                                reflection .1
                                }
                        }
                interior {
                        ior 1.9
                        }
                translate <-.4,-sin(20*pi/180)+1,-cos(20*pi/180)-.05>
                }               
        texture {Gold_Metal}                
        }                

#declare Couronne = union {
        #declare Temoin=0; 
        #declare Lim=4;
        #while (Temoin <Lim)
                object {Kron2
                        rotate y*Temoin/Lim*360
                        texture {Gold_Metal}
                        }
                #declare Temoin=Temoin+1;
                #end
        #declare Temoin=0; 
        #declare Lim=Lim;
        #while (Temoin <Lim)
                object {Pierre1
                        #switch (mod (Temoin,2))
                        #case (0)  
                                texture {
                                        pigment {color rgbf <.8,0,0,.9>}
                                        finish {phong 1.0 phong_size 20}
                                        }
                                #break                                                                        
                        #case (1)
                                texture {
                                        pigment {color rgbf <0,0,.8,.9>}
                                        finish {phong 1.0 phong_size 20}
                                        }
                                #break               
                        #end               
                        interior {
                                ior 1.5
                                }
                        rotate y*(Temoin+1/2)/Lim*360
                        }
                #declare Temoin=Temoin+1;
                #end
        object {Kron3                
                }
        }                
#end

#if (P_Stat2 = 1) 
#declare Pas_de_Logo = 1;
#declare Pas_de_Cheveux = 1;
#declare T_1 = texture {Silver_Metal}
#declare T_2 = texture {Copper_Metal}
#declare Statue_Fond0 = Droide1 (8,-8,8,-8
        30,0,30,0,
        -50,0,-50,0,
        0,0,45,45,90,90)             
#undef T_2        
#undef T_1        
#undef Pas_de_Logo       
#undef Pas_de_Cheveux 
#declare Statue_Fond= union {
        object {Statue_Fond0
                }
        #if (P_Couronne = 1)                                
        object {Couronne   
                scale 2*.3
                translate 2*5.9*y
                }           
        #end                                
        scale .48 //.5   
        rotate y*0
        }        
#end        

#if (P_Lustre = 1)
#declare Flame2=interior {
    media {
      emission 8.05
      intervals 10
      samples 1, 10
      confidence 0.9999
      variance 1/1000
      density {
        spherical
        ramp_wave
        turbulence <.3,.1,.3>
        color_map {
          [0.0 color rgb <0, 0, 0>]
          [0.15 color rgb <.5,0,0>]
          [0.2 color rgb <1, 0,0>]
          [0.8 color rgb <1,.5,0>]
          [1.0 color rgb <1, 1, 0>]
        } 
      }
    translate -.2*y
    scale .5*<.4,1,.3>
    }
  }    


#declare Lustre = union {
        intersection {
                difference {
                        sphere {0,1}
                        sphere {0,.99 texture {pigment {color White} finish {reflection 0.0 diffuse 1}}}
                        }                                
                plane {-y,-.8}
                }
        intersection {                
                torus {.6,.01
                        rotate -x*90
                        }
                plane {y,0}
                translate .8*y
                }        
        intersection {                
                torus {.6,.01
                        rotate -z*90
                        }
                plane {y,0}
                translate .8*y
                }                 
        cylinder {-.05*y,.05*y,.1
                translate y*.2
                }                
        torus {.07,.01
                rotate -x*90
                translate y*(1+.07)
                }
        torus {.6,.01
                translate .8*y
                }   
        sphere {0,1
                scale <.6,.35,.6>
                hollow      
                interior {Flame2
                        }
                texture {pigment {color Clear}}
                translate .55*y
                }                               
        #if (P_Light = 1)      
                #render "coucou"
                light_source {0 color White
                        spotlight
                        point_at -y
                        radius 60
                        falloff 90    
                        media_attenuation off
                        media_interaction off
                        translate .6*y
                        }
        #end                
        no_shadow
        texture {Polished_Brass}           
        rotate -y*90
        translate <0,-1.07,0>
        }                              
        
#declare Haut6 = .8;
#declare Lustre_Pendu = union {
        object {Lustre scale .7}
        Chaine (0,<0,Haut6,Long1/2-Ray2>,Long1/40,2)
        Chaine (0,<0,Haut6,-Long1/2+Ray2>,Long1/40,2)
        texture {Polished_Brass}
        translate -Haut6*y
        }
object {Lustre_Pendu
        translate <-Larg1/2,Haut1,Long1>
        }
object {Lustre_Pendu
        translate <-Larg1/2,Haut1,2*Long1>
        }

object {Lustre_Pendu
        translate <Larg1/2,Haut1,0*Long1>
        }
object {Lustre_Pendu
        translate <-Larg1/2,Haut1,0*Long1>
        }

object {Lustre_Pendu
        translate <Larg1/2,Haut1,2*Long1>
        }

object {Lustre_Pendu
        translate <Larg1/2,Haut1,Long1>
        }


                                    
#end

// PARTIE AFFICHAGE DES OBJETS
#render concat ("Positionnement dans la scene",chr(13),chr(10))
                              
// Les robots qui prient                              
#if (P_Prieurs =1)
object {Croyants }                                          
#end

#if (P_Stat2 = 1)
object {Statue_Fond
        translate z*(n2/2*Long1+Prof1/4)
        }
#end

// Le temple
object {Temple}

// Le sol, s�rement dans une premi�re version        
#if (Beau_Sol=1)
object  {Sol}
#else
plane {y,0                               
        texture {pigment {checker color Green color Blue}}
        }
#end        

sky_sphere  {pigment {color White}}
        
// Position de le cam�ra        
#switch (Camera)
#case (0) //camera finale
camera {
        location <Larg1/2-1.5*Ray1,2,-12>
        //direction 3*z
        look_at <0,2,0>
        }
#break
#case (1) //camera centree
camera {
        location <0,2,-5>
        direction 2*z
        look_at <0,2,0>
        }
#case (3) //camera d'essai
camera {
        location <Larg1/2-2*Ray1,2,-4>
        //direction 3*z
        look_at <0,2,Long1*n2/2>
        }
#break
#end        
// Position des sources de lumi�re interm�diaies 
#if (P_Light = 0)       
light_source {10000*(x/2+y-z) color .5*White media_attenuation on}
light_source {10000*(-x/2+y-z) color .5*White media_attenuation on}
#else
light_source {<0,800,2000> color 3*White media_attenuation on}                                                         
#end

