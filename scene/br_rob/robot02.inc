#declare L_Cuisse=2.7;
#declare L_Mollet=2.4;
#declare L_Humerus=2.4;
#declare L_Radius=2.2;

#declare H_Pied=.8;
#declare R_Pied=.6;
#declare Sqrt_2=.7071067812;   
#declare Ang1=7;
#declare Pi=3.14159265358;
#declare R_Membres=.25;
#declare R_Doigt=.1;
#declare L_Doigt=.7;

#declare L_Pas=(L_Mollet*(sin(10*pi/180)+sin(30*pi/180))+L_Cuisse*(sin(30*pi/180)+sin(20*pi/180))*cos (Ang1*pi/180));

#macro Main_Droite1 (J1A1,J1A2,J2A1,J2A2,
                B1A1,B1A2,B2A1,B2A2,
                B1A3,B1A4,B2A3,B2A4,
                R_B_Y, R_B_X, O_M_D, O_M_G,
                 R_M_D, R_M_G)
#local Haut=max (
        .9*Sqrt_2+L_Cuisse*cos (Ang1*Pi/180)*cos (J1A1*Pi/180)+L_Mollet*cos (J1A2*Pi/180)+H_Pied,
        .9*Sqrt_2+L_Cuisse*cos (Ang1*Pi/180)*cos (J2A1*Pi/180)+L_Mollet*cos (J2A2*Pi/180)+H_Pied)*y;

#local Centre_Coude=<-2-L_Humerus*sin (B1A1*Pi/180)*cos(B1A2*Pi/180),
                2.9-L_Humerus*cos (B1A1*Pi/180),
                -L_Humerus*sin (B1A1*Pi/180)*sin(B1A2*Pi/180)>;
#local Coude_Main=<-(L_Radius+L_Doigt)*sin (B1A3*Pi/180)*cos(B1A4*Pi/180),
                -(L_Radius+L_Doigt)*cos (B1A3*Pi/180),
                -(L_Radius+L_Doigt)*sin (B1A3*Pi/180)*sin(B1A4*Pi/180)>; 
#local Vect1=vrotate (Centre_Coude+Coude_Main,<0,R_B_Y,0>);
Haut+vrotate (Vect1,<R_B_X,0,0>)
#end

#macro Droide1 (J1A1,J1A2,J2A1,J2A2,
                B1A1,B1A2,B2A1,B2A2,
                B1A3,B1A4,B2A3,B2A4,
                R_B_Y, R_B_X, O_M_D, O_M_G,
                 R_M_D, R_M_G)

#render concat ("Vous rentrez dans la macro qui dessine le droide 1",chr (13),chr (10))

#ifdef (T_1)
#local T1=texture {T_1}
#else
#local T1=texture {pigment {color Orange}}
#warning concat ("texture du corps non definie",chr(13),chr(10))
#end

#ifdef (T_2)
#local T2=texture {T_2}
#else
#local T2=texture {pigment {color Blue}}
#warning concat ("texture des articulations non definie",chr(13),chr(10))
#end

#ifdef (T_3)
#local T3=texture {T_3}
#else
#local T3=texture {pigment {color Gray50}}
#warning concat ("texture des cheveux non definie",chr(13),chr(10))
#end

#local Haut=max (
        .9*Sqrt_2+L_Cuisse*cos (Ang1*Pi/180)*cos (J1A1*Pi/180)+L_Mollet*cos (J1A2*Pi/180)+H_Pied,
        .9*Sqrt_2+L_Cuisse*cos (Ang1*Pi/180)*cos (J2A1*Pi/180)+L_Mollet*cos (J2A2*Pi/180)+H_Pied);

#local Doigt= union {
       cylinder {0,<L_Doigt/3,-L_Doigt,0>,R_Doigt}
       sphere {<L_Doigt/3,-L_Doigt,0>,R_Doigt}
       cylinder {<L_Doigt/3,-L_Doigt,0>,<R_Doigt,-L_Doigt*3/2,0>,R_Doigt}
       sphere {<R_Doigt,-L_Doigt*3/2,0>,R_Doigt}
       }

/* 
Quelques cas de messages d'avertissements
Pour positions dignes du kama sutra... 
ou d'une fracture du genou!  
*/

#if (J2A2>J2A1)
#warning concat ("Jambe gauche non cohéerente",chr (13),chr (10))
#end

#if (J1A2>J1A1)
#warning concat ("Jambe droite non coherente",chr (13),chr (10))
#end  

#if (abs (R_B_Y)>45)
#warning concat ("Bassin trop tordu",chr (13),chr (10))
#end             

                                   
union {
  union {                               //        partie en haut de la ceinture
    union {                               //tete
      sphere {0,1                               //la sphere de base                       
        scale <.6,1.2,.6>
        translate 4.5*y
        }
      sphere {0,.1                              //un oeil
        translate <0,.2+4.5,-.55>
        rotate y*25
        texture {pigment {color White}}
        }  
      sphere {0,.1                              //et l'autre
        translate <0,.2+4.5,-.55>
        rotate -y*25
        texture {pigment {color White}}
        }
      #ifndef (Bass_Res) 
      #ifndef (Pas_de_Cheveux)   
        #local X0=-.6;                            //les trucs qui font penser aux cheveux
        #while (X0<=.6)
          #local Z0=-.4;  
          #while (Z0<=.6)
            #if (sqrt (X0*X0+Z0*Z0)<.6)
              cone {<X0,4.5,Z0>,.03,<X0,5.9,Z0>,.01
                texture {T3}
                }
            #end
            #local Z0=Z0+.04;  
          #end
          #local X0=X0+.04;
        #end  
      #end
      #end  
      }
    torus {.4,.2                  //cou
      translate 3.6*y
      texture {T2}
      }
    sphere {0,1                         //epaules
      scale <2,.6,1.1>
      translate 2.9*y
      }                    
    intersection {                      //torax
      sphere {0,1
        scale <1.4,3.4,1>               //avant 3.1 en y
        }
      plane {y,0}
      translate 2.9*y
      texture {T1}   
      #ifndef (Bass_Res)
        #ifndef (Pas_de_Logo)
        texture {
          pigment {image_map {gif "Supelec.gif" map_type 0 interpolate 2 once}
            translate <-.5,-.5,0>
            scale <2,1.5,1>
            translate 1.5*y
            } 
          }                  
          #end
      #end    
      }  
        
    sphere {0,1                   //artic epaule gauche
      scale <.4,.4,.4>
      translate <2,2.9,0>
      texture {T2}
      }            
    sphere {0,1                   //artic epaule droite
      scale <.4,.4,.4>
      translate <-2,2.9,0>
      texture {T2}
      }
    union {
      cylinder {0,-L_Humerus*y,R_Membres}  //Humerus Droit
      sphere {-L_Humerus*y,.4 
        texture {T2}
        }
      rotate -z*B1A1
      rotate -y*B1A2
      translate <-2,2.9,0>      
      }
    union{
      cylinder {0,-L_Humerus*y,R_Membres}  //Humerus Gauche
      sphere {-L_Humerus*y,.4 
        texture {T2}
        }
      rotate z*B2A1
      rotate y*B2A2
      translate <2,2.9,0>      
      }
    union {                                     //Radius gauche + poignet + main
      cylinder {0,-L_Radius*y,R_Membres}
      sphere {-L_Radius*y,.4
        texture {T2}
        }
      union {
        object {Doigt
          rotate z*O_M_G
          }  
   /*     object {Doigt
          rotate z*O_M_G
          rotate y*90
          }          */
        object {Doigt
          rotate z*O_M_G
          rotate y*180
          }  
        /*object {Doigt
          rotate z*O_M_G
          rotate y*270
          }  */
        translate -L_Radius*y  
        }
      rotate y*R_M_G      
      rotate -z*B1A3
      rotate -y*B1A4
      translate <-2-L_Humerus*sin (B1A1*Pi/180)*cos(B1A2*Pi/180),
                2.9-L_Humerus*cos (B1A1*Pi/180),
                -L_Humerus*sin (B1A1*Pi/180)*sin(B1A2*Pi/180)>                
      }                
    union {                                     //Radius droit + poignet + main
      cylinder {0,-L_Radius*y,R_Membres}
      sphere {-L_Radius*y,.4
        texture {T2}
        }
      union {
        object {Doigt
          rotate z*O_M_D
          }  
 /*       object {Doigt
          rotate z*O_M_D
          rotate y*120
          }  */
        object {Doigt
          rotate z*O_M_D
          rotate y*180
          }  
        /*object {Doigt
          rotate z*O_M_D
          rotate y*270
          }  */
        translate -L_Radius*y  
        }
      rotate y*R_M_D  
      rotate z*B2A3
      rotate y*B2A4
      translate <2+L_Humerus*sin (B2A1*Pi/180)*cos(B2A2*Pi/180),
                2.9-L_Humerus*cos (B2A1*Pi/180),
                -L_Humerus*sin (B2A1*Pi/180)*sin(B2A2*Pi/180)>                
      }                
    torus {.75,.3                  //taille                
      translate <0,.1,0>  
      texture {T2}
      }
    rotate y*R_B_Y
    rotate x*R_B_X  
    }  
  union {                         //partie en dessous de la ceinture
    intersection {                //"fesses"
      sphere {0,1
        scale <.9,1,.9> 
        }
      plane {y,0}     
      }
    sphere {0,1                   //artic jambe droite
      scale <.4,.4,.4>
      translate <-.9,0,0>
      rotate z*45
      texture {T2}
      }
    sphere {0,1                   //artic jambe droite
      scale <.4,.4,.4>
      translate <.9,0,0>
      rotate -z*45
      texture {T2}
      }
    union {                        //cuisse + genou droits
      cylinder {0,-L_Cuisse*y,R_Membres
        rotate -z*7 
        rotate x*J1A1
        translate <-.9*Sqrt_2,-.9*Sqrt_2,0>
        }            
      sphere {0,.4
        translate -L_Cuisse*y
        rotate -z*Ang1 
        rotate x*J1A1
        translate <-.9*Sqrt_2,-.9*Sqrt_2,0>
        texture {T2}
        }            
      }          
    union {                        //cuisse + genou gauches
      cylinder {0,-L_Cuisse*y,R_Membres
        rotate z*Ang1 
        rotate x*J2A1
        translate <.9*Sqrt_2,-.9*Sqrt_2,0>
        }            
      sphere {0,.4
        translate -L_Cuisse*y
        rotate z*Ang1 
        rotate x*J2A1
        translate <.9*Sqrt_2,-.9*Sqrt_2,0>
        texture {T2}
        }
      }          
    union {                             //mollet + cheville droits
     cylinder {0,-L_Mollet*y,R_Membres
        rotate x*J1A2
        translate <-.9*Sqrt_2-L_Cuisse*sin (Ang1*Pi/180),-.9*Sqrt_2-L_Cuisse*cos (Ang1*Pi/180)*cos (J1A1*Pi/180),-L_Cuisse*cos (Ang1*Pi/180)*sin (J1A1*Pi/180)>
        }
      sphere {<0,-L_Mollet,0>,0.4
        rotate x*J1A2
        translate <-.9*Sqrt_2-L_Cuisse*sin (Ang1*Pi/180),-.9*Sqrt_2-L_Cuisse*cos (Ang1*Pi/180)*cos (J1A1*Pi/180),-L_Cuisse*cos (Ang1*Pi/180)*sin (J1A1*Pi/180)>
        texture {T2}
        }
      }  
    union {                             //mollet + cheville gauches
     cylinder {0,-L_Mollet*y,R_Membres
        rotate x*J2A2
        translate <.9*Sqrt_2+L_Cuisse*sin (Ang1*Pi/180),-.9*Sqrt_2-L_Cuisse*cos (Ang1*Pi/180)*cos (J2A1*Pi/180),-L_Cuisse*cos (Ang1*Pi/180)*sin (J2A1*Pi/180)>
        }
      sphere {<0,-L_Mollet,0>,0.4
        rotate x*J2A2
        translate <.9*Sqrt_2+L_Cuisse*sin (Ang1*Pi/180),-.9*Sqrt_2-L_Cuisse*cos (Ang1*Pi/180)*cos (J2A1*Pi/180),-L_Cuisse*cos (Ang1*Pi/180)*sin (J2A1*Pi/180)>
        texture {T2}
        }
      }  
      cone {0,R_Pied/5,-H_Pied*y,R_Pied   //pied droit
        translate <-.9*Sqrt_2-L_Cuisse*sin (Ang1*Pi/180),
                   -.9*Sqrt_2-L_Cuisse*cos (Ang1*Pi/180)*cos (J1A1*Pi/180)-L_Mollet*cos (J1A2*Pi/180),
                   -L_Cuisse*cos (Ang1*Pi/180)*sin (J1A1*Pi/180)-L_Mollet*sin (J1A2*Pi/180)>
        }                   
      cone {0,R_Pied/5,-H_Pied*y,R_Pied   //pied gauche
        translate <.9*Sqrt_2+L_Cuisse*sin (Ang1*Pi/180),
                   -.9*Sqrt_2-L_Cuisse*cos (Ang1*Pi/180)*cos (J2A1*Pi/180)-L_Mollet*cos (J2A2*Pi/180),
                   -L_Cuisse*cos (Ang1*Pi/180)*sin (J2A1*Pi/180)-L_Mollet*sin (J2A2*Pi/180)>
        }                   
    }  
  texture {T1}
  translate y*Haut
  }                              
#render concat ("et Vous en sortez!!",chr (13),chr (10),chr (10))
#end 
