#include "colors.inc"
#include "textures.inc"

#declare T_Pneu = texture {
        pigment {color Black}
        finish {phong 1.0 phong_size 20}
}

#declare T_Jante = texture { 
        pigment {color NeonPink}
/*        finish {
                phong 1.0
                phong_size 10
                metallic     
                reflection .3
        } */
        finish {
                metallic
                ambient 0.2
                diffuse 0.7
                brilliance 6
                reflection 0.25
                phong 0.75
                phong_size 80
        }
#declare T_Jante = texture {Silver_Metal}

}

#declare Roue_1 = union {
        torus {2,.3             
                        rotate -x*90
                        texture {T_Pneu}
                }
  /*              #declare Temoin = 0;
                #while (Temoin < 360)
                        torus {.3,.02 
                                rotate x*30  
                                scale <1,1,1/cos (30*pi/180)>
                                translate x*2
                                rotate z*Temoin 
                                texture {T_Pneu}
                        }
                        torus {.3,.02 
                                rotate -x*30  
                                scale <1,1,1/cos (30*pi/180)>
                                translate x*2
                                rotate z*Temoin 
                                texture {T_Pneu}
                        }
                        #declare Temoin= Temoin+ 30;
                        #end         */
        difference {
                cylinder {<0,0,-.2>,<0,0,.2>,2}
                cylinder {<0,0,-.25>,<0,0,.25>,1.65}
                texture {T_Jante
                }
        }
        cylinder  {<0,0,-0.35>,<0,0,0.35>,.1 texture {T_Jante}}
        difference {
                cylinder  {<0,0,-0.25>,<0,0,0.25>,.3 }
                torus {1.5,1.22 rotate -x*90}        
                texture {T_Jante}
        }
        #declare Temoin = 0;
        #while (Temoin<360)           
                union {
                        cylinder {<0,.3,.20>,<0,1.65,.0>,.01}
                        cylinder {<0,.3,-.20>,<0,1.65,-.0>,.01 rotate z*5}
                        texture {T_Jante}
                        rotate z*Temoin
                        }
                #declare Temoin= Temoin + 10;
                #end  
        scale 1/2.3                
}

#declare Roue_2 = union {
        torus {2,.3             
                        rotate -x*90
                        texture {T_Pneu}
                }
        difference {
                cylinder {<0,0,-.2>,<0,0,.2>,2}
                cylinder {<0,0,-.25>,<0,0,.25>,1.65}
                texture {T_Jante
                }
        }
        cylinder  {<0,0,-0.35>,<0,0,0.35>,.1 texture {T_Jante}}
        difference {
                cylinder  {<0,0,-0.25>,<0,0,0.25>,.3 }
                torus {1.5,1.22 rotate -x*90}        
                texture {T_Jante}
        }
        union {
        #declare Temoin = 0;
        #while (Temoin<360)           
                        cylinder {<0,.1,.0>,<0,1.65,.0>,.1
                                scale <2,1,.05>
                                rotate y*30
                        texture {T_Jante}
                        rotate z*Temoin
                        }
                #declare Temoin= Temoin +72;
                #end    
                }
        scale 1/2.3                
}
         
         
object {Roue_1
        rotate 15
        translate 1.1*x  
        }
object {Roue_2
        rotate -15
        translate -1.1*x
        }

                       
sky_sphere {
        pigment {
                gradient y 
                color_map {
                        [0 color Cyan]
                        [1 color MidnightBlue]
                }
        }
}                        

                       
light_source {1000*(y-z-x/2) color White}
light_source {1000*(y-z) color .2*Red}
light_source {1000*(y-z+x/2) color .3*Yellow}

camera {location <0,1,-5>
        look_at 0
        }                
                
        
                