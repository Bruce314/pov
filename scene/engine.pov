#version 3.7;
global_settings { 
  assumed_gamma 1.0
}

// reference file for camshaft
#include "came.inc"
#include "spring.inc"
#include "gears.inc"

// used for interpolate
#include "math.inc"     


// some default includes
#include "golds.inc"
#include "metals.inc"

// debug
#declare DebugTexture=1;
#declare d_axis=1;

// used to help debugging
#include "b_debug.inc"      

// clock related
#declare EngineAngle=720*clock;
#declare CameAngle= EngineAngle / 2;

         
// constants         

// lever
#declare RadiusCrank=2;
#declare LeverPiston=2.5*RadiusCrank;      
#declare CrankThickness=0.4;


// piston
#declare RadiusPiston=1.5;
#declare HeightPiston=1;
#declare NumberSpi=3;
#declare SpiMargin=3;

#declare PistonSpacing=2*NumberSpi+SpiMargin;
//valves
#declare SpringSpires=3;
#declare SpringRadius=0.05;
#declare ValveRadius=RadiusPiston/4-2*0.05;

// cam
#declare local_h=1.2;

#declare AxisPiston= HeightPiston/14;
#declare InternalRadiusPiston=RadiusPiston*0.8;

// Arbre
#declare RadiusArbre = AxisPiston*5;

// Engine
#declare PistonNumber = 6;
#declare PistonSpace = RadiusPiston/4;
#declare Margin=0.05;
#declare CylinderHeadOrigin=RadiusCrank+LeverPiston+HeightPiston*(1-1/PistonSpacing);

// lighting order
#declare Index=0;
#declare LightOrder=array[PistonNumber]
#while (Index<PistonNumber/2)
  #debug concat(str(Index,0,0),"-",str(PistonNumber,0,0))
  #if (mod(Index,2)=0)
    #declare LightOrder[Index]=Index;
    #declare LightOrder[Index+PistonNumber/2]=PistonNumber-1-Index;
  #else
    #declare LightOrder[Index]=PistonNumber-1-Index;
    #declare LightOrder[Index+PistonNumber/2]=Index;
  #end
  #declare Index=Index+1;
#end

// computed

#declare angleLever = function(MyAngle) {
  -asin(RadiusCrank/LeverPiston*sin(MyAngle*pi/180))*180/pi
}

#declare positionLever = function (MyAngle) {
  RadiusCrank*cos(MyAngle*pi/180)+sqrt(LeverPiston*LeverPiston-RadiusCrank*RadiusCrank*sin(MyAngle*pi/180)*sin(MyAngle*pi/180))
}

#macro Piston()
merge {
  // piston                               
  difference {
    cylinder {0,HeightPiston*y,RadiusPiston}
    #local Index=0;                      

    #while (Index<NumberSpi)
      torus {RadiusPiston,HeightPiston/PistonSpacing/2/2
        scale <1,2,1>
        translate y*(SpiMargin*2+1+4*Index)/PistonSpacing/2*HeightPiston
      } 
      #local Index=Index+1;
      
    #end
    box {<-RadiusPiston*1.5,-1,-CrankThickness/2>,<RadiusPiston,HeightPiston/PistonSpacing*3,CrankThickness/2>}
  }
  cylinder {-z*InternalRadiusPiston,z*InternalRadiusPiston,AxisPiston
    translate HeightPiston/PistonSpacing*y 
  }               
  translate < 0,-HeightPiston/PistonSpacing,0>
  #ifdef (DebugTexture)
    texture {
      pigment {
        color rgb <1,0,1>
      }
    }
  #else
    texture {
      pigment {
        color rgbf <1,1,1,0.9>
      }
    }
    interior {
      ior 1.4
    }
  #end                
  
}
#end

#macro Gaz(MyAngle) 
  union {
    #local Index=0;
    #while (Index<PistonNumber)
      #local ThisPistonAngle=clamp(EngineAngle + 720*LightOrder[Index]/PistonNumber,0,720);
      #switch(int(ThisPistonAngle/180))
        #case (0) // admission
          #declare T1=texture {
            pigment {color rgb <0,0,0.4>}
          }
        #break
        #case (1) // compression
          #declare T1=texture {
            pigment {color rgb <Interpolate(ThisPistonAngle,180,360,0,1,1),
              0,
              Interpolate(ThisPistonAngle,180,360,0.4,0,1)>}
          }
        #break
        #case (2) // power
          #declare T1=texture {
            pigment {color rgb <Interpolate(ThisPistonAngle,360,540,1,0.2,1),
              Interpolate(ThisPistonAngle,360,540,0,0.2,1),
              Interpolate(ThisPistonAngle,360,540,0,0.2,1)>}
          }
        #break
        #case (3) // exhaust
          #declare T1=texture {
            pigment {color rgb <0.2,0.2,0.2>}
          }
        #break
      #end
      merge {
        #local HeightGaz = (positionLever(00)-positionLever(ThisPistonAngle));
        #if (HeightGaz>0.001)
        cylinder {
          0,
          -y*HeightGaz,
          RadiusPiston
        }
        #end
        sphere {
          0,
          RadiusPiston
          scale <1,0.1,1>
        }
        texture {T1}
        translate y*CylinderHeadOrigin
        translate z*(2*RadiusPiston + PistonSpace)*Index
      }
      #local Index=Index+1;
    #end
  }
#end


#macro LeverSystem(MyAngle)
  difference {
    union {
      cylinder {-CrankThickness/2*z,CrankThickness/2*z,AxisPiston*2}
      cylinder {-CrankThickness/2*z,CrankThickness/2*z,AxisPiston*2
        translate <LeverPiston,0,0>
      }
      cylinder {0,<LeverPiston,0,0>,AxisPiston*2}
    }
    union {
      // axis holes
      cylinder {-CrankThickness*z,CrankThickness*z,AxisPiston}
      cylinder {-CrankThickness*z,CrankThickness*z,AxisPiston
        translate <LeverPiston,0,0>
      }
    }
    #ifdef (DebugTexture)
      texture {
        pigment {
          color rgb <0,1,1>
        }
      }
    #else
      texture {T_Brass_2A}
    #end
    rotate <0,0,angleLever(MyAngle)>
    translate <RadiusCrank*cos(MyAngle*pi/180),RadiusCrank*sin(MyAngle*pi/180),0>
    rotate z*90
  }
#end           

#macro Arbre(AnglePosition) 
  merge {
    #local Index=0;
    #while (Index<PistonNumber)
      #local ThisPistonAngle=AnglePosition + 720*LightOrder[Index]/PistonNumber;
      merge {
        difference {
          union {
            intersection { 
              cylinder {-z*1.5*CrankThickness,z*1.5*CrankThickness,RadiusCrank+ AxisPiston}
              box {<-RadiusArbre,-RadiusCrank/2,-1.6*CrankThickness>,
                <RadiusArbre,(RadiusCrank+ AxisPiston),1.6*CrankThickness>
              }
            }
            intersection {
              cylinder {-z*1.5*CrankThickness,z*1.5*CrankThickness,RadiusCrank+ AxisPiston}
              plane {y,0
                rotate z*-60
              }
              plane {y,0
                rotate z*60
              }
            }
          }
          cylinder {-z*0.5*CrankThickness,z*0.5*CrankThickness,RadiusCrank+ 2*AxisPiston}
        }
        // small axis
        cylinder {-z*1.55*CrankThickness,z*1.55*CrankThickness,AxisPiston
          translate y*RadiusCrank
        }
        // big axis
        cylinder {z*1.4*CrankThickness,
          z*(2*RadiusPiston + PistonSpace - 1.4*CrankThickness), 
          RadiusArbre
        }
        rotate z*ThisPistonAngle
        translate z*Index*(2*RadiusPiston + PistonSpace)
      }
    #local Index=Index+1;
    #end

    cylinder {-z*1.4*CrankThickness,
      -4*z, 
      RadiusArbre
      rotate z*AnglePosition
    }
    
    #ifdef (DebugTexture)
      texture {
        pigment {
          radial color_map{[0.5 color rgb <1,1,0>][0.5 color rgb <1,1,1>]}
          frequency 4
          rotate x*90
        }
      }
    #else
      texture {T_Brass_2A}
      interior{
        ior 1.3
      }
    #end
  }
#end

#macro Bloc()
  difference
  {
    box {
      <-(RadiusCrank+2*AxisPiston),
      -RadiusCrank-2*AxisPiston,
      -RadiusPiston-PistonSpace>
      <(RadiusCrank+2*AxisPiston),
      RadiusCrank+LeverPiston+HeightPiston*(1-1/PistonSpacing),
      (PistonNumber-1)*(2*RadiusPiston + PistonSpace)+RadiusPiston + PistonSpace>
    }
    #local Index=0;
    #while (Index<PistonNumber)
      cylinder {
        0,<0,RadiusCrank+LeverPiston+HeightPiston*(1-1/PistonSpacing)+1,0>,RadiusPiston+Margin
        translate z*(2*RadiusPiston + PistonSpace)*Index
      }
    #local Index=Index+1;
    #end
    cylinder {-(Margin+1.5*CrankThickness)*z,((PistonNumber-1)*(2*RadiusPiston + PistonSpace)+1.5*CrankThickness+Margin)*z,RadiusCrank+AxisPiston+Margin}
    texture {
      pigment{
        color rgbf <1,1,1,.9>
      }
    }
  }
#end

#macro Valve()
  merge {
    sphere {0,0.1 translate -0.1*y}
    cylinder {-0.1*y,-2*y,0.1}
    cylinder {-0.45*y,-0.5*y,ValveRadius}
    
    sphere {0,RadiusPiston/4
      scale <1,0.1,1>
      translate -2*y
    }
  }
#end
// came is twice as fast as engine (4 strokes, 2 full rotation per cycle)
// depending on technology to make cams turning, might need to use either one rotation or the other
// rotation is {-1,1}, with 1=same as engine = k.z with k>0
#macro Camshaft(MyAngle,rotation)
  union
  {
    object {Gear (0.2,6)
      scale <1,1,2>
        texture {
          pigment {
            radial color_map{[0.5 color rgb <0.5,0.9,0.4>][0.5 color rgb <1,1,1>]}
            frequency 3
            rotate x*90
          }        
        }
      
      rotate z*(MyAngle+360/20/2)/2*rotation
      translate <0,0,-(RadiusCrank+2*AxisPiston+0.2+Margin)>
    }
    #local Index=0;
    #while (Index<PistonNumber)

      union {
        // admission, must be full open at 90 engine = 45 cam
        //works for rotation = -1
        //#local CameAngle=(90+45+MyAngle/2 + 360*LightOrder[Index]/PistonNumber)*rotation;
        #local CameAngle=(135+MyAngle/2 + 360*LightOrder[Index]/PistonNumber)*rotation;
        object {came(1,local_h)
          rotate z*CameAngle
        }
        object{Valve()
          #ifdef (DebugTexture)
            texture {pigment {color rgb <0.1,0.1,0.9>}}
          #else
            texture {T_Silver_4A}
          #end
          translate -y*radiusCame(1,local_h,CameAngle*pi/180+3*pi/2)
        }
        translate z*(2*RadiusPiston + PistonSpace)*Index
        translate -z*(RadiusPiston/2)
      }
      //exhaust
      union {
        // works for rotation = -1
        // #local CameAngle=(90-215+EngineAngle/2 + 360*LightOrder[Index]/PistonNumber)*rotation;
        #local CameAngle=(-135+EngineAngle/2 + 360*LightOrder[Index]/PistonNumber)*rotation;
        object {came(1,local_h)
          // exhaust, must be full open at 630 engine = 315 cam
          rotate z*CameAngle
        }
        object{Valve()
          #ifdef (DebugTexture)
            texture {pigment {color rgb <0.9,0.1,0.1>}}
          #else
            texture {T_Silver_4A}
          #end
          translate -y*radiusCame(1,local_h,CameAngle*pi/180+3*pi/2)
        }
        translate z*(2*RadiusPiston + PistonSpace)*Index
        translate z*(RadiusPiston/2)
      }
      #local Index=Index+1;
      cylinder {<0,0,-3>,
        <0,0,(PistonNumber-1)*(2*RadiusPiston + PistonSpace)+RadiusPiston + PistonSpace>,
        RadiusArbre
        #ifdef (DebugTexture)
          texture {
            pigment {
              radial color_map{[0.5 color rgb <0.5,0.9,0.4>][0.5 color rgb <1,1,1>]}
              frequency 3
              rotate x*90
            }        
          }
        #else
          texture {T_Silver_4A}
        #end
        rotate z*(MyAngle+360/20/2)/2*rotation
      }
    #end
    #ifdef (DebugTexture)
      texture {
        pigment {
          radial color_map{[0.5 color rgb <0.5,0.9,0.4>][0.5 color rgb <1,1,1>]}
          frequency 3
          rotate x*90
        }        
      }
    #else
      texture {T_Silver_4A}
    #end
    translate (3+CylinderHeadOrigin)*y
  }
#end

#macro DualCamshaft(MyAngle,rotation)
  #local CamScale = RadiusPiston/4*sqrt(2)/LongRadius(1,local_h)*0.95;
  union {
    union
    //admission
    {
      // entry gear
      object {Gear (0.1,12)
        scale <1,1,2>
        texture {
          pigment {
            radial color_map{[0.5 color rgb <0.5,0.9,0.4>][0.5 color rgb <1,1,1>]}
            frequency 3
            rotate x*90
          }        
        }
        
        rotate z*(MyAngle+360/20/2)/2*rotation
        translate <0,0,-(RadiusCrank+2*AxisPiston+0.2+Margin)>
      }

      #local Index=0;
      #while (Index<PistonNumber)

        union {
          #local CameAngle=(135+MyAngle/2 + 360*LightOrder[Index]/PistonNumber)*rotation;
          union {
            object {came(1,local_h)
              scale <CamScale,CamScale,2>
              rotate z*CameAngle
            }
            object{Valve()
              #ifdef (DebugTexture)
                texture {pigment {color rgb <0.1,0.1,0.9>}}
              #else
                texture {T_Silver_4A}
              #end
              translate -y*radiusCame(1,local_h,CameAngle*pi/180+3*pi/2)*CamScale
            }
            object {Spring(ValveRadius,SpringSpires,SpringRadius,
              (1-(radiusCame(1,local_h,CameAngle*pi/180+3*pi/2)-1)*CamScale))
              translate -y*(1.5+LongRadius(1,local_h)*CamScale)
            }
              
            translate -z*(RadiusPiston/2)*sqrt(2)/2
          }
          union {
            object {came(1,local_h)
              scale <CamScale,CamScale,2>
              rotate z*CameAngle
            }
            object{Valve()
              #ifdef (DebugTexture)
                texture {pigment {color rgb <0.1,0.1,0.9>}}
              #else
                texture {T_Silver_4A}
              #end
              translate -y*radiusCame(1,local_h,CameAngle*pi/180+3*pi/2)*CamScale
            }
            object {Spring(ValveRadius,SpringSpires,SpringRadius,
              (1-(radiusCame(1,local_h,CameAngle*pi/180+3*pi/2)-1)*CamScale))
              translate -y*(1.5+LongRadius(1,local_h)*CamScale)
            }
            translate z*(RadiusPiston/2)*sqrt(2)/2
          }
          translate z*(2*RadiusPiston + PistonSpace)*Index
        }
        #local Index=Index+1;
      #end
      cylinder {<0,0,-3>,
        <0,0,(PistonNumber-1)*(2*RadiusPiston + PistonSpace)+RadiusPiston + PistonSpace>,
        RadiusArbre*CamScale
        #ifdef (DebugTexture)
          texture {
            pigment {
              radial color_map{[0.5 color rgb <0.5,0.9,0.4>][0.5 color rgb <1,1,1>]}
              frequency 3
              rotate x*90
            }        
          }
        #else
          texture {T_Silver_4A}
        #end
        rotate z*(MyAngle+360/20/2)/2*rotation
      }
      translate -x*sqrt(2)/2*RadiusPiston/2
    }
    union
    //exhaust
    {
      object {Gear (0.1,12)
        scale <1,1,2>
        texture {
          pigment {
            radial color_map{[0.5 color rgb <0.5,0.9,0.4>][0.5 color rgb <1,1,1>]}
            frequency 3
            rotate x*90
          }        
        }
        
        rotate z*(MyAngle+360/20/2)/2*rotation
        translate <0,0,-(RadiusCrank+2*AxisPiston+0.2+Margin)>
      }
      #local Index=0;
      #while (Index<PistonNumber)

        union {
          #local CameAngle=(-135+EngineAngle/2 + 360*LightOrder[Index]/PistonNumber)*rotation;
          union {
            object {came(1,local_h)
              scale <CamScale,CamScale,2>
              rotate z*CameAngle
            }
            object{Valve()
              #ifdef (DebugTexture)
                texture {pigment {color rgb <0.9,0.1,0.1>}}
              #else
                texture {T_Silver_4A}
              #end
              translate -y*radiusCame(1,local_h,CameAngle*pi/180+3*pi/2)*CamScale
            }
            object {Spring(ValveRadius,SpringSpires,SpringRadius,
                   1-(radiusCame(1,local_h,CameAngle*pi/180+3*pi/2)-1)*CamScale)
              translate -y*(1.5+LongRadius(1,local_h)*CamScale)
            }           
            translate z*(2*RadiusPiston + PistonSpace)*Index
            translate -z*(RadiusPiston/2)*sqrt(2)/2
          }
          union {
            object {came(1,local_h)
              scale <CamScale,CamScale,2>
              rotate z*CameAngle
            }
            object{Valve()
              #ifdef (DebugTexture)
                texture {pigment {color rgb <0.9,0.1,0.1>}}
              #else
                texture {T_Silver_4A}
              #end
              translate -y*radiusCame(1,local_h,CameAngle*pi/180+3*pi/2)*CamScale
            }
            object {Spring(ValveRadius,SpringSpires,SpringRadius,
                   1-(radiusCame(1,local_h,CameAngle*pi/180+3*pi/2)-1)*CamScale)
              translate -y*(1.5+LongRadius(1,local_h)*CamScale)
            }           
            translate z*(2*RadiusPiston + PistonSpace)*Index
            translate z*(RadiusPiston/2)*sqrt(2)/2
          }
        }
        #local Index=Index+1;
      #end
      cylinder {<0,0,-3>,
        <0,0,(PistonNumber-1)*(2*RadiusPiston + PistonSpace)+RadiusPiston + PistonSpace>,
        RadiusArbre*CamScale
        #ifdef (DebugTexture)
          texture {
            pigment {
              radial color_map{[0.5 color rgb <0.5,0.9,0.4>][0.5 color rgb <1,1,1>]}
              frequency 3
              rotate x*90
            }        
          }
        #else
          texture {T_Silver_4A}
        #end
        rotate z*(MyAngle+360/20/2)/2*rotation
      }
      translate x*sqrt(2)/2*RadiusPiston/2
    }
//    box {<-1,-(1.5+LongRadius(1,local_h)*CamScale),-3>
//      <1,-(2+LongRadius(1,local_h)*CamScale),3>
//    }


    #ifdef (DebugTexture)
      texture {
        pigment {
          radial color_map{[0.5 color rgb <0.5,0.9,0.4>][0.5 color rgb <1,1,1>]}
          frequency 3
          rotate x*90
        }        
      }
    #else
      texture {T_Silver_4A}
    #end
    
    translate (3+CylinderHeadOrigin)*y
  }
#end //DualCamShaft

/*
  union {
    came (1,local_h)
    cylinder {-0.2*z,0.2*z,0.1}
    texture {pigment {color rgb <1,0,0>}}
    rotate z*angleCame
  }
**/      
#macro FullEngine()

union {
  #local Index=0;
  #while (Index<PistonNumber)
    #local ThisPistonAngle=EngineAngle + 720*LightOrder[Index]/PistonNumber;
    union {
      object {
        Piston()         
        translate y*positionLever(ThisPistonAngle)
      }                          

      object{
        LeverSystem(ThisPistonAngle)
      }
      translate z*(2*RadiusPiston + PistonSpace)*Index
    }
    #local Index=Index+1;
  #end
  object {Arbre(EngineAngle)}
//  object {Bloc()}
  object {Gaz(EngineAngle)}
 // object {Camshaft(EngineAngle,-1)}
  object {DualCamshaft(EngineAngle,-1)
  }
  union {
    object {
      Gear (0.1,20)
      scale <1,1,2>
      #ifdef (DebugTexture)
        texture {
          pigment {
            radial color_map{[0.5 color rgb <0.5,0.9,0.4>][0.5 color rgb <1,1,1>]}
            frequency 3
            rotate x*90
          }        
        }
      #else
        texture {T_Brass_3A}
      #end

      
      rotate z*EngineAngle
      translate -(RadiusCrank+2*AxisPiston+0.2+Margin)*z
    }
    
    object {
      Gear (0.1,40)
      scale <1,1,2>
      #ifdef (DebugTexture)
        texture {
          pigment {
            radial color_map{[0.5 color rgb <0.9,0.2,0.4>][0.5 color rgb <1,1,1>]}
            frequency 3
            rotate x*90
          }        
        }
      #else
        texture {T_Silver_4A}
      #end
    
      rotate -z*(EngineAngle+360/20/2)/2
      translate <0,pitchDiameter(0.1,40)+pitchDiameter(0.1,20),-(RadiusCrank+2*AxisPiston+0.2+Margin)>
    }
    
    
  }
  
  translate -z*(PistonNumber-1)*(2*RadiusPiston + PistonSpace)/2
  translate -y*4
  //rotate y*(90)//+360*clock)
//  rotate 2*y*clock*360
  
}               
#end

union {
  object {FullEngine()
    scale 0.8
    rotate y*(90+0)
   // translate -x*6
    
  }
/*
  object {FullEngine()
    scale 0.8
    rotate y*180
    rotate -x*90
 
  }
  */
  /*
  object {FullEngine()
    rotate y*45
    translate -x*7
  }
  */
}

camera {
  location <0,0,-18>
  look_at 0
  right x*image_width/image_height
  up y
}                   

light_source {
    100000*(-z) color rgb <1,1,1>
}

light_source {
    100000*(y) color rgb <0,0,1>
}

light_source {
    100000*(x) color rgb <1,0,0>
}
