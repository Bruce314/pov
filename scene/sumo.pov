#global_settings { hf_gray_16 on }

//les includes
#include "colors.inc"
#include "textures.inc"
#include "robot03.inc"

//les dimensions
#declare Largeur = 1;
#declare Longueur = 1.8;
#declare R_Cyl = .3;
#declare R_Art = .5;                                             
                                             
// les textures.                           
#declare T_Corps = texture {pigment {color Red}}

#declare T_Artic = texture {pigment {color Blue}}                           

#declare T_1 = texture {
        pigment {color White}
        finish {phong 1.0 phong_size 100}
}
#declare T_2 = texture {pigment {color Black}
        finish {phong 1.0 phong_size 15}
}
                           
//les primitives                           
#declare Filament = union {
        cylinder {-Largeur*x,Largeur*x,R_Cyl texture {T_Corps}}
        sphere {-Largeur*x,R_Art texture {T_Artic}}
        sphere {Largeur*x,R_Art texture {T_Artic}}
        cylinder {0,y*Longueur,R_Cyl texture {T_Corps}}
        cylinder {-Largeur*x+y*Longueur,Largeur*x+y*Longueur,R_Cyl texture {T_Corps}}
        sphere {-Largeur*x+y*Longueur,R_Art texture {T_Artic}}
        sphere {Largeur*x+y*Longueur,R_Art texture {T_Artic}}
}

#macro f(X,Y)
        (Y-X)/abs (Y-X)*pow(abs(Y-X),3)/100
#end

#declare Inc = .5;

#declare Sol = union {
        #declare XXX = -2.5;
        #while (XXX<=2.5)            
                #declare YYY=-1.5;
                #while (YYY<=3)          
                        sphere {<XXX,f(XXX,YYY),YYY>,.05 texture {T_2}}
                        #if (XXX!=-2.5) 
                        cylinder {<XXX,f(XXX,YYY),YYY>,<XXX-Inc,f(XXX-Inc,YYY),YYY>,.02 texture {T_1}}
                        #end
                        #if (YYY!=-1.5)
                        cylinder {<XXX,f(XXX,YYY),YYY>,<XXX,f(XXX,YYY-Inc),YYY-Inc>,.02 texture {T_1}}
                        #end
                        #declare YYY=YYY+Inc;
                #end
                #declare XXX=XXX+Inc;
        #end
}
                

#declare Pas_de_Cheveux=2;

#declare Robot = Droide1 (-20,-50,60,20,
        45,85,45,-45,
        75,135,105,135,
        10,-5,30,00,
        20,20) 
        /*
#declare T_Slip = texture{T_1};
#declare Robot = Droide2 (-20,-50,60,20,
        45,85,45,-45,
        75,135,105,135,
        10,-5,30,00,
        20,20,0)*/
#undef Pas_de_Cheveux

// la scene
//object {Filament}
object {Robot 
        scale 1/4 
        translate -y
        rotate -y*20
}

object {Sol 
        rotate -x*15
        translate -y*1.15
        translate z*.5
}

light_source {1000*(y-z) color 1.3*White}
light_source {1000*(-x/2) color .3*White}
light_source {1000*(x/2) color .3*White}

camera {
        location <0,-1,-5>
        look_at -.5*y
}
 
background {White}       