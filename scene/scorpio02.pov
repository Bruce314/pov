#include "colors.inc"
#include "textures.inc"
                   
                   
// les variables d'animation
#declare Epsilon1=.15;  
#declare Max_F=.6;

#switch (clock) 
#range (0,Epsilon1)
        #declare Position=<0,0,-1>;
        #declare Reg=-3*z; 
        #declare Dir=1;                  
        
        #declare Fil=Max_F*(clock/Epsilon1);
        #declare Trans1=1-clock/Epsilon1;
        #break

#range (1-Epsilon1,1)
        #declare Position=<0,0,-2>;
        #declare Reg=0;
        #declare Dir=2;                  
       
        #declare Trans1=0;
        #declare Fil=Max_F*(1-(clock-1+Epsilon1)/Epsilon1);
        #break
#range (Epsilon1,1-Epsilon1)
        #declare Tps=(clock-Epsilon1)/(1-2*Epsilon1);

        #declare Position=<-1.5*(1-cos (2*pi*Tps)),0,-1-Tps>;
        #declare Reg=-3*(1-Tps)*z;                      
        #declare Dir=1+Tps;

        #declare Trans1=0;
        #declare Fil=Max_F;  
        #break
#range (1,2)  //deuxi�me partie, d�placement
        #declare Tps=clock-1;
        
        #declare Position=<0,1-cos(2*pi*Tps),-2+Tps*Tps-sin(2*pi*Tps)>;        
        #declare Reg=-3*Tps*z;
        #declare Dir=2-Tps;
        #break

#end

#switch (clock)
#range (1,2-Epsilon1)
        #declare Trans1=0;
        #declare Fil=Max_F;        
        #break
#range (2-Epsilon1,2)
        #declare Trans1=(clock-2+Epsilon1)/Epsilon1;
        #declare Fil=Max_F*(1-(clock-2+Epsilon1)/Epsilon1);
        #break
                
#end
                               
                               
#declare Lim1=.4;
#switch (clock)
#range (0,Lim1)                               
        #declare Coef_Light=2.5;
        #break                 
#range (Lim1,1)
        #declare Tps=(clock-Lim1)/(1-Lim1);
        #declare Coef_Light=2.5+4*(1-cos (pi*Tps));
        #break
#range (1,1+Lim1)           
        #declare Coef_Light=2.5+4*2;
        #break
#range (1+Lim1,2)          
        #declare Tps=(clock-1-Lim1)/(1-Lim1);
        #declare Coef_Light=2.5+4*(1+cos(pi*Tps));
        #break
        
#end

//
// Add media. 
//

media {
  intervals 10
  scattering { 1, rgb 0.03}
  samples 1, 10         
  confidence 0.9999
  variance 1/1000
  ratio 0.9
}



// le symbole
union {    
	plane {z,0
		hollow
		texture {
    		        pigment {
        		        image_map {
                    		gif "scorpio.gif"
                    		filter all Fil      //pour le final .2
                    		transmit all Trans1
                        	transmit 0,1
                        	filter 2,0	                        
                        	once
    	                        interpolate 2
        	                } 
		        translate <-.5,-.5,0>
	                scale 1
    	                }
                finish {
                        ambient 1.0
                        }
	        }
	}                     
	sphere {0,100 texture {pigment {color rgbt 1}} hollow}
    }
        

// l'univers r�duit       
box {<-10,-5,-3>,<10,5,8>
        texture {
                pigment {color White}
                normal {bumps .3
                        scale .05
                        }                        
                }
        hollow
        }
    

//le spot
light_source {<0,0,2> color Coef_Light*White
        spotlight
        point_at 0             
        radius 9
        falloff 13
        tightness 50
        media_attenuation on
        }


   /*
//des cam�ras de debuggage
#declare Cam=1;
#switch (Cam)
case (0) 
camera { //face au logo camera finale
	location <0,0,-2>
    direction 2*z //2
    look_at 0     
    //rotate y*90
    }     
    #break
case (1) 
camera { //camera initiale
	location <0,0,-1>
    direction z //2
    look_at -3*z     
    //rotate y*90
    }     
    #break
case (2)  //camera au milieu, phase de l'animation
camera {
        location <-3,0,-1>
        look_at <0,0,-1>
        }    
        #break
#end
     */                                                              
                                                   
// la camera anim�e                                                   
camera { //camera initiale
        location Position
    direction Dir*z //2
    look_at Reg     
    //rotate y*90
    }     
                                                   