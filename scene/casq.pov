#include "colors.inc"
#include "textures.inc"     

#declare E_Tissus = 0.02;
#declare L_Vis = 1;
#declare Epsilon = 0.00000001;
#declare Filet = 0.015;

#declare T_Casq1 = texture {
	pigment {color Red}
	}
#declare T_Casq2 = texture {pigment {color Yellow}}
#declare T_Casq3 = texture {pigment {color Blue}}


#declare Casq1 = quadric {<-0.02,0,.2>,<0,0,0>,<0,1,0>,0} //la base de la visiere

//le dessus de la casquette
#declare Casq4 = union {
	sphere {0,1}
	cylinder {0,-y,1 - Epsilon
		}
	sphere {0,1
		scale <.1,.03,.1>
		translate y
		texture {T_Casq3}
		}
	texture {radial 
		texture_map {
			[0 T_Casq3]
			[Filet T_Casq1]
			[.5-Filet T_Casq1]
			[.5 T_Casq3]
			[.5+Filet T_Casq2]
			[1-Filet T_Casq2]
			[1 T_Casq3]
			}
		frequency 5
		}
	}

//la casquette visible
#declare Casq12 = union {
	object {Casq1}
	object {Casq4}
	}

#declare Casq11 = difference {
	object {Casq12}
	object {Casq1 translate -E_Tissus * y}
	}

#declare Casq2 = merge {
	cylinder {-10*y,10*y,1}
	cylinder {-10*y,10*y,1 translate <L_Vis,0,0>}
	box {<0,-10,-1>,<L_Vis,10,1>}
	}


#declare Casq3 = intersection {
	object {Casq11}
	object {Casq2}
	}

#declare Casq5 = difference {
	object {Casq3}
	merge {
		sphere {0,1-E_Tissus}
		cylinder {0,-y,1-E_Tissus}
		}
	}
	
#declare Casquette = object {Casq5 texture {T_Casq3}}

object {Casquette
	scale 4
	//rotate  -x * 90
	//rotate -y*90
	//rotate -20
	}

camera {location <0,5.4,-27>
        direction 2*z
        look_at 3*y
        }

sky_sphere {pigment {color Black}}
        
light_source {100*(y-z-x) color 1.2*White
	#ifdef (Lumiere_Large)
          area_light 5*(-x+z)/.707,5*y,Lumiere_Large,Lumiere_Large //10,10,8,8 
          jitter adaptive 2                       //10,10,5,5 peut sembler suffisant
	#end
        spotlight 
        point_at 2*y
        radius 2
        falloff 3
        }                
              
light_source {100*(y-z+x) color 1.2*White
	#ifdef (Lumiere_Large)
          area_light 5*(x+z)/.707,5*y,Lumiere_Large,Lumiere_Large //8,8
          jitter adaptive 2
	#end
        spotlight 
        point_at 2*y
        radius 2
        falloff 3
        }        
