#include "colors.inc"
#include "textures.inc"
#include "matrices.inc"
/* usage : matrix Transf (u1,u2,u3)
o� u1 est le vect img de x
   u2 est le vect img de y
   u3 est le vect img de z
*/
#include "chaine.inc"


#declare LF=concat (chr (13),chr (10))

// Textures.

#declare T_Etoile=texture {
        pigment {bozo color_map {
                [0 color rgb <.5,0,0>]
                [.1 color <.5,0,0>]
                [.4 color Red]
                [1 color Red]}
                scale 1/30
                }
        finish {phong .6 phong_size 45}
        //normal {bumps .7 scale 1/20}                
        }        

#declare T_Ancre1=texture {
        pigment {bozo color_map {
                [0 color Black]
                [.5 color rgb <.2,.2,.2>]
                [1 color rgb <.5,.5,.5>]}
                scale .2
                }
        finish {phong .2
                phong_size 50
                reflection .1
                }
        normal {gradient y }
        }
        
#declare T_Ancre2=texture {
        pigment {marble color_map {
                [0 color DarkBrown]
                [.2 color IndianRed]
                [.3 color Orange ]
                [.4 color rgbt 1]
                [1 color rgbt 1]}
                turbulence 3
                }
        //normal {crackle .5 scale .1}
        //normal {gradient x}
        }                
        
        
#declare T_Sable=texture {
        pigment {wrinkles        
                color_map {
                        [0 color Yellow]                           
                        [.5 color Quartz]
                        [1 color Bronze]
                        }
                scale .01                        
                }
        }                                        
                
#declare T_Bois=texture {pigment {DMFWood4}}

#declare T_Boite=texture {Brass_Metal}
        


// Objets

//Etoile de mer

#render concat ("Creation de la boite de conserve",LF)

#declare Boite=union {
        difference  {
                cylinder {<0,-.05,0>,<0,.05,0>,5}
                union {
                        #declare Tem1=360/20;
                        #while (Tem1<360)                 
                                cylinder {<5.5,-.1,0>,<5.5,.1,0>,1.1
                                        rotate y*Tem1
                                        }
                                #declare Tem1=Tem1+360/20;
                                #end
                        }
                texture {T_Boite
                        normal {wood rotate x*90}
                        }
                translate <-5,0,0>
                rotate -z*45
                translate <5,5.75,0>
                }
        difference {
                cylinder {<0,-5.75,0>,<0,5.75,0>,5.1
                texture {
                      /*  pigment {
                                image_map {
                                        tga "pois1.tga"
                                        once map_type 2
                                        }
                                translate -.5*y
                                scale <5,10,5>
                                }*/
                        pigment {color Yellow}
                        }                                
                }
                cylinder {<0,-6,0>,<0,6,0>,5}
                texture {T_Boite
                        normal {onion}
                        }
                }
        cylinder {<0,-.05,0>,<0,.05,0>,5
                translate -y*5.75
                texture {T_Boite
                        normal {wood rotate x*90}
                        }
                }
        torus {5,.2
                translate 5.75*y
                }
        torus {5.05,.2
                translate -5.75*y
                }
        texture {T_Boite}
        }                
                


#render concat ("Creation de l'etoile",LF)

#declare Pas=.2;
#declare Lim=4;
#macro F1 (T,A1)
#local A=A1/72;
<T,
#if (mod (A,2)=0)
pow (T,2)/pow(Lim,2)
#else
        #if (A=3)
        -pow (T,2)/pow(Lim,2)
        #else
        0
        #end
#end,0>
#end        

#declare Etoile=blob { threshold .7
        sphere {0,1,1 }
        #declare Temoin1=Pas;
        #declare Rap=9/5;
        #while (Temoin1<Lim)
                #declare Temoin1=Temoin1+Pas;
                #declare Angle=0;
                #while (Angle<360)
                        #declare Angle=Angle+72;
                        sphere {0,1,1
                                scale (Rap*Lim-Temoin1)/(Rap*Lim)
                                translate F1(Temoin1,Angle)
                                rotate y*Angle
                                }
                        #end
                #end
        sphere {-2*y,1,-1
                scale 1.7
                }
        sphere {2*y,1,-1
                scale 1.7
                }
        texture {T_Etoile}
        }                                        

//Ancre

#render concat ("Creation de l'ancre",LF)

#declare Ancre=union {
        intersection {
                torus {4,.3
                        rotate -x*90
                        }
                plane {y,0
                        rotate z*40
                        }
                plane {y,0
                        rotate -z*40
                        }
                }
        cone {<4,0,0>,.5,<4,2,0>,0
                rotate -z*40
                }                                        
        cone {<-4,0,0>,.5,<-4,2,0>,0
                rotate z*40
                }
        cone {<0,-4,0>,.3,<0,2,0>,.5}
        cone {<0,-4,0>,.3,<0,-4.8,0>,0}
        cylinder {<0,2,-2>,<0,2,2>,.5}
        sphere {<0,2,-2>,.6}
        sphere {<0,2,2>,.6}
        torus {.6,.1
                rotate -z*90
                translate <0,2.6,0>
                }
       translate <0,-3.1,0>
        }                           

// Bateau

#render concat ("Creation du bateau",LF)

#macro F1(Var)
pow ((Var-1)*(Var+1),2)/3
#end
                
#macro F2(Var)
pow(-(Var-1)*(Var+1),1/3)/3
#end

#declare Pas=.015; //.02

#declare Bateau=union {
        #declare Temoin=-1+Pas;
        #while (Temoin<=1)
//        cylinder {<Temoin,-F2(Temoin),0>,<Temoin,0,F1(Temoin)>,Pas}
        union {
                cylinder {<0,-F2(Temoin),0>,<0,-F2(Temoin)*2/3,F1(Temoin)/2>,Pas}
                cylinder {<0,-F2(Temoin)*2/3,F1(Temoin)/2>,<0,0,F1(Temoin)>,Pas}
                texture {T_Bois rotate -x*90}
                translate Temoin*x
                }

//        cylinder {<Temoin,-F2(Temoin),0>,<Temoin,0,-F1(Temoin)>,Pas}
        union {
                cylinder {<0,-F2(Temoin),0>,<0,-F2(Temoin)*2/3,-F1(Temoin)/2>,Pas}
                cylinder {<0,-F2(Temoin)*2/3,-F1(Temoin)/2>,<0,0,-F1(Temoin)>,Pas}
                texture {T_Bois rotate -x*90}
                translate Temoin*x
                }

        cylinder {<Temoin,0,-F1(Temoin)>,<Temoin-Pas,0,-F1(Temoin-Pas)>,Pas texture {T_Bois rotate y*90}} //Haut
        cylinder {<Temoin,0,F1(Temoin)>,<Temoin-Pas,0,F1(Temoin-Pas)>,Pas texture {T_Bois rotate y*90}}

        cylinder {<Temoin,-F2(Temoin)*2/3,-F1(Temoin)/2>,<Temoin-Pas,-F2(Temoin-Pas)*2/3,-F1(Temoin-Pas)/2>,Pas texture {T_Bois rotate y*90}} //Interm�diraire
        cylinder {<Temoin,-F2(Temoin)*2/3,F1(Temoin)/2>,<Temoin-Pas,-F2(Temoin-Pas)*2/3,F1(Temoin-Pas)/2>,Pas texture {T_Bois rotate y*90}} //Interm�diraire

        cylinder {<Temoin,-F2(Temoin),0>,<Temoin-Pas,-F2(Temoin-Pas),0>,Pas texture {T_Bois rotate y*90}} //bas
        #declare Temoin=Temoin+Pas;
        #end
        translate <0,1/9,0>
        }
        
                                                
//Poisson1
#render concat ("creation du poisson 1",LF)
#macro F3 (Var)
pow (Var,1/1.5)*pow (Var-1,2)
#end

#declare K1=1;
#declare Pas=.01;        

#declare Poisson1=union {
        #declare Temoin=0;
        #while (Temoin<=1)
        torus {F3(Temoin),Pas
                rotate z*90
                translate <K1*Temoin,0,0>
                }
        #declare Temoin=Temoin+Pas;
        #end
        translate <-K1*1/2,0,0>
        }
                
#render concat ("Creation de la chaine",LF)
#declare Chaine1=Chaine (<7,5,-17>,<-5,30+25/9,80>,1.5,3)      //1.5    //.8
                        
#render concat ("Creation de l'ensemble bateau",LF)
#declare Ens1=union {
object {Bateau 
        scale 25 
        rotate z*15         
        //rotate -x*65
        rotate -y*(50)
        translate <-5,30,80>
        }

object {Ancre
        texture {T_Ancre1}
        texture {T_Ancre2}
        rotate <0,-40,0>
        rotate <30,0,0>   
        translate <0,5,0>
        translate <7,0,-17>
        }
        
object {Chaine1 
        texture {T_Ancre1}        
        texture {T_Ancre2}
        }
}
    

#render concat ("creation  du fond (height-field)",LF)
/* #declare Fond=height_field {        
        gif "mer.ghf"
        smooth
        translate -.5
        scale <100,3.5,100> //avant 2 en y
        texture {T_Sable}
        }
   */    
  
//scene           

#render concat ("On arrive dans la sc�ne...",LF)

object {Ens1}        

object {Boite
        scale 1/3
        rotate -y*30
        rotate x*12
        translate <-5,4,0>
        }
  
object {Etoile 
        scale .4 //.3
        rotate <-41,18,-5>
        translate <-5,0,2>}

                

/* object {Poisson1
        scale 10
        translate 5*y
        texture {pigment {color Yellow}}
        }
        */

// object {Fond}
// object {Fond translate <0,0,100>}
   
   
//plane {y,0 texture {T_Sable}}   

/*fog {   distance 90
        color MidnightBlue transmit .7
        turbulence 1.
        }
*/      

plane {y,30
        hollow on
        texture {
                pigment {color Clear}
                finish {
                        phong 1.0                                 
                        phong_size 10
                        reflection .8
                        }
                normal {bumps .6 scale <10,4,8>}
                }
        interior {caustics .4}
        }                
        
sky_sphere {
        pigment {
                gradient y color_map {
                [0 color MidnightBlue]
                [1 color Cyan]}
                }
        }                
        
         
light_source {10000*(y-z+x/2) color rgb 1}
light_source {10000*(y-z-x/3) color rgb .3}

camera {
        location <0,4,-40>
        direction 2*z
        look_at <0,2,0>
        }
        
        