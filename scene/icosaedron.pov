#include "colors.inc"
#include "textures.inc"
#include "geometry.inc"

#version 3.5;

// some flags to tune rendering
#declare Photons=on;
#declare Transp = on;
#declare AREA = on;


global_settings {
  assumed_gamma 1.0
  max_trace_level 10
  #if (Photons)          // global photon block
  photons {
    spacing 0.02                 // specify the density of photons
    //count 100000               // alternatively use a total number of photons

    //gather min, max            // amount of photons gathered during render [20, 100]
    //media max_steps [,factor]  // media photons
    //jitter 1.0                 // jitter phor photon rays
    //max_trace_level 5          // optional separate max_trace_level
    //adc_bailout 1/255          // see global adc_bailout
    //save_file "filename"       // save photons to file
    //load_file "filename"       // load photons from file
    //autostop 0                 // photon autostop option
    //radius 10                  // manually specified search radius
    // (---Adaptive Search Radius---)
    //steps 1
    //expand_thresholds 0.2, 40
  }

  #end
}

#if (Transp)
#declare M_Glass=    // Glass material
material {
  texture {
  pigment {rgbt 1}
  finish {
    ambient 0.0
    diffuse 0.05
    specular 0.6
    roughness 0.005
    reflection {
    0.1, 1.0
    fresnel on
    }
    conserve_energy
  }
  }
  interior {
  ior 1.5
  fade_power 1001
  fade_distance 0.9
  fade_color <0.5,0.8,0.6>
  }
}
#else 
  #declare M_Glass =material {
  texture {
    pigment {color Red}
    finish {
    diffuse .9
    reflection 0
    }
  }
  }
#end

#declare Radius_sphere=.15;
#declare Radius_cylinder=0.05;
#declare a=<0,1,0>;
#declare b=<2/5*sqrt(5),sqrt(5)/5,0>;
#declare c=vrotate (b,<0,72,0>);
#declare d=vrotate (<2/5*sqrt(5),-sqrt(5)/5,0>,<0,36,0>);

#declare Generator_2 = union {
  sphere {a,Radius_sphere}
  cylinder {a,b,Radius_cylinder}
  sphere {b,Radius_sphere}    
  cylinder {b,c,Radius_cylinder}
  sphere {c,Radius_sphere}    
  cylinder {c,d,Radius_cylinder}
  sphere {d,Radius_sphere}
  cylinder {b,d,Radius_cylinder}
}

#declare Generator_1 = intersection {
  Plane_From_3_Points (a,b,c)
  Plane_From_3_Points (c,b,d)

  Plane_From_3_Points (-b,-a,-c)
  Plane_From_3_Points (-b,-c,-d)
}


#declare Icosahedron = merge {
  #declare Temoin = 0;
  #while (Temoin<5)
  object {Generator_1
    rotate y*Temoin*72
  }
  #declare Temoin=Temoin+1;
  #end // loop
  #declare Temoin = 0;
//   merge {
//  #while (Temoin<5)
//    object {Generator_2
//    rotate y*Temoin*72
//    }
//    #declare Temoin=Temoin+1;
//  #end
//  }
}


#declare Generator_3 =union {
  intersection {
  Plane_From_3_Points (-a,-b,c) 
  Plane_From_3_Points (-b,-c,a)
  Plane_From_3_Points (-c,-a,b)
  plane {(a+b+c)/3,.1} 
  }
  intersection {
  Plane_From_3_Points (b,a,-c) 
  Plane_From_3_Points (c,b,-a)
  Plane_From_3_Points (a,c,-b)
  plane {-(a+b+c)/3,.1} 
  }

  intersection {
  Plane_From_3_Points (b,c,<0,sqrt(5)/5,0>) 
  Plane_From_3_Points (d,b,-c)
  Plane_From_3_Points (c,d,-b)
  plane {-(c+b+d)/3,.1} 
  }
  intersection {
  Plane_From_3_Points (-c,-b,-<0,sqrt(5)/5,0>) 
  Plane_From_3_Points (-b,-d,c)
  Plane_From_3_Points (-d,-c,b)
  plane {(c+b+d)/3,.1} 
  }
}

#declare Icosahedron_2 = merge {
  #declare Temoin = 0;
  #while (Temoin<5)
  object {Generator_3
    rotate y*Temoin*72
  }
  #declare Temoin=Temoin+1;
  #end 
}

object {Icosahedron_2
//  scale 2  
  rotate (y*12)
//  rotate -x*45
  material { M_Glass }
  photons {  // photon block for an object
  target 1.0
  refraction on
  reflection on
  }
}                       

plane {y,-2
  material {
  texture {
    pigment {color White}
    finish {
    reflection 0.9
    phong 0
    }
  }
  }
  photons {
  target 1.0
  reflection on
  }
}

plane {z,10
  hollow
  rotate y*10
  texture {
  pigment {
    color White
  }
  }
}


light_source {<0,10,-10> color <1,0,0>
  #if (AREA)
  area_light
  x,y,3,3
  #end
  spotlight
  point_at 0
  radius 10
  falloff 15
  photons {
  refraction on
  reflection on
  }
}
light_source {<0,10,-10> color <0,0,1>
  #if (AREA)
  area_light
  x,y,3,3
  #end
  spotlight
  point_at 0
  radius 10
  falloff 15
  photons {
  refraction on
  reflection on
  }
  rotate -y*30
}
light_source {<0,10,-10> color <0,1,0>
  #if (AREA)
  area_light
  x,y,3,3
  #end
  spotlight
  point_at 0
  radius 10
  falloff 15
  photons {
  refraction on
  reflection on
  }
  rotate y*30
}




camera {
  location <-2,3,-7>
  right <16/10,1,1>
  look_at 0
}