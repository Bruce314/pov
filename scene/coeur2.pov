#include "colors.inc"

camera {
   location  <1, -1,-6>
   direction <0, 0,  1>
   up        <0, 1,  0>
   right   <4/3, 0,  0>
   look_at   <0.5, 0.3, 0>
}

light_source {<10, 10, -15> color White}
light_source {<0, 20,0> color White} 
light_source {<-5,-5,-8> color White}
background{color Pink}

object {
  poly { 6,<8, 0, 0, 0, 12, 0, 0, 12, 0, -12, 0, 0, 0, 
   0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 12, 0, -12, 0, 0, 0, 
  0, 6, -0.100000, -12, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 3, 0, 
   -3, 0, 0, 0, 0, 3, -1, -6, 0, 3, 0, 0, 0, 0, 0, 0, 
   1, 0, -3, 0, 3, 0, -1> 
   
 texture{
   pigment {Red}
   finish {
      ambient 0.2
      diffuse 0.7
      phong 1
      phong_size 80
      brilliance 2
   }
 }
    rotate <0 90 0>
    rotate < 0, 0, 100>
    translate <0.01, 0.1, 0.01>
scale 1.5
}
//bounded_by{sphere{<0,0,0>,1 }}
}

