#include "colors.inc"
#include "textures.inc"
#include "robot.inc"

// set the maximum ray tracing bounce depth (1...20) [5]
#max_trace_level 20


#declare Bass_Res=1;

#declare T_1 = texture {
  pigment {color rgb <1,.8,.9>}
  finish {phong .1 phong_size 10 metallic}
  
}      

#declare T_1=texture {Gold_Metal 
  //normal {agate  .5 scale .1 rotate z*90}
}



#declare T_Slip = texture {
  pigment {
	leopard color_map {
	  [0 color Black]
	  [.2 color Gray10]
	  [.21 color Black]
	  [.5 color Black]
	  [.7 color Orange]
	  [.75 color Yellow]
	  [.85 color Orange]
	  [1 color Black]
	}       
	turbulence .5
	scale .02
  }
}

#declare Angle = 45;

#declare Mon_Rob=
  Droide2 (-15,-15,110,90, //-00,-00,00,0, //
	40,120,20,-80, 
	160,90,90,90,
	-20,10,0,45,0,0,0) //-20,10
#declare Mon_Rob=Droide2 (25,-8,8,-8
  30,0,10,75,
  -90,-30,-70,-20,
  0,0,0,20,30,90,0)

object {Mon_Rob
  rotate y*Angle                   
  translate 2*x
}                         
object {Mon_Rob
  //translate y
  rotate y*(180+Angle)
  translate -4*x
}                         

plane {y,0 //texture {pigment {checker color Black color White}}
  texture {Silver_Metal}
}
//plane {z,0 texture {pigment {checker color Black color White}}}

sky_sphere {pigment {gradient y color_map {
  [0 color Cyan]
  [1 color MidnightBlue]}
}}

light_source {<1000,3000,-4000> color .4*White}
light_source {<-1000,3000,-4000> color .6*White}

camera {location <0,5,-15>
  //        direction 2*z
  //        look_at 2*y
  
}