// reference file for camshaft
#include "came.inc"
// used for interpolate
#include "math.inc"
// used to help debugging
#include "b_debug.inc"


#declare angleCame=360*clock;
// #declare angleCame=10;

#ifdef (h_parameter)
  #declare local_h=h_parameter;
#else
  #declare local_h=1.2;
#end

#ifdef (TestCylinders)
union {
  #declare min_ang=-3*pi;
  #declare max_ang=3*pi;
  
  #declare ang=min_ang;
  #while (ang<max_ang)
    cylinder {-z*Interpolate(ang,min_ang,max_ang,0,1.5,1),z,0.05
        translate x*radiusCame(1,local_h,ang)
        rotate z*ang*180/pi
        texture  {
          pigment {
            color rgb <
            Interpolate(ang,min_ang,max_ang,1,0,2),
            Interpolate(ang,min_ang,max_ang,0,1,1),
            Interpolate(ang,min_ang,max_ang,1,0,0)
            >
          }
        }
    }
    #declare ang=ang+0.2;
  #end    
}
#end

union{
  union {
    came (1,local_h)
    cylinder {-0.2*z,0.2*z,0.1}
    texture {pigment {color rgb <1,0,0>}}
    rotate z*angleCame
  }

  union {
/*    cylinder {-0.1*z,0.1*z,0.2
      texture {
        pigment {
          color rgb <1,0,1>
        }
      }
    }*/
    cone {
      -0.2*y,0,0,0.1
      texture {
        pigment {
          color rgb <1,1,1>
        }
      }

    }
    cylinder {0,5*y,0.1
      texture { 
        pigment {
          gradient y 
          color_map {
            [0.0 color rgb <1,0,0>]
            [0.0 color rgb <1,0,0>]
            [1.0 color rgb <0,1,0>]
          }
        }
      }
    }
    translate y*0.2
    translate y*radiusCame(1,local_h,angleCame*pi/180+pi/2)
  }
  rotate -y*10
  rotate x*15
}


camera {
  location <0,0.1,-6> 
  look_at 0
  right x*image_width/image_height
  up y
}

light_source {10000*(y-z) color rgb <1,0,1>}
light_source {10000*(y-z-x) color rgb <0,1,1>}
light_source {10000*(y-z+x) color rgb <1,1,0>}