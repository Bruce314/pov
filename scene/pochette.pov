/* future include robot */

#include "colors.inc"
#include "textures.inc"
#include "robot01.inc"

global_settings {
  ambient_light .7
  max_trace_level 20
  }
  
#declare T_1=texture {
  pigment {color Orange} 
  finish {
    phong .8 
    phong_size 20 
    metallic 
    //reflection .3
    }
  }
#declare T_2=texture {
  pigment {color rgb <0,0,.6>} 
  finish {
    phong .8 
    phong_size 200 
    metallic 
    //reflection .4
    }
  }

//#declare T_1=texture {Gold_Metal}
//#declare T_1=texture {Silver_Metal}

#declare T_3= texture {
        pigment {color rgb <.7,.7,.9>}
        finish {phong 1.0 phong_size 20}
        normal {bumps 1 scale .005}
        }
  
#declare Temps=clock*16;

// #declare Bass_Res=true; //quand cet identificateur existe, le robot est moins beau
                        //pas de cheveux, pas de logo...

#declare J1A1=10*(1+sin(Temps*2*pi));
#declare J1A2=-10*(1+sin(Temps*2*pi));
#declare J2A1=10*(1-sin(Temps*2*pi));
#declare J2A2=-10*(1-sin(Temps*2*pi));
#declare R_B_Z=-5*sin (Temps*2*pi);
#declare R_M_D=0;
#declare R_M_G=-0;

#switch (Temps)
#range (0,1) //les seins 1
   #declare B1A1=40;
   #declare B2A1=40;     
   #declare B1A2=60;
   #declare B2A2=60;     
   #declare B1A3=135;
   #declare B2A3=135;     
   #declare B1A4=90;
   #declare B2A4=90;     
   #declare O_M_D=40;
   #declare O_M_G=40;
   #declare R_M_D=90*sin( Temps*2*pi);
   #declare R_M_G=-90*sin( Temps*2*pi);
   #break
#range (1,2) //les seins 2
   #declare B1A1=40;
   #declare B2A1=40;     
   #declare B1A2=60;
   #declare B2A2=60;     
   #declare B1A3=135;
   #declare B2A3=135;     
   #declare B1A4=90;
   #declare B2A4=90;     
   #declare O_M_D=40;
   #declare O_M_G=40;
   #declare R_M_D=90*sin( Temps*2*pi);
   #declare R_M_G=-90*sin( Temps*2*pi);
   #break
#range (2,3) //les tetons 1
   #declare B1A1=40;
   #declare B2A1=40;     
   #declare B1A2=60;
   #declare B2A2=60;     
   #declare B1A3=135;
   #declare B2A3=135;     
   #declare B1A4=90;
   #declare B2A4=90;     
   #declare O_M_D=5;
   #declare O_M_G=5;
   #declare R_M_D=90*sin( Temps*2*pi);
   #declare R_M_G=-90*sin( Temps*2*pi);
   #break
#range (3,4) //les tetons 2
   #declare B1A1=40;
   #declare B2A1=40;     
   #declare B1A2=60;
   #declare B2A2=60;     
   #declare B1A3=135;
   #declare B2A3=135;     
   #declare B1A4=90;
   #declare B2A4=90;     
   #declare O_M_D=5;
   #declare O_M_G=5;
   #declare R_M_D=90*sin( Temps*2*pi);
   #declare R_M_G=-90*sin( Temps*2*pi);
   #break
#range (4,5) //les seins 1'
   #declare B1A1=40;
   #declare B2A1=40;     
   #declare B1A2=60;
   #declare B2A2=60;     
   #declare B1A3=135;
   #declare B2A3=135;     
   #declare B1A4=90;
   #declare B2A4=90;     
   #declare O_M_D=40;
   #declare O_M_G=40;
   #declare R_M_D=90*sin( Temps*2*pi);
   #declare R_M_G=-90*sin( Temps*2*pi);
   #break  
#range (5,6) //les seins 2'
   #declare B1A1=40;
   #declare B2A1=40;     
   #declare B1A2=60;
   #declare B2A2=60;     
   #declare B1A3=135;
   #declare B2A3=135;     
   #declare B1A4=90;
   #declare B2A4=90;     
   #declare O_M_D=40;
   #declare O_M_G=40;
   #declare R_M_D=90*sin( Temps*2*pi);
   #declare R_M_G=-90*sin( Temps*2*pi);
   #break
#range (6,7) //les fesses 1
   #declare B1A1=30;
   #declare B2A1=30;     
   #declare B1A2=45;
   #declare B2A2=45;     
   #declare B1A3=90;
   #declare B2A3=90;     
   #declare B1A4=98;
   #declare B2A4=98;     
   #declare O_M_D=40;
   #declare O_M_G=40;
   #declare R_M_D=90*sin( Temps*2*pi);
   #declare R_M_G=-90*sin( Temps*2*pi);
   #break
#range (7,8) //les fesses 2
   #declare B1A1=30;
   #declare B2A1=30;     
   #declare B1A2=45;
   #declare B2A2=45;     
   #declare B1A3=90;
   #declare B2A3=90;     
   #declare B1A4=98;
   #declare B2A4=98;     
   #declare O_M_D=40;
   #declare O_M_G=40;
   #declare R_M_D=90*sin( Temps*2*pi);
   #declare R_M_G=-90*sin( Temps*2*pi);
   #break
#range (8,9) //j'enfourche
   #declare B1A1=20+10*cos(Temps*pi*4);
   #declare B2A1=20+10*cos(Temps*pi*4);     
   #declare B1A2=-22.5+57.5*cos (Temps*pi*2);
   #declare B2A2=-22.5+57.5*cos (Temps*pi*2);         
   #declare B1A3=80+10*cos (Temps*pi*2);
   #declare B2A3=80+10*cos (Temps*pi*2);     
   #declare B1A4=89+9*cos (Temps*2*pi);
   #declare B2A4=89+9*cos (Temps*2*pi);     
   #declare O_M_D=40;
   #declare O_M_G=40;
   #break
#range (9,10) //je r�cidive
   #declare B1A1=20+10*cos(Temps*pi*4);
   #declare B2A1=20+10*cos(Temps*pi*4);     
   #declare B1A2=-22.5+57.5*cos (Temps*pi*2);
   #declare B2A2=-22.5+57.5*cos (Temps*pi*2);         
   #declare B1A3=80+10*cos (Temps*pi*2);
   #declare B2A3=80+10*cos (Temps*pi*2);     
   #declare B1A4=89+9*cos (Temps*2*pi);
   #declare B2A4=89+9*cos (Temps*2*pi);     
   #declare O_M_D=40;
   #declare O_M_G=40;
   #break  
#range (10,11) //je m'occuppe
   #declare B1A1=45;
   #declare B2A1=30;     
   #declare B1A2=45;
   #declare B2A2=0;         
   #declare B1A3=81+9*cos (Temps*2*pi);
   #declare B2A3=10;     
   #declare B1A4=120-30*cos (Temps*2*pi);
   #declare B2A4=100;     
   #declare O_M_D=20;
   #declare O_M_G=40;
   #break
#range (11,12)//je m'occuppe
   #declare B1A1=45;
   #declare B2A1=30;     
   #declare B1A2=45;
   #declare B2A2=0;         
   #declare B1A3=81+9*cos (Temps*2*pi);
   #declare B2A3=10;     
   #declare B1A4=120-30*cos (Temps*2*pi);
   #declare B2A4=100;     
   #declare O_M_D=20;
   #declare O_M_G=40;
   #break
#range (12,16)//prends �a
   #declare B1A1=37.5+7.5*cos (Temps*pi);
   #declare B2A1=80;     
   #declare B1A2=60*(1-cos (Temps*pi));
   #declare B2A2=0;         
   #declare B1A3=95+15*cos (Temps*pi);
   #declare B2A3=80;     
   #declare B1A4=110-50*cos (Temps*pi);
   #declare B2A4=90;     
   #declare O_M_D=40;
   #declare O_M_G=5;
   #break
#end        



#declare Objet =Droide1 (J1A1,J1A2,J2A1,J2A2,
        B1A1,B1A2,B2A1,B2A2,
        B1A3,B1A4,B2A3,B2A4,
        0,R_B_Z,O_M_D,O_M_G,R_M_D,R_M_G)

object {Objet
        translate -3*y 
        rotate y*(30)
        }
 plane {y,-3 
  texture {pigment {checker color Yellow color Magenta turbulence .3} scale 4}
 // texture {pigment {color Black}}
  }        

/*
sky_sphere {
  pigment {
    gradient y
    color_map {
      [0 color Pink]
      [1 color Cyan]
      }
    }
  pigment {color White}  
  }*/
  
 //background {White}
  
camera {location <0,6,-27>
        right x
        up y
        direction 2*z
        look_at 3.4*y      
        }
        
light_source {100*(y-z-x) color 1.2*White
        area_light 4*(-x+z)/.707,4*y,5,5 //10,10,8,8 
        jitter adaptive 2                       //10,10,5,5 peut sembler suffisant
        spotlight 
        point_at 2*y
        radius 2
        falloff 3
        }                
              
light_source {100*(y-z+x) color 1.2*White
        area_light 4*(x+z)/.707,4*y,5,5 //8,8
        jitter adaptive 2
        spotlight 
        point_at 2*y
        radius 2
        falloff 3
        }        
                
                