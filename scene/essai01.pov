/* Include Chaine
Contient une macro
Chaine (Origine (vect),fin (vect), maille (float))
*/
/*
**************************
* Nécessite Matrices.inc *
**************************
*/

#macro Chaine (Orig,Fin,Maille,Type)
#if (vlength (vcross (Fin-Orig,y)) = 0)

#local Direct1= vnormalize (Fin-Orig);
#local Direct2=x;
#local Direct3=z;

#else

#local Direct1= vnormalize (Fin-Orig);
#local Direct2= vnormalize (vcross (Direct1,y));
#local Direct3= vcross (Direct1,Direct2);
#end

#if (Type=1)
#local Maillon= union {
        difference {
                cylinder {-y*Maille/10,y*Maille/10,3*Maille/8
                        matrix Transf (Direct1,Direct2,Direct3)
                        translate Direct1*Maille/4
                        }
                cylinder {-y*Maille/8,y*Maille/8,Maille/4
                        matrix Transf (Direct1,Direct2,Direct3)
                        translate Direct1*Maille/4
                        }
                }                        
        difference {
                cylinder {-z*Maille/10,z*Maille/10,Maille*3/8
                        matrix Transf (Direct1,Direct2,Direct3)
                        translate Direct1*Maille*3/4
                        }
                cylinder {-z*Maille/8,z*Maille/8,Maille/4
                        matrix Transf (Direct1,Direct2,Direct3)
                        translate Direct1*Maille*3/4
                        }
                }                                       
        }
#end
#if (Type=2)
#local Maillon= union {
        torus {Maille*3/8-Maille/20,Maille/10
                matrix Transf (Direct1,Direct2,Direct3)
                translate Direct1*Maille*1/4
                }
        torus {Maille*3/8-Maille/20,Maille/10
                rotate x*90
                matrix Transf (Direct1,Direct2,Direct3)
                translate Direct1*Maille*3/4
                }                                       
        }
#end

union {          
#local Temoin=0;
#while (Temoin<((vlength (Orig-Fin))/Maille))
object {Maillon
        translate (Orig+Maille*Temoin*Direct1)
        }
#local Temoin=Temoin+1;
#end        //du while
}

#end //de la macro   

/*
#declare T=Chaine (<-10,-5,4>,<10,5,12>,3,2)

object {T texture {pigment {color rgb <1,0,0>}}}

light_source {1000*(y-z) color rgb 1}

camera {location <0,2,-20>
        look_at 0
        }
        */