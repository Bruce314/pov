// gears : circular_pitch
#include "gears.inc"


#declare ang_1=360/24*clock; //deg/clock
#declare ang_2=-2*ang_1;
#declare ang_3=-(24/18)*ang_2;

#declare gear_thickness=0.2;
#declare tol=0.1;


#declare g1=Gear (0.1,24)
#declare g2=Gear (0.1,12)
#declare g3=Gear (0.1,18)


union {
  object {g1 
    texture {
  	  pigment { color rgb <1,1,0> }
    }
    rotate y*0	
  	rotate z*(360/24/2+ang_1)
    translate -x*pitchDiameter(0.1,24)
  }

  union {
    object {g2
      texture {
  	    pigment { color rgb <0,1,1> }
      }
	  }
    object {g1 
      texture {
  	    pigment { color rgb <1,1,1> }
      }
	  	translate (gear_thickness+tol)*z
			rotate z*(360/24/2)
    }
    rotate y*0
  	rotate z*(ang_2)
    translate x*pitchDiameter(0.1,12)
  }

  union {
    object {g3
      texture {
  	    pigment { color rgb <1,0,1> }
      }
	  	translate (gear_thickness+tol)*z
	  }
  	rotate z*(ang_3)
    translate x*(pitchDiameter(0.1,12)+pitchDiameter(0.1,24)+pitchDiameter(0.1,18))
  }

  rotate x*10
	rotate -y*10
}

camera {location <0,0,-3> look_at 0}

light_source {10000*(y-z) color rgb <1,1,1>}
light_source {10000*(y-z-x) color rgb <1,0,0>}
light_source {10000*(y-z+x) color rgb <0,1,0>}