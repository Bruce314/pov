#include "colors.inc"
#include "textures.inc"

global_settings {
        max_trace_level 15
        }

#declare Pi=3.14159265358;

#declare Angle1=360*clock;
#declare Angle2=-216*clock;
#declare Angle3=144*clock;
#declare Angle4=360*clock;
#declare Angle5=-180*clock;

#declare Temps1=360*clock;
#declare Temps2=- abs (-2*clock+1);

#declare E1=abs(sin(Pi*clock));
#declare E2=abs(cos(2*Pi*clock));
#declare E3=abs(sin(3*Pi*clock));
#declare Coef=1.5;

#declare T_1=texture {Gold_Texture}
#declare T_2=texture {Bronze_Texture}
#declare T_3=texture {Copper_Texture}
#declare T_4=texture {Silver_Texture}


#declare Objet=blob {
        threshold .7
        sphere {y,1,1 
                texture {T_1}
                }
        sphere {<sin (Angle4*Pi/180)*1.5,1,0>,1,1 
                texture {T_2}
                rotate y*Angle5
                }
        sphere {<sin (Angle4*Pi/180)*1.5,1,0>,1,1 
                texture {T_2}
                rotate y*(Angle5+180)
                }
        sphere {<Temps2,Temps2*Temps2+1,0>,1,1
                texture {T_3}
                rotate y*Angle4
                }
        sphere {<Temps2,Temps2*Temps2+1,0>,1,1
                texture {T_3}
                rotate y*(Angle4+180)
                }
        sphere {<0,1.5+abs (sin (Angle4*Pi/180)),0>,1,1 
                texture {T_4}
                }
        }                
        
object {Objet
        scale 2
        }

cylinder {-.5*y,0,5
        texture {
                pigment {spiral1 5 
                        color_map {
                                [0 color Blue]
                                [1 color Yellow]
                                }
                        }
                finish {
                        phong .2
                        phong_size 20
                        reflection .3
                        diffuse .6
                        }
                rotate x*90                        
                }
        rotate -y*Angle1*10
        }

cylinder {-y,-.5*y,6
        texture {
                pigment {spiral1 5 
                        color_map {
                                [0 color Red]
                                [1 color Yellow]
                                }
                        }
                finish {
                        phong .2
                        phong_size 20
                        reflection .3
                        diffuse .6
                        }
                rotate -x*90                        
                }
        rotate -y*Angle2*4
        }

cylinder {-1.5*y,-1*y,7
        texture {
                pigment {spiral1 5 
                        color_map {
                                [0 color Red]
                                [1 color Blue]
                                }
                        }
                finish {
                        phong .2
                        phong_size 20
                        reflection .3
                        diffuse .6
                        }
                rotate x*90                        
                }
        rotate y*Angle3*5
        }

light_source {
        10000*(x+y-z) color E1*Coef*Blue
        }
light_source {
        10000*(y-z) color E2*Coef*Green
        }
light_source {
        10000*(-x+y-z) color E3*Coef*Red
        }

camera {
        location <0,7+2*sin (Temps1*Pi/180),-10>
        look_at y
        }       