#/bin/bash

DEFAULT_BASE=engine
BASE=${1:-${DEFAULT_BASE}}
OUTPUT=output

WIDTH=853
HEIGHT=480

find  ${OUTPUT} -iname "anim_${BASE}*.tga" -delete
povray scene/${BASE}.ini -w${WIDTH} -h${HEIGHT} -O${OUTPUT}/anim_${BASE}

# povray -Iscene/$BASE.pov -w${WIDTH} -h${HEIGHT} -ki0 -kf1 -kff100 -kfi0 -kc

convert -delay 10 -size ${WIDTH}x${HEIGHT} -loop 0  output/anim_${BASE}*.tga ${BASE}.gif
