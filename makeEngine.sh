#/bin/bash
BASE="engine"
OUTPUT=output

for a in `seq 0 0.125 0.999`; do 
		OUT=$OUTPUT/${BASE}_${a}.tga
		OUT2=${OUTPUT}/${BASE}_${a}.jpg
		TOTAL="${TOTAL} ${OUT2}"
		povray -W700 -H350 -K${a} -Iscene/$BASE.pov -O${OUT} 
		convert ${OUT} -fill white -stroke red -pointsize 40 -gravity south -annotate 0 "h:$a" ${OUT2}
done

montage ${TOTAL} -geometry '+2+2' -tile '2x4' result.jpeg
xdg-open result.jpeg
