#/bin/bash


DEFAULT_BASE="engine"
BASE=${1:-${DEFAULT_BASE}}
OUTPUT=output

rm -rf ${OUTPUT}/animation
rm ${BASE}.mp4
find  ${OUTPUT} -iname "$BASE*.tga" -delete
mkdir -p ${OUTPUT}/animation
povray scene/${BASE}.ini -W1920 -H1080 -o${OUTPUT}/animation/ 
cd output/animation

parallel convert '{}' 'a_{/.}.jpg' ::: ./${BASE}*.tga
parallel convert '{}' 'b_{/.}.jpg' ::: ./${BASE}*.tga
parallel convert '{}' 'c_{/.}.jpg' ::: ./${BASE}*.tga
parallel convert '{}' 'd_{/.}.jpg' ::: ./${BASE}*.tga
parallel convert '{}' 'e_{/.}.jpg' ::: ./${BASE}*.tga
parallel convert '{}' 'f_{/.}.jpg' ::: ./${BASE}*.tga
parallel convert '{}' 'g_{/.}.jpg' ::: ./${BASE}*.tga
parallel convert '{}' 'h_{/.}.jpg' ::: ./${BASE}*.tga

ffmpeg -framerate 25 -pattern_type glob -i '*.jpg' -c:v libx264 -r 25  ../../${BASE}.mp4

# ncoder mf://*.jpg -mf w=1280:h=720:fps=25:type=jpg -ovc lavc -lavcopts vcodec=mpeg4:mbd=2:trell:vbitrate=12000 -oac copy -o $BASE.avi
cd -
