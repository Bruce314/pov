#/bin/bash
BASE="came"
OUTPUT=output

find  $OUTPUT -iname "$BASE*.tga" -delete
TOTAL=""
for a in `seq 0.1 0.1 1.6`; do 
		OUT=$OUTPUT/${BASE}_${a}.tga
		OUT2=${OUTPUT}/${BASE}_${a}.jpg
		TOTAL="${TOTAL} ${OUT2}"
		povray -W200 -H200 +k0.1 -Iscene/$BASE.pov -O${OUT} Declare=h_parameter=$a
		convert ${OUT} -fill white -stroke red -pointsize 40 -gravity south -annotate 0 "h:$a" ${OUT2}
done

montage ${TOTAL} -geometry +2+2 result.jpeg
