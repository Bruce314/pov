#/bin/bash

DEFAULT_BASE="Engine"

export BASE=${1:-${DEFAULT_BASE}}
export OUTPUT=output

export X_SIZE=${2:-"3"}
export Y_SIZE=${3:-"3"}

# TODO : compute size of pictures to render...

function render() {
		CLOCK=$1
		OUT=$OUTPUT/${BASE}_${CLOCK}.tga
		OUT2=${OUTPUT}/${BASE}_${CLOCK}.jpg
		TOTAL="${TOTAL} ${OUT2}"
		povray -W700 -H350 -K${CLOCK} -Iscene/$BASE.pov -O${OUT} 
		convert ${OUT} -fill white -stroke red -pointsize 40 -gravity south -annotate 0 "h:${CLOCK}" ${OUT2}
}

export -f render
export OFFSET=`bc <<< "scale=10;1/(${X_SIZE}*${Y_SIZE}-1)"`

CLOCK_VALUES=`seq 0 ${OFFSET} 1`

for a in ${CLOCK_VALUES}; do
		TOTAL="$TOTAL ${OUTPUT}/${BASE}_${a}.jpg"
done

parallel render {} ::: ${CLOCK_VALUES}

#montage ${TOTAL} -geometry '+2+2' -tile '2x4' result.jpeg
montage ${TOTAL} -geometry '+2+2' -tile "${X_SIZE}x${Y_SIZE}" result.jpeg

xdg-open result.jpeg
