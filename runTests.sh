#!/bin/bash

find . -iname '*.pov' > files.txt


# truncate error.txt
echo > error.txt

#temp locations
rm -rf logs
rm -rf output
mkdir -p output
mkdir -p logs

parallel 'grep {} whitelist.txt || povray -W5 -H5 -I{} -Ooutput/{/.}.tga 2>logs/{/.}.log || echo {} >> error.txt ' :::: files.txt


