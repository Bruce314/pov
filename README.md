#README#
This project is a collection of includes and scenes for POV (Persistence of Vision).
Some scene are really old (as in literally more than 10 years old) and used to work with pov 2 and hence are most likely broken.

Most of it is a mix of french and english, depending when it was
created. It's likely that the scene themselves are not of interest for
you, but there are some libraries which can be useful.
In addition, some of the files are used in conjunction with my blog
articles, that can be found on
[www.revelut.ch with tag pov](http://www.revelut.ch/tag/pov/).



#Libraries (a.k.a includes)#
- `chaine.inc`: allows to draw a chain between two positions
- `matrices.inc`: (a.k.a.) matrix : allows to do some basic matrices operations, dependency for some of the remaining. Transf is of particular interest as it allows to go easily from one base to the other. 
- `lego.inc`: allows to draw using CSG a lego brick of a given size
- `E_Nom.inc`: allows to write using lego bricks
- `robot.inc`: allows to draw a robot or an android. Good luck understanding the parameters
- `geometry.inc`: contains a useful macro to draw a plane containing 3 given points.

#Scenes#
- `coeur.pov`: draw a heart using a bicubic(?) implicit surface 
- `icosaedron.pov`: nice for wallpaper, a spiked-icosaedron
- `engine.pov`: work in progress of a full 4-stroke engine animation
- `box.pov`: reference for [blog article on box building](http://www.revelut.ch/2016/02/16/box-with-non-vertical-sides/)
