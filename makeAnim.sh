#/bin/bash
BASE="walking_robots"
OUTPUT=output

rm -rf $OUTPUT/animation
find  $OUTPUT -iname "$BASE*.tga" -delete
mkdir -p $OUTPUT/animation
cd ./scene/animations
povray $BASE.ini
cd -
cd output/animation
parallel convert '{}' 'a_{/.}.jpg' ::: ../$BASE*.tga
parallel convert '{}' 'b_{/.}.jpg' ::: ../$BASE*.tga
parallel convert '{}' 'c_{/.}.jpg' ::: ../$BASE*.tga
parallel convert '{}' 'd_{/.}.jpg' ::: ../$BASE*.tga
parallel convert '{}' 'e_{/.}.jpg' ::: ../$BASE*.tga
mencoder mf://*.jpg -mf w=1280:h=720:fps=25:type=jpg -ovc lavc -lavcopts vcodec=mpeg4:mbd=2:trell:vbitrate=12000 -oac copy -o $BASE.avi
