/*
Crée une brique de lego de Nb_Larg pico de larges et Nb_Long picos de long
La variable Quality permet de choisir le niveau de rendu : 
0 )=> rapide, les briques sont angulaires
1 )=> nettement plus lent, beaucoup plus dobjets, les briques sont arrondies, pico central pour les briques de largeur >= 2
NB : Lego is a trademark and when used, one should read "something similar to a lego brick"
*/

#macro Lego (Nb_Larg,Nb_Long,Quality)

#local Largeur_Bloc = 4;
#local Hauteur_Bloc = 5;
#local Epaisseur = .75;
#local Rayon_Pic = Largeur_Bloc/2 - Epaisseur;
#local Hauteur_Pic = Hauteur_Bloc / 4;
#local Coef_Arrondi = 0.1;
#local Rayon_Centre = sqrt(2) * (Largeur_Bloc/2) - Rayon_Pic;

#if (Quality = 1)
#local Centre = difference {
  merge {
    cylinder {Epaisseur*y,(Hauteur_Bloc-Epaisseur/2)*y,Rayon_Centre
      }
    torus {Rayon_Centre-Epaisseur/2,Epaisseur/2
      translate y*Epaisseur
      }
    }
  cylinder {0,(Hauteur_Bloc-Epaisseur)*y,Rayon_Centre-Epaisseur
    }
  //texture {pigment {color Black}}
  }
//#render concat (str(Rayon_Centre,2,2)," ",str(Rayon_Centre-Epaisseur,2,2),chr(13),chr(10))
#end //du if de qualité des pivots centraux


#if (Quality = 0)
#local Pic = cylinder {0,Hauteur_Pic*y,Rayon_Pic
  translate Hauteur_Bloc * y
  }
#else
#local Pic = merge {
  cylinder {0,Hauteur_Pic*(1-Coef_Arrondi)*y,Rayon_Pic}
  torus {Rayon_Pic-Hauteur_Pic*Coef_Arrondi,Hauteur_Pic*Coef_Arrondi
    translate y*Hauteur_Pic*(1-Coef_Arrondi)
    } 
  cylinder {y*Hauteur_Pic*(1-Coef_Arrondi),Hauteur_Pic*y,Rayon_Pic-Hauteur_Pic*Coef_Arrondi}
  translate Hauteur_Bloc * y
  }
#end //du if de qualité des picots

#if (Quality = 0)
#local Bloc = difference {
  box {<-Nb_Long/2*Largeur_Bloc,0,-Nb_Larg/2*Largeur_Bloc>,<Nb_Long/2*Largeur_Bloc,Hauteur_Bloc,Nb_Larg/2*Largeur_Bloc>}
  box {<-Nb_Long/2*Largeur_Bloc+Epaisseur,-Epaisseur,-Nb_Larg/2*Largeur_Bloc+Epaisseur>,
    <Nb_Long/2*Largeur_Bloc-Epaisseur,Hauteur_Bloc-Epaisseur,Nb_Larg/2*Largeur_Bloc-Epaisseur>
    }
  }
#else
#local Delta = Coef_Arrondi * Hauteur_Bloc;
#local Bloc = difference {
  merge {
    //box qui fait le toit
    box {<-Nb_Long/2*Largeur_Bloc+Delta,0,-Nb_Larg/2*Largeur_Bloc+Delta>,
      <Nb_Long/2*Largeur_Bloc-Delta,Hauteur_Bloc,Nb_Larg/2*Largeur_Bloc-Delta>}
    //et les deux bords
    box {<-Nb_Long/2*Largeur_Bloc,0,-Nb_Larg/2*Largeur_Bloc+Delta>,
      <Nb_Long/2*Largeur_Bloc,Hauteur_Bloc-Delta,Nb_Larg/2*Largeur_Bloc-Delta>
      }
    box {<-Nb_Long/2*Largeur_Bloc+Delta,0,-Nb_Larg/2*Largeur_Bloc>,
      <Nb_Long/2*Largeur_Bloc-Delta,Hauteur_Bloc-Delta,Nb_Larg/2*Largeur_Bloc>}
    //les arrondis verticaux
    cylinder {0,(Hauteur_Bloc - Delta)*y,Delta
      translate <Nb_Long/2*Largeur_Bloc-Delta,0,Nb_Larg/2*Largeur_Bloc-Delta>
      }
    cylinder {0,(Hauteur_Bloc - Delta)*y,Delta
      translate <Nb_Long/2*Largeur_Bloc-Delta,0,-Nb_Larg/2*Largeur_Bloc+Delta>
      }
    cylinder {0,(Hauteur_Bloc - Delta)*y,Delta
      translate <-Nb_Long/2*Largeur_Bloc+Delta,0,Nb_Larg/2*Largeur_Bloc-Delta>
      }
    cylinder {0,(Hauteur_Bloc - Delta)*y,Delta
      translate <-Nb_Long/2*Largeur_Bloc+Delta,0,-Nb_Larg/2*Largeur_Bloc+Delta>
      }
    //les arrondis de coins supérieurs.
    sphere {(Hauteur_Bloc - Delta)*y,Delta
      translate <Nb_Long/2*Largeur_Bloc-Delta,0,Nb_Larg/2*Largeur_Bloc-Delta>
      }
    sphere {(Hauteur_Bloc - Delta)*y,Delta
      translate <Nb_Long/2*Largeur_Bloc-Delta,0,-Nb_Larg/2*Largeur_Bloc+Delta>
      }
    sphere {(Hauteur_Bloc - Delta)*y,Delta
      translate <-Nb_Long/2*Largeur_Bloc+Delta,0,Nb_Larg/2*Largeur_Bloc-Delta>
      }
    sphere {(Hauteur_Bloc - Delta)*y,Delta
      translate <-Nb_Long/2*Largeur_Bloc+Delta,0,-Nb_Larg/2*Largeur_Bloc+Delta>
      }
    //les arrondis supérieurs.
    cylinder {<-Nb_Long/2*Largeur_Bloc+Delta,0,-Nb_Larg/2*Largeur_Bloc+Delta>,
      <Nb_Long/2*Largeur_Bloc-Delta,0,-Nb_Larg/2*Largeur_Bloc+Delta>,Delta
      translate y*(Hauteur_Bloc-Delta)
      }
    cylinder {<-Nb_Long/2*Largeur_Bloc+Delta,0,+Nb_Larg/2*Largeur_Bloc-Delta>,
      <Nb_Long/2*Largeur_Bloc-Delta,0,+Nb_Larg/2*Largeur_Bloc-Delta>,Delta
      translate y*(Hauteur_Bloc-Delta)
      }
    cylinder {<-Nb_Long/2*Largeur_Bloc+Delta,0,-Nb_Larg/2*Largeur_Bloc+Delta>,
      <-Nb_Long/2*Largeur_Bloc+Delta,0,Nb_Larg/2*Largeur_Bloc-Delta>,Delta
      translate y*(Hauteur_Bloc-Delta)
      }
    cylinder {<Nb_Long/2*Largeur_Bloc-Delta,0,-Nb_Larg/2*Largeur_Bloc+Delta>,
      <Nb_Long/2*Largeur_Bloc-Delta,0,Nb_Larg/2*Largeur_Bloc-Delta>,Delta
      translate y*(Hauteur_Bloc-Delta)
      }
    }
  box {<-Nb_Long/2*Largeur_Bloc+Epaisseur,-Epaisseur,-Nb_Larg/2*Largeur_Bloc+Epaisseur>,
    <Nb_Long/2*Largeur_Bloc-Epaisseur,Hauteur_Bloc-Epaisseur,Nb_Larg/2*Largeur_Bloc-Epaisseur>
    }
  }
#end //du if de qualité de la boite

merge {
  #local Pic_X = 1;
  #while (Pic_X <= Nb_Long)
    #local Pic_Z = 1;
    #while (Pic_Z <= Nb_Larg)
      object {Pic
        translate ((Pic_X-Nb_Long/2-1/2)*x+(Pic_Z-Nb_Larg/2-1/2)*z)*Largeur_Bloc
        }
      #local Pic_Z = Pic_Z +1;
      #end
    #local Pic_X = Pic_X + 1;
    #end
  object {Bloc}
  #if ((Quality = 1) * (Nb_Larg>=2) * (Nb_Long>=2))
    #local Pic_X = 1;
    #while (Pic_X < Nb_Long)
      #local Pic_Z = 1;
      #while (Pic_Z < Nb_Larg)
        object {Centre
          translate ((Pic_X-Nb_Long/2)*x+(Pic_Z-Nb_Larg/2)*z)*Largeur_Bloc
          }
        #local Pic_Z = Pic_Z +1;
        #end
      #local Pic_X = Pic_X + 1;
      #end
    #end //du if pour les centres
  }
#end //de la macro

