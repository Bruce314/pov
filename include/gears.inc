#declare pitchDiameter = function (circular_pitch,nb_teeth) {nb_teeth*circular_pitch/pi}
#macro Gear (circular_pitch,nb_teeth)
  #declare p_d=pitchDiameter(circular_pitch,nb_teeth);
  difference {
    union {
		  //base cylinder 
			cylinder {-0.1*z,0.1*z,p_d}
			union {
				#declare i=0;
				#while (i<nb_teeth)
			  cylinder {-0.1*z,0.1*z,circular_pitch /2
				  translate x*p_d
					rotate z*360*i/nb_teeth
				}
				#declare i = i+1;
				#end
			}
    }
		union {
		  #declare i=0;
      #while (i<nb_teeth)
			cylinder {-0.11*z,0.11*z,circular_pitch /2
			  translate x*p_d
			  rotate z*360*(i+0.5)/nb_teeth
			}
			#declare i = i+1;
			#end
		}
  }
#end
