

// broken concept !
#macro SpringSpire(BaseRadius,WireRadius,SpireHeight)
  #local MyAngle=atan(SpireHeight/(4*BaseRadius));

  merge {
    #local Index=0;
    #while (Index<4)
      merge {
        intersection {
          torus {BaseRadius,WireRadius
            translate y*WireRadius
          }
          plane {-x,0}
          plane {z,0}
        }
        sphere {0,WireRadius
          translate -z*BaseRadius
          translate y*WireRadius
        }
        rotate z*(MyAngle*180/pi)
        translate y*SpireHeight/4*(Index+1)
        rotate -y*Index*90
      }
      #local Index=Index+1;
    #end
  }
#end
  
// sprint left on x-z plan and growing on y>0
#macro Spring(BaseRadius,NbSpires,WireRadius,Length)
  merge {
    torus {BaseRadius,WireRadius
      translate y*WireRadius
    }

    #local Previous=<0,WireRadius,-BaseRadius>;
    #local AngularIncrement=pi/30;
    
    merge {
      sphere {Previous,WireRadius}
      #local CurrentValue=AngularIncrement;
      #while (CurrentValue<=2*pi*NbSpires)
        #local Next=<BaseRadius*sin(CurrentValue),WireRadius+CurrentValue*(Length-WireRadius*2)/(2*pi*NbSpires),-BaseRadius*cos(CurrentValue)>;
        cylinder {Previous,Next,WireRadius}
        sphere {Next,WireRadius }
        
        #local CurrentValue=CurrentValue+AngularIncrement;
        #local Previous=Next;
      #end
    }
    torus {BaseRadius,WireRadius
      translate y*(Length-WireRadius)
    }
}
#end