// quality can be 1 : nice or 0 : not so nice 
#macro Ecris_Nom (Chaine,Quality)

/* D�claration des constantes et des variables */
#local Chaine1 = strupr (Chaine) //on convertit en majuscules
#local Longueur = strlen (Chaine1); //et on prend la longueur
#debug concat ("*********************************",chr(13),chr(10))
#debug concat ("* About to create a Lego String *",chr(13),chr(10))
#debug concat ("*********************************",chr(13),chr(10))

#debug concat ("String to write : ",Chaine1,chr(13),chr(10))
#debug concat ("Number of chars of that string : ",str(Longueur,2,2),chr(13),chr(10))

#declare LB = 4; // Largeur Bloc (d�fini dans lego.inc)
#declare HB = 5; // Hauteur Bloc (idem)

#declare Espace = 1; // un espace entre deux caract�es fait deux picots
			// il vaut mieux ici garder un entier pour coh�rence!
			// ben maintennat, il n'en fait plus qu'un!

#declare Q = Quality ; // Qualit� des briques lego

/* Base bricks that we will reuse) */
#declare Brique1=Lego (2,2,Q)
#declare Brique2=Lego (2,5,Q)
#declare Brique3=Lego (2,4,Q)
#declare Brique4=Lego (2,6,Q)
#declare Brique5=Lego (2,3,Q)


/* D�claration des letttres de l'alphabet et de leur largeur*/
#local Long_Space=8;
#local Long_A = 6;
#local Lettre_A = union {
	object {Brique4 translate y*HB+2*x*LB} //la barre du A
	object {Brique1 }			//les pieds
	object {Brique1 translate 4*x*LB}
	object {Brique1 translate 4*x*LB+2*y*HB}	//au dessus de la barre
	object {Brique1 translate 2*y*HB}
	object {Brique1 translate 2*x*LB+4*y*HB}
	object {Brique3 translate 3*y*HB+2*x*LB}
	}

#local Long_B = 5;
#local Lettre_B = union {
	object {Brique2 translate 1.5*x*LB}
	object {Brique1 translate y*HB}
	object {Brique1 translate y*HB+3*x*LB}
	object {Brique3 translate 2*y*HB+1*x*LB}
	object {Brique1 translate 3*y*HB}
	object {Brique1 translate 3*y*HB+3*x*LB}
	object {Brique2 translate 4*y*HB+1.5*x*LB}
	}

#local Long_C = 5;
#local Lettre_C = union {
	object {Brique2 translate 1.5*x*LB}
	object {Brique1 translate y*HB}
	object {Brique1 translate 2*y*HB}
	object {Brique1 translate 3*y*HB}
	object {Brique2 translate 4*y*HB+1.5*x*LB}
	}

#local Long_D = 6;
#local Lettre_D = union {
	object {Brique2 translate 1.5*x*LB}
	object {Brique1 translate y*HB + 1*x*LB}
	object {Brique1 translate 2*y*HB+1*x*LB}
	object {Brique1 translate 3*y*HB+1*x*LB}
	object {Brique1 translate y*HB + 4*x*LB}
	object {Brique1 translate 2*y*HB+4*x*LB}
	object {Brique1 translate 3*y*HB+4*x*LB}
	object {Brique2 translate 4*y*HB+1.5*x*LB}
	}

#local Long_E = 5;
#local Lettre_E = union {
	object {Brique2 translate 1.5*x*LB}
	object {Brique1 translate y*HB}
	object {Brique3 translate 2*y*HB+1*x*LB}
	object {Brique1 translate 3*y*HB}
	object {Brique2 translate 4*y*HB+1.5*x*LB}
	}

#local Long_F = 5;
#local Lettre_F = union {
	object {Brique1}
	object {Brique1 translate y*HB}
	object {Brique3 translate 2*y*HB+1*x*LB}
	object {Brique1 translate 3*y*HB}
	object {Brique2 translate 4*y*HB+1.5*x*LB}
	}

#local Long_G = 5;
#local Lettre_G = union {
	object {Brique2 translate 1.5*x*LB}
	object {Brique1 translate y*HB}
	object {Brique1 translate 3*x*LB+y*HB}
	object {Brique1 translate 2*y*HB}
	object {Brique1 translate 3*y*HB}
	object {Brique2 translate 4*y*HB+1.5*x*LB}
	}

#local Long_H = 6;
#local Lettre_H = union {
	object {Brique4 translate 2*y*HB+2*x*LB}
	object {Brique1 }
	object {Brique1 translate 4*x*LB}
	object {Brique1 translate 4*x*LB+y*HB}
	object {Brique1 translate y*HB}
	object {Brique1 translate 4*x*LB+3*y*HB}
	object {Brique1 translate 3*y*HB}
	object {Brique1 translate 4*x*LB+4*y*HB}
	object {Brique1 translate 4*y*HB}
	}

#local Long_I = 3;
#local Lettre_I = union {
	object {Brique3 translate x*LB}
	object {Brique1 translate x*LB+y*HB}
	object {Brique1 translate x*LB+2*y*HB}
	object {Brique1 translate x*LB+3*y*HB}
	object {Brique3 translate x*LB+4*y*HB}
	}

#local Long_K = 6;
#local Lettre_K = union {
	object {Brique1}
	object {Brique1 translate 4*x*LB}
	object {Brique1 translate y*HB}
	object {Brique1 translate y*HB+3*x*LB}
	object {Brique3 translate 2*y*HB+1*x*LB}
	object {Brique1 translate 3*y*HB}
	object {Brique1 translate 3*y*HB+3*x*LB}
	object {Brique1 translate 4*y*HB}
	object {Brique1 translate 4*y*HB+4*x*LB}
	}

#local Long_L = 5;
#local Lettre_L = union {
	object {Brique2 translate 1.5*x*LB}
	object {Brique1 translate y*HB}
	object {Brique1 translate 2*y*HB}
	object {Brique1 translate 3*y*HB}
	object {Brique1 translate 4*y*HB}
	}

#local Long_N = 8;
#local Lettre_N = union {
	object {Brique1 translate 0}
	object {Brique1 translate y*HB}
	object {Brique1 translate 2*y*HB}
	object {Brique1 translate <6*LB,4*HB,0>}
	object {Brique1 translate <6*LB,3*HB,0>}
	object {Brique1 translate <6*LB,2*HB,0>}
	object {Brique1 translate <3*LB,2*HB,0>}
	object {Brique5 translate <.5*LB,4*HB,0>}
	object {Brique5 translate <5.5*LB,0*HB,0>}
	object {Brique3 translate <1*LB,3*HB,0>}
	object {Brique3 translate <5*LB,HB,0>}
	}
	
#local Long_O = 6;
#local Lettre_O = union {
	object {Brique1 translate y*HB}
	object {Brique1 translate y*HB+4*x*LB}
	object {Brique1 translate 2*y*HB}
	object {Brique1 translate 2*y*HB+4*x*LB}
	object {Brique1 translate 3*y*HB}
	object {Brique1 translate 3*y*HB+4*x*LB}
	object {Brique3 translate 4*y*HB+2*x*LB}
	object {Brique3 translate 0*y*HB+2*x*LB}
	}

#local Long_P = 6;
#local Lettre_P = union {
	object {Brique1}
	object {Brique1 translate y*HB}
	object {Brique2 translate 2*y*HB+1.5*x*LB}
	object {Brique1 translate 3*y*HB}
	object {Brique1 translate 3*y*HB+4*x*LB}
	object {Brique2 translate 4*y*HB+1.5*x*LB}
	}

#local Long_R = 6;
#local Lettre_R = union {
	object {Brique1}
	object {Brique1 translate 4*x*LB}
	object {Brique1 translate y*HB}
	object {Brique1 translate y*HB+3*x*LB}
	object {Brique3 translate 2*y*HB+1*x*LB}
	object {Brique1 translate 3*y*HB}
	object {Brique1 translate 3*y*HB+3*x*LB}
	object {Brique2 translate 4*y*HB+1.5*x*LB}
	}


#local Long_S = 6;
#local Lettre_S = union {
	object {Brique2 translate 1.5*x*LB}
	object {Brique1 translate y*HB+4*x*LB}
	object {Brique3 translate 2*y*HB+2*x*LB}
	object {Brique1 translate 3*y*HB }
	object {Brique2 translate 4*y*HB+2.5*x*LB}
	}

#local Long_T = 6;
#local Lettre_T = union {
	object {Brique1 translate 2*x*LB}
	object {Brique1 translate y*HB+2*x*LB}
	object {Brique1 translate 2*y*HB+2*x*LB}
	object {Brique1 translate 3*y*HB + 2*x*LB}
	object {Brique4 translate 4*y*HB+2*x*LB}
	}

#local Long_U = 6;
#local Lettre_U = union {
	object {Brique1 translate 4*y*HB}			
	object {Brique1 translate 4*y*HB+4*x*LB}
	object {Brique1 translate 1*y*HB}			
	object {Brique1 translate 1*y*HB+4*x*LB}
	object {Brique1 translate 3*y*HB}			
	object {Brique1 translate 3*y*HB+4*x*LB}
	object {Brique1 translate 4*x*LB+2*y*HB}	
	object {Brique1 translate 2*y*HB}
	object {Brique1 translate 2*x*LB}
	object {Brique3 translate 2*x*LB}
	}

#local Long_V = 6;
#local Lettre_V = union {
	object {Brique1 translate 4*y*HB}			
	object {Brique1 translate 4*y*HB+4*x*LB}
	object {Brique1 translate 3*y*HB}			
	object {Brique1 translate 3*y*HB+4*x*LB}
	object {Brique1 translate 4*x*LB+2*y*HB}	
	object {Brique1 translate 2*y*HB}
	object {Brique1 translate 2*x*LB}
	object {Brique3 translate 1*y*HB+2*x*LB}
	}

#local Long_W = 12;
#local Lettre_W = union {
	object {Brique1 translate 4*y*HB}			
	object {Brique1 translate 3*y*HB}			
	object {Brique1 translate 2*y*HB}	
	object {Brique5 translate 2*y*HB + 4.5*x*LB}
	object {Brique1 translate 2*x*LB}
	object {Brique3 translate 1*y*HB+2*x*LB}
	union {
		object {Brique1 translate 4*y*HB+4*x*LB}
		object {Brique1 translate 3*y*HB+4*x*LB}
		object {Brique1 translate 4*x*LB+2*y*HB}	
		object {Brique1 translate 2*x*LB}
		object {Brique3 translate 1*y*HB+2*x*LB}
		translate 5*x*LB
		}
	}

#local Long_X = 7;
#local Lettre_X = union {
	object {Brique1 translate 0*x*LB}
	object {Brique1 translate 5*x*LB}
	object {Brique1 translate y*HB + x*LB}
	object {Brique1 translate y*HB+4*x*LB}
	object {Brique5 translate 2*y*HB+2.5*x*LB}
	object {Brique1 translate 3*y*HB+x*LB}
	object {Brique1 translate 3*y*HB+4*x*LB}
	object {Brique1 translate 4*y*HB+0*x*LB}
	object {Brique1 translate 4*y*HB+5*x*LB}
	}

#local Long_Y = 6;
#local Lettre_Y = union {
	object {Brique1 translate <2*LB,0,0>}
	object {Brique1 translate <2*LB,HB,0>}
	object {Brique3 translate <2*LB,2*HB,0>}
	object {Brique1 translate <0,3*HB,0>}
	object {Brique1 translate <0,4*HB,0>}
	object {Brique1 translate <4*LB,3*HB,0>}
	object {Brique1 translate <4*LB,4*HB,0>}
	}
	

#local Long_Z = 6;
#local Lettre_Z = union {
	object {Brique4 translate 2*x*LB}
	object {Brique1 translate y*HB+1*x*LB}
	object {Brique1 translate 2*y*HB+2*x*LB}
	object {Brique1 translate 3*y*HB + 3*x*LB}
	object {Brique4 translate 4*y*HB+2*x*LB}
	}


#local Long_Point = 1;
#local Char_Point = object {Brique1}

/* cr�ation de l'objet */
/* on d�file les lettres, et � chaque fois, si on l'a, on l'affiche */
union {
#local Temoin = 1;
#local Pos = 0;
#while (Temoin <= Longueur)
	#local Courant = substr (Chaine1,Temoin,1)
	#switch (asc(Courant))
		#case (asc("."))  
			object {Char_Point translate Pos*x*LB}
			#local Pos = Pos + Long_Point + Espace;
			#break
		#case (asc("A"))  
			object {Lettre_A translate Pos*x*LB}
			#local Pos = Pos + Long_A + Espace;
			#break
		#case (asc("B"))  
			object {Lettre_B translate Pos*x*LB}
			#local Pos = Pos + Long_B + Espace;
			#break
		#case (asc("C"))  
			object {Lettre_C translate Pos*x*LB}
			#local Pos = Pos + Long_C + Espace;
			#break
		#case (asc("D"))  
			object {Lettre_D translate Pos*x*LB}
			#local Pos = Pos + Long_D + Espace;
			#break
		#case (asc("E"))  
			object {Lettre_E translate Pos*x*LB}
			#local Pos = Pos + Long_E + Espace;
			#break
		#case (asc("F"))  
			object {Lettre_F translate Pos*x*LB}
			#local Pos = Pos + Long_F + Espace;
			#break
		#case (asc("G"))  
			object {Lettre_G translate Pos*x*LB}
			#local Pos = Pos + Long_G + Espace;
			#break
		#case (asc("H"))  
			object {Lettre_H translate Pos*x*LB}
			#local Pos = Pos + Long_H + Espace;
			#break
		#case (asc("I"))  
			object {Lettre_I translate Pos*x*LB}
			#local Pos = Pos + Long_I + Espace;
			#break
		#case (asc("K"))  
			object {Lettre_K translate Pos*x*LB}
			#local Pos = Pos + Long_K + Espace;
			#break
		#case (asc("L"))  
			object {Lettre_L translate Pos*x*LB}
			#local Pos = Pos + Long_L + Espace;
			#break
		#case (asc("N"))  
			object {Lettre_N translate Pos*x*LB}
			#local Pos = Pos + Long_N + Espace;
			#break
		#case (asc("O")) 
			object {Lettre_O translate Pos*x*LB}
			#local Pos = Pos + Long_O + Espace;
			#break
		#case (asc("P")) 
			object {Lettre_P translate Pos*x*LB}
			#local Pos = Pos + Long_P + Espace;
			#break
		#case (asc("R")) 
			object {Lettre_R translate Pos*x*LB}
			#local Pos = Pos + Long_R + Espace;
			#break
		#case (asc("S")) 
			object {Lettre_S translate Pos*x*LB}
			#local Pos = Pos + Long_S + Espace;
			#break
		#case (asc("T")) 
			object {Lettre_T translate Pos*x*LB}
			#local Pos = Pos + Long_T + Espace;
			#break
		#case (asc("U")) 
			object {Lettre_U translate Pos*x*LB}
			#local Pos = Pos + Long_U + Espace;
			#break
		#case (asc("W")) 
			object {Lettre_W translate Pos*x*LB}
			#local Pos = Pos + Long_W + Espace;
			#break
		#case (asc("V")) 
			object {Lettre_V translate Pos*x*LB}
			#local Pos = Pos + Long_V + Espace;
			#break
		#case (asc("X")) 
			object {Lettre_X translate Pos*x*LB}
			#local Pos = Pos + Long_X + Espace;
			#break
		#case (asc("Y")) 
			object {Lettre_Y translate Pos*x*LB}
			#local Pos = Pos + Long_Y + Espace;
			#break
		#case (asc("Z")) 
			object {Lettre_Z translate Pos*x*LB}
			#local Pos = Pos + Long_Z + Espace;
			#break
		#case (asc(" ")) 
			#local Pos = Pos + Long_Space + Espace;
			#break
		#else
			#debug concat ("Unknown character : ",Courant," , ASCII code : ",str(asc(Courant),1,1),chr(13),chr(10))
		#end // du switch				
	#local Temoin = Temoin + 1;
	#end //du while
	translate x*floor((1-Pos/2))*LB
}

#debug concat ("Out of the macro",chr(13),chr(10),chr(10))
#end //of macro

#macro WriteName(Chaine,Quality)
Ecris_Nom (Chaine,Quality)
#end // of macro
