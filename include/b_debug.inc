// This file contains a bunch of objects that are usually useful for debugging
// use the following options
// d_axis : display all 3 vectors
// xy_plane, xz_plane: relevant planes, with checkered to measure / check

//axis
#ifdef (d_axis)
  union {
    cylinder {0,100*x,0.01
      texture {
        pigment {
					gradient x
					color_map {
						[0 color rgb <1,0,0>]
						[1 color rgb <1,1,1>]
					}
        }
      }
    }
    cylinder {0,100*z,0.01
      texture {
        pigment {
					gradient z
					color_map {
						[0 color rgb <0,1,0>]
						[1 color rgb <1,1,1>]
					}
        }
      }
    }
    cylinder {0,100*y,0.01
      texture {
        pigment {
          gradient y
          color_map {
            [0 color rgb <0,0,1>]
            [1 color rgb <1,1,1>]
          }
        }
      }
    }
  }
#end


#ifdef (xz_plane)
	plane{
		y,0
		texture {
			pigment {
				checker color rgb<1,1,0> color rgb <0,1,1> 
			}
		}
	}
#end

#ifdef (xy_plane)
	plane{
		z,0
		texture {
			pigment {
				checker color rgb<0,0,0> color rgb <1,1,1> 
			}
		}
	}
#end