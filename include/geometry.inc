
/*
This macro is used to draw a plane passing
through 3 defined points
*/

#macro Plane_From_3_Points (a,b,c) 
  #local norm_vect=vnormalize (vcross (b-a,c-a));

  plane {norm_vect,vdot (a,norm_vect)}
#end

