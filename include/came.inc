/*
helper functions
cf explanation on blog post
*/
#declare radiusCame_2 = function(r,h,theta) {r*(sqrt(cos(theta)*cos(theta)+3)-cos(theta))}
#declare radiusCame_3 = function(r,h,theta) {h*sin(theta)+sqrt(-4*r*sqrt(r*r+h*h)+5*r*r+h*h*sin(theta)*sin(theta))}

// works with theta in [-pi/2,pi/2]
#declare radiusCameLim = function(r,h,theta) {
  select(theta,r,
    select (theta-atan(2*sin(atan(h/r))/(2*cos(atan(h/r))-1)),
      radiusCame_2(r,h,theta),
      radiusCame_3(r,h,theta)
    )
  )
}

// works for theta in [-pi/2,3pi/2]
#declare radiusCameLim2 = function(r,h,theta) {
  select(theta-pi/2,
    radiusCameLim(r,h,theta),
    radiusCameLim(r,h,pi-theta))
    
}

#declare LongRadius = function (r,h){h+2*r-sqrt(r*r+h*h)}


/*
* theta is 0 on x axis
* if the rod is vertical (y) axis, needs to use +pi/2
*/

#declare radiusCame = function(r,h,theta) {
    // first modulo brings into [-2"pi, 2*pi]
    // second modulo brings into [0,2*pi]
    radiusCameLim2(r,h,mod(mod(theta+pi/2,2*pi)+2*pi,2*pi)-pi/2)
}
    
/*
* extended part is growing on y
* centered on 0, thickness is 0.2 on z [-0.1 -> 0.1]
*/
#macro came(r,h)
  union {
    intersection {
      cylinder {-0.1*z,0.1*z,r}
      plane {y,0}
    }
    intersection{
      cylinder {-0.1*z,0.1*z,2*r
        translate <-r,0,0>
      }
      plane {-y,0}
      plane {y,0
        rotate <0,0,atan(h/r)*180/pi>
        translate <-r,0,0>
      }
    }
    intersection{
      cylinder {-0.1*z,0.1*z,2*r
        translate <r,0,0>
      }
      plane {-y,0}
      plane {y,0
        rotate <0,0,-atan(h/r)*180/pi>
        translate <r,0,0>
      }
    }
    intersection{
      cylinder {-0.1*z,0.1*z,2*r-sqrt(r*r+h*h)
      }
      plane {-y,0
        rotate <0,0,atan(h/r)*180/pi>
      }
      plane {-y,0
        rotate <0,0,-atan(h/r)*180/pi>
      }
      translate <0,h,0>
    }
    texture {pigment {color rgb <1,1,1>}} 
  }
#end
