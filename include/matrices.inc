/*
Include Matrice
Sert à calculer la matrice de passage d'une base à l'autre:
Il suffit
de faire matrix Transf (u1,u2,u3) comme transformation
pour passer de la base (x,y,z) à (u1,u2,u3)    
Cette unité utilise l'algorithme d'inversion de Gauss-Jordan
Complexité en O(n^3)

Les fonctions Swap, Multiplie... peuvent s'appliquer à tout type de matrice
*/

/*
To go from one base to another, the helper function Transf can be used.
If you want (x,y,z) to become (u1,u2,u3), you can use this special tranformation. It's 3 coordinate vector so it won't handle any translation for you.
object {A
  matrix Transf (u1,u2,u3)
}

This allows to easily do symetries, rotations, skewing effects...
symetry against y,z plane
object {A
  matrix Transf (-x,y,z)
}
*/

/*
**************************************
* Author  : Florent "Bruce" REVELUT  *
**************************************
*/    
                


#macro Affiche (Mat)
  #local Lim1=dimension_size (Mat,1);
  #local Lim2=dimension_size (Mat,2);
  #local Tem1=0;
  #while (Tem1<Lim1)
        #local Tem2=0;
        #local S=""
        #while (Tem2<Lim2)
                #local S=concat (S,str (Mat [Tem1][Tem2],0,2)," ")
                #local Tem2=Tem2+1;
        #end
        #debug  concat (S,chr (13),chr (10))
        #local Tem1=Tem1+1;                
  #end
#debug concat (chr (13),chr (10))
#end        

#macro Swap (Mat,l1,l2)
// #render concat ("Echange la ligne ",str (l1,0,0)," et la ligne ",str (l2,0,0),chr (13),chr (10))
#local B=Mat
#local Tem1=0;
#local Lim1=dimension_size (Mat,2);
  #while (Tem1<Lim1)
  #local B[l2][Tem1]=Mat[l1][Tem1];
  #local B[l1][Tem1]=Mat[l2][Tem1];
  #local Tem1=Tem1+1;                
  #end
B
#end

#macro Ajoute (Mat,K,L2,L1) //ajoute k fois la ligne 1 à la ligne 2
// #render concat ("Ajoute ",str (K,0,2)," fois la ligne ",str (L1,0,0)," à la ligne ",str (L2,0,0),chr (13),chr (10))
#local B=Mat
#local Lim1=dimension_size (Mat,2);
#local Tem1=0;
#while (Tem1<Lim1)
   #local B [L2] [Tem1]=B[L2] [Tem1] + K*B[L1][Tem1];
   #local Tem1=Tem1+1;
   #end
B   
#end

#macro Multiplie (Mat,K,L1)
//#render concat ("Multiplie ",str (K,0,2)," fois la ligne ",str (L1,0,0),chr (13),chr (10))
#local B=Mat
#local Lim2=dimension_size (Mat,2);
#local Tem2=0;
#while (Tem2<Lim2)
        #local B[L1][Tem2]=B[L1][Tem2]*K;
        #local Tem2=Tem2+1;
        #end
B
#end
        
#macro Inverse (Mat)
#local B=Mat
#local Lim1=dimension_size (Mat,1);
#local Lim2=dimension_size (Mat,2);
#local Tem1=0;
#while (Tem1<Lim1) //on trignonalise sup la première matrice
        #if (B[Tem1][Tem1]=0)
                #local Tem3=Tem1;
                #local Ind=-1;              
                #while (Tem3<Lim1)         
                        #if (B[Tem3][Tem1]=0)
                                #else
                                #local Ind=max (Ind,Tem3);
                                #end //du if
                        #local Tem3=Tem3+1;
                        #end //du while
                #if (Ind=-1)
                        Affiche (B)
                        #error "Matrice  non inversible - la base est dégénérée"
                        #else
                        #local B=Swap (B,Ind,Tem1)
                        #end  //du if
                #end //du if
        #local Tem2=Tem1+1;
        #local Truc=B[Tem1][Tem1];
        #while (Tem2<Lim1)
                #local B=Ajoute (B,-(B [Tem2][Tem1])/Truc,Tem2,Tem1)
                #local Tem2=Tem2+1;
                #end
        #local Tem1=Tem1+1;
        #end // du while sur Tem1
#local Tem1=0;
#while (Tem1<Lim1)  //on transforme la diag de la première matrice en 1
        #local B=Multiplie (B,1/(B[Tem1][Tem1]),Tem1)
        #local Tem1=Tem1+1;
        #end
#local Tem1=Lim1-1;
#while (Tem1>=0)  //on trigonalise inf la première matrice
        #local Tem2=Tem1-1;
        #while (Tem2>=0)
                #local B=Ajoute (B,-B[Tem2][Tem1],Tem2,Tem1)
                #local Tem2=Tem2-1;                
                #end
        #local Tem1=Tem1-1;
        #end
B
#end                

#macro Transf (u1,u2,u3)   
#debug concat ("appel a l'unit matrice",chr (13),chr( 10))
#debug concat ("Inversion de matrice",chr (13),chr (10))
#debug concat (chr (13),chr (10))
#local P_Loc_Cour_Truc001a=array [3] [6] {{u1.x,u2.x,u3.x,1,0,0}
        {u1.y,u2.y,u3.y,0,1,0}
        {u1.z,u2.z,u3.z,0,0,1}}
#local P_Loc_Cour_Truc001a=Inverse (P_Loc_Cour_Truc001a)
<P_Loc_Cour_Truc001a[0][3],P_Loc_Cour_Truc001a[0][4],P_Loc_Cour_Truc001a[0][5],
        P_Loc_Cour_Truc001a[1][3],P_Loc_Cour_Truc001a[1][4],P_Loc_Cour_Truc001a[1][5],
        P_Loc_Cour_Truc001a[2][3],P_Loc_Cour_Truc001a[2][4],P_Loc_Cour_Truc001a[2][5],
        0,0,0>
#end        


